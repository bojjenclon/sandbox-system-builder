class auxMeth {
  /** Gets Sheets */
  static async getSheets() {
    // console.log("getting sheets");

    const templates = [];

    templates.push('Default');

    const templatenames = game.actors.filter((y) => y.data.data.istemplate);

    for (let i = 0; i < templatenames.length; i += 1) {
      templates.push(templatenames[i].name);
    }

    // console.log(templates);
    return templates;
  }

  static async registerDicID(relativeID, objectID, ciKey = null) {
    const jsontxt = game.settings.get('sandbox', 'idDict');
    let idDict = {};

    if (jsontxt !== '') {
      idDict = JSON.parse(jsontxt);
    } else {
      idDict.ids = {};
    }
    if (ciKey) {
      if (ciKey !== relativeID) {
        idDict.ids[ciKey] = objectID;
      }
    }

    idDict.ids[relativeID] = objectID;

    const myJSON = JSON.stringify(idDict);
    if (game.user.isGM) {
      await game.settings.set('sandbox', 'idDict', myJSON);
    }
  }

  static async getcItem(id, ciKey = null) {
    // console.log("getting cItem");
    let ciTemplate = game.items.get(id);

    if (ciTemplate === null) {
      const jsontxt = game.settings.get('sandbox', 'idDict');

      if (jsontxt !== '') {
        const idDict = JSON.parse(jsontxt);
        if (idDict.ids[id] !== null) {
          ciTemplate = game.items.get(idDict.ids[id]);
        }
        if (ciTemplate === null) {
          if (idDict.ids[ciKey] !== null) {
            ciTemplate = game.items.get(idDict.ids[ciKey]);
          }
        }
      }
    }

    if (ciTemplate === null) {
      // let allcitems = ;
      let isHere = game.items.filter((y) => Boolean(y.data.data.ciKey)).find((y) => y.data.data.ciKey === id);
      if (!isHere) {
        isHere = game.items.filter((y) => Boolean(y.data.data.ciKey)).find((y) => y.data.data.ciKey === ciKey);
      }

      if (isHere) {
        ciTemplate = isHere;
        await auxMeth.registerDicID(id, isHere.id, ciKey);
      }
    }

    // To correct post 0.9
    if (ciTemplate === null) {
      // let allcitems = ;
      let isHere = game.items.filter((y) => Boolean(y.data._source.data.ciKey)).find((y) => y.data._source.data.ciKey === id);
      if (!isHere) {
        isHere = game.items.filter((y) => Boolean(y.data._source.ciKey)).find((y) => y.data._source.ciKey === ciKey);
      }
      if (isHere) {
        ciTemplate = isHere;
        await auxMeth.registerDicID(id, isHere.id, ciKey);
      }
    }

    if (ciTemplate === null) {
      let locatedPack;
      let locatedId;
      let found = false;

      const { packs } = game;
      packs.forEach(async (pack) => {
        if (found) {
          return;
        }

        if (pack.documentName !== 'Item') {
          return;
        }

        const packContents = await pack.getDocuments();
        // let citems = ;
        let newciKey = id;
        if (ciKey !== null) {
          newciKey = ciKey;
        }
        const isHere = packContents.filter((y) => Boolean(y.data.data)).find((y) => y.data.data.ciKey === id || y.id === id || y.data.data.ciKey === newciKey);
        if (isHere) {
          locatedPack = pack;
          locatedId = isHere.id;
          found = true;
        }
      });

      if (locatedPack !== null) {
        let findFolder = game.folders.find((y) => y.name === locatedPack.title);

        if (!findFolder) {
          findFolder = await Folder.create({ name: locatedPack.title, type: 'Item' });
        }

        const importedobject = await game.items.importFromCompendium(locatedPack, locatedId, { folder: findFolder.id }, { keepId: true });
        // console.log(importedobject);
        ciTemplate = importedobject;

        // ciObject.id = ciTemplate.id;
      }
    }

    return ciTemplate;
  }

  static async getTElement(id, type = null, key = null) {
    // console.log(id + " " + type + " " + key);
    let myElem = game.items.get(id);

    let propKey = '';

    if (type === 'property') {
      propKey = 'attKey';
    }
    if (type === 'panel' || type === 'multipanel') {
      propKey = 'panelKey';
    }
    if (type === 'sheettab') {
      propKey = 'tabKey';
    }
    if (type === 'group') {
      propKey = 'groupKey';
    }
    if (key !== null && myElem !== null) {
      if (myElem.data.data[propKey] !== key) {
        myElem = null;
      }
    }
    if (myElem === null) {
      myElem = await game.items.filter((y) => Boolean(y.data.data[propKey])).find((y) => y.data.data[propKey] === key);
    }

    // To correct post 0.9
    if (myElem === null) {
      const isHere = game.items.filter((y) => Boolean(y.data._source.data[propKey])).find((y) => y.data._source.data[propKey] === id);
      if (isHere) {
        myElem = isHere;
      }
    }

    if (myElem === null && key !== null) {
      let locatedPack;
      let locatedId;

      const { packs } = game;
      packs.forEach(async (pack) => {
        if (pack.documentName !== 'Item') {
          return;
        }

        const packContents = await pack.getDocuments();
        const isHere = packContents.filter((y) => Boolean(y.data.data)).find((y) => y.data.data[propKey] === key);
        if (isHere) {
          locatedPack = pack;
          locatedId = isHere.id;
        }
      });

      if (locatedPack !== null) {
        let findFolder = game.folders.find((y) => y.name === locatedPack.title);

        if (!findFolder) {
          findFolder = await Folder.create({ name: locatedPack.title, type: 'Item' });
        }

        const importedobject = await game.items.importFromCompendium(locatedPack, locatedId, { folder: findFolder.id }, { keepId: true });
        myElem = importedobject;
      }
    }
    // console.log(myElem);
    return myElem;
  }

  static async getTempHTML(gtemplate, istemplate = false) {
    let html = '';

    // const mytemplate = gtemplate;
    if (gtemplate !== 'Default') {
      const _template = await game.actors.find((y) => y.data.data.istemplate && y.data.data.gtemplate === gtemplate);
      if (_template !== null) {
        html = _template.data.data._html;
      }
    }

    if (html === null || html === '') {
      gtemplate = 'Default';
      html = await fetch(this.getHTMLPath(gtemplate)).then((resp) => resp.text());
    }

    return html;
  }

  static getHTMLPath(gtemplate) {
    let path = `worlds/${game.data.world.name}`;
    //        const path = "systems/sandbox/templates/" + game.data.world + "/";
    // let gtemplate = '';

    if (gtemplate === '' || gtemplate === 'Default') {
      gtemplate = 'character';
      path = 'systems/sandbox/templates/';
    }

    const templatepath = `${path}/${gtemplate}.html`;
    return templatepath;
  }

  /* -------------------------------------------- */

  static async retrieveBTemplate() {
    const form = await fetch('systems/sandbox/templates/character.html').then((resp) => resp.text());
    return form;
  }

  static async buildSheetHML() {
    const parser = new DOMParser();
    const htmlcode = await auxMeth.retrieveBTemplate();
    const html = parser.parseFromString(htmlcode, 'text/html');
    return html;
  }

  // EXPORT TEST

  static exportBrowser() {
    const entities = {};

    entities.actors = [];
    entities.items = [];
    entities.folders = [];

    const allfolders = game.folders.contents.filter((y) => y.type === 'Item' || y.type === 'Actor');
    const itemfolders = game.folders.contents.filter((y) => y.type === 'Item' && y.data.parent === null);
    const actorfolders = game.folders.contents.filter((y) => y.type === 'Actor' && y.data.parent === null);

    let finalContent = '<div class="exportbrowser">';
    const endDiv = '</div>';
    finalContent += '<div class="new-row">ITEM FOLDERS</div>';

    for (let i = 0; i < itemfolders.length; i += 1) {
      const thisfolder = itemfolders[i];

      finalContent += `<div class="new-row"><input class="exportDialog checkbox check-folder${i}" folderid ="${thisfolder.id}" type="checkbox">`;
      finalContent += `<label class="exportlabel">${thisfolder.name}</label></div>`;

      const containedfolders = thisfolder.children;
      for (let j = 0; j < containedfolders.length; j += 1) {
        const subfolder = containedfolders[j];

        finalContent += `<div class="new-row" style="margin-left:30px"><input class="exportDialog checkbox check-folder${i}" folderid ="${subfolder.id}" parentid="${thisfolder.id}" type="checkbox">`;
        finalContent += `<label class="exportlabel">${subfolder.name}</label></div>`;

        const subcontainedfolders = subfolder.children;
        for (let k = 0; k < subcontainedfolders.length; k += 1) {
          const subsubfolder = subcontainedfolders[k];

          finalContent += `<div class="new-row" style="margin-left:60px"><input class="exportDialog checkbox check-folder${i}" folderid ="${subsubfolder.id}" parentid="${subfolder.id}" type="checkbox">`;
          finalContent += `<label class="exportlabel">${subsubfolder.name}</label></div>`;
        }
      }
    }
    finalContent += '<div class="new-row">ACTOR FOLDERS</div>';

    for (let i = 0; i < actorfolders.length; i += 1) {
      const thisfolder = actorfolders[i];

      finalContent += `<div class="new-row"><input class="exportDialog checkbox check-folder${i}" folderid ="${thisfolder.id}" type="checkbox">`;
      finalContent += `<label class="exportlabel">${thisfolder.name}</label></div>`;

      const containedfolders = thisfolder.children;
      for (let j = 0; j < containedfolders.length; j += 1) {
        const subfolder = containedfolders[j];

        finalContent += `<div class="new-row" style="margin-left:30px"><input class="exportDialog checkbox check-folder${i}" folderid ="${subfolder.id}" parentid="${thisfolder.id}" type="checkbox">`;
        finalContent += `<label class="exportlabel">${subfolder.name}</label></div>`;

        const subcontainedfolders = subfolder.children;
        for (let k = 0; k < subcontainedfolders.length; k += 1) {
          const subsubfolder = subcontainedfolders[k];

          finalContent += `<div class="new-row" style="margin-left:60px"><input class="exportDialog checkbox check-folder${i}" folderid ="${subsubfolder.id}" parentid="${subfolder.id}" type="checkbox">`;
          finalContent += `<label class="exportlabel">${subsubfolder.name}</label></div>`;
        }
      }
    }

    //         for (let i = 0; i < entities.folders.length; i+= 1) {
    //             finalContent += `
    //     <div class="new-row">
    //     `;

    //             finalContent += `
    //     <input class="exportDialog checkbox check-folder${i}" folderid ="${entities.folders[i].id}" type="checkbox">
    // `;
    //             finalContent += `
    //     <label class="exportlabel">${entities.folders[i].name}</label>
    //     `;
    //             finalContent += endDiv;
    //         }

    finalContent += endDiv;

    const d = new Dialog({
      title: 'Choose folders to export',
      content: finalContent,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: 'OK',
          callback: async (html) => {
            const selectedfolder = html[0].getElementsByClassName('exportDialog');

            for (let k = 0; k < selectedfolder.length; k += 1) {
              const folderKey = selectedfolder[k].getAttribute('folderid');

              if (selectedfolder[k].checked) {
                const theFolder = allfolders.find((y) => y.id === folderKey);
                entities.folders.push(theFolder);

                for (let n = 0; n < theFolder.contents.length; n += 1) {
                  if (theFolder.contents[n].documentName === 'Item') {
                    entities.items.push(theFolder.contents[n]);
                  }

                  if (theFolder.contents[n].documentName === 'Actor') {
                    entities.actors.push(theFolder.contents[n]);
                  }
                }
              }
            }

            auxMeth.exportTree(true, entities);
          },
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: 'Cancel',
          callback: () => {
            console.log('canceling selection');
          },
        },
      },
      default: 'one',
      exportDialog: true,
      close: () => {
        console.log('cItem selection dialog was shown to player.');
      },
    });
    d.render(true);
  }

  static exportTree(writeFile = true, groups = null) {
    let allData = null;
    // const metadata = {
    //   world: game.world.id,
    //   system: game.system.id,
    //   coreVersion: game.data.version,
    //   systemVersion: game.system.data.version,
    // };

    allData = JSON.stringify(groups, null, 2);

    console.log(`Exported ${groups.actors.length} Actors and ${groups.items.length} Items of the world`);

    // Trigger file save procedure
    const filename = 'export.json';

    if (writeFile) {
      auxMeth.writeJSONToFile(filename, allData);
    }
    return {
      filename,
      allData,
    };
  }

  static async writeJSONToFile(filename, data) {
    this.saveDataToFile(data, 'text/json', filename);
    console.log(`Saved to file ${filename}`);
  }

  static async getImportFile() {
    (new FilePicker({
      type: 'json',
      callback: (filePath) => auxMeth.importTree(filePath),
    })).browse();
  }

  static async importTree(exportfilePath) {
    // let exportfilePath = "worlds/" + gameName + "/export.json";

    const response = await fetch(exportfilePath);
    const importedPack = await response.json();
    const { actors } = importedPack;
    const { items } = importedPack;
    const { folders } = importedPack;

    const idCollection = {};

    for (let i = 0; i < actors.length; i += 1) {
      const anactor = actors[i];
      const istemplate = duplicate(anactor.data.istemplate);
      const result = await Actor.create(anactor);
      if (anactor.folder) {
        result.setFlag('sandbox', 'folder', anactor.folder);
      }
      result.setFlag('sandbox', 'istemplate', istemplate);
      idCollection[anactor._id] = result.data._id;
    }

    for (let i = 0; i < items.length; i += 1) {
      const anitem = items[i];
      const result = await Item.create(anitem);
      if (anitem.folder) {
        result.setFlag('sandbox', 'folder', anitem.folder);
      }
      idCollection[anitem._id] = result.data._id;
    }

    for (let i = 0; i < folders.length; i += 1) {
      const afolder = folders[i];
      const result = await Folder.create({ name: afolder.name, type: afolder.type });
      if (afolder.parent) {
        result.realparent = afolder.parent;
      }
      idCollection[afolder._id] = result.data._id;
    }

    const { contents: folderContents } = game.folders;
    folderContents.forEach((folder) => {
      if (hasProperty(idCollection, folder.realparent)) {
        folder.update({ parent: idCollection[folder.realparent] });
      }
    });

    const { contents: itemsContents } = game.items;
    itemsContents.forEach(async (item) => {
      const finalitem = duplicate(item);
      if (item.data.type === 'property') {
        if (hasProperty(idCollection, finalitem.data.dialogID) && finalitem.data.dialogID !== '') {
          finalitem.data.dialogID = idCollection[finalitem.data.dialogID];
        }

        if (hasProperty(idCollection, finalitem.data.group.id) && finalitem.data.group.id !== '') {
          finalitem.data.group.id = idCollection[finalitem.data.group.id];
        }
      }

      if (item.data.type === 'panel' || item.data.type === 'group') {
        const { properties } = finalitem.data;
        if (properties.length > 0) {
          properties.forEach((property) => {
            if (hasProperty(idCollection, property.id)) {
              property.id = idCollection[property.id];
            } else {
              let findprop = game.items.get(property.id);
              if (findprop === null) {
                findprop = game.items.find((y) => y.data.type === 'property' && y.data.data.attKey === property.ikey);
              }
              if (findprop !== null) {
                property.id = findprop.id;
              }
            }
          });
        }
      }

      if (item.data.type === 'multipanel' || item.data.type === 'sheettab') {
        const { panels } = finalitem.data;
        if (panels.length > 0) {
          panels.forEach((panel) => {
            if (hasProperty(idCollection, panel.id)) {
              panel.id = idCollection[panel.id];
            }
          });
        }
      }

      if (item.data.type === 'cItem') {
        const { groups } = finalitem.data;
        if (groups.length > 0) {
          groups.forEach((group) => {
            if (hasProperty(idCollection, group.id)) {
              group.id = idCollection[group.id];
            } else {
              let findgroup = game.items.get(group.id);
              if (findgroup === null) {
                findgroup = game.items.find((y) => y.data.type === 'group' && y.data.data.groupKey === group.ikey);
              }
              if (findgroup !== null) {
                group.id = findgroup.id;
              }
            }
          });
        }

        const { mods } = finalitem.data;
        if (mods.length > 0) {
          mods.forEach((mod) => {
            if (mod.items.length > 0) {
              const { items: modItems } = mod;
              modItems.forEach((modItem) => {
                if (hasProperty(idCollection, modItem.id)) {
                  modItem.id = idCollection[modItem.id];
                } else {
                  let findcitem = game.items.get(modItem.id);
                  if (findcitem === null) {
                    findcitem = game.items.find((y) => y.data.type === 'cItem' && y.data.data.ciKey === modItem.id);
                  }

                  if (findcitem === null) {
                    findcitem = game.items.find((y) => y.data.type === 'cItem' && y.name === modItem.name);
                  }

                  if (findcitem !== null) {
                    modItem.id = findcitem.id;
                  }
                }
              });
            }
          });
        }

        if (item.data.data.dialogID !== '') {
          finalitem.data.dialogID = idCollection[finalitem.data.dialogID];
        }
      }

      const folderlink = idCollection[item.getFlag('sandbox', 'folder')];
      if (folderlink) {
        await item.update({ data: finalitem.data, folder: folderlink });
      } else {
        await item.update({ data: finalitem.data });
      }
    });

    const { contents: actorsContents } = game.actors;
    actorsContents.forEach(async (actor) => {
      const finalactor = await duplicate(actor);
      // console.log("importing actor: " + finalactor.name);
      if (finalactor.token !== null) {
        if (finalactor.token.actorId !== null) {
          if (hasProperty(idCollection, finalactor.token.actorId)) {
            finalactor.token.actorId = idCollection[finalactor.token.actorId];
          }
        }
      }

      const { citems: actorItems } = actor.data.data;
      if (actorItems.length > 0) {
        const { citems: finalItems } = finalactor.data;
        finalItems.forEach((citem) => {
          if (hasProperty(idCollection, citem.id)) {
            citem.id = idCollection[citem.id];
            citem.addedBy = idCollection[citem.addedBy];

            const { groups } = citem;
            if (groups) {
              groups.forEach((group) => {
                if (hasProperty(idCollection, group.id)) {
                  group.id = idCollection[group.id];
                }
              });
            }

            const { mods } = citem;
            if (mods) {
              mods.forEach((mod) => {
                if (hasProperty(idCollection, mod.citem)) {
                  mod.citem = idCollection[mod.citem];
                }
              });
            }
          }
        });
      }

      const { tabs: actorTabs } = actor.data.data;
      if (actorTabs.length > 0) {
        const { tabs: finalTabs } = finalactor.data;
        finalTabs.forEach((tab) => {
          if (hasProperty(idCollection, tab.id)) {
            tab.id = idCollection[tab.id];
          }
        });
      }

      const { attributes: finalAttrs } = finalactor.data;
      Object.values(finalAttrs).forEach((attr) => {
        if (attr.id !== null) {
          if (hasProperty(idCollection, attr.id)) {
            attr.id = idCollection[attr.id];
          }
        }
      });

      const folderlink = await idCollection[actor.getFlag('sandbox', 'folder')];
      finalactor.data.istemplate = await actor.getFlag('sandbox', 'istemplate');

      if (folderlink) {
        await actor.update({ data: finalactor.data, folder: folderlink });
      } else {
        await actor.update({ data: finalactor.data });
      }
    });
  }

  /* -------------------------------------------- */

  static async registerIfHelper() {
    Handlebars.registerHelper('ifCond', (v1, v2, options) => {
      if (!v1 || !v2) {
        return options.inverse(this);
      }

      const regE = /^\d+$/g;
      v1 = v1.toString();
      v2 = v2.toString();

      const isnumv1 = v1.match(regE);
      const isnumv2 = v2.match(regE);

      if (isnumv1) {
        v1 = Number(v1);
      }

      if (isnumv2) {
        v2 = Number(v2);
      }

      if (v1 === v2) {
        return options.fn(this);
      }
      return options.inverse(this);
    });
  }

  static async registerIfGreaterHelper() {
    Handlebars.registerHelper('ifGreater', (v1, v2, options) => {
      if (parseInt(v1, 10) > parseInt(v2, 10)) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerIfLessHelper() {
    Handlebars.registerHelper('ifLess', (v1, v2, options) => {
      if (v1 < v2) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerIfNotHelper() {
    Handlebars.registerHelper('ifNot', (v1, v2, options) => {
      if (!v1 || !v2) {
        return options.inverse(this);
      }

      v1 = v1.toString();
      v2 = v2.toString();

      if (v1 !== v2) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerIsGM() {
    Handlebars.registerHelper('isGM', (options) => {
      if (game.user.isGM) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerShowMod() {
    Handlebars.registerHelper('advShow', (options) => {
      if (game.settings.get('sandbox', 'showADV')) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerShowSimpleRoll() {
    Handlebars.registerHelper('showRoller', (options) => {
      if (game.settings.get('sandbox', 'showSimpleRoller')) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async registerShowRollMod() {
    Handlebars.registerHelper('rollMod', (options) => {
      if (game.settings.get('sandbox', 'rollmod')) {
        return options.fn(this);
      }

      return options.inverse(this);
    });
  }

  static async isNumeric(str) {
    if (typeof str !== 'string') {
      return false;
    } // we only process strings!

    return !Number.isNaN(str) // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
            && !Number.isNaN(parseFloat(str)); // ...and ensure strings of whitespace fail
  }

  static async regParser(expr, attributes, itemattributes) {
    const regArray = [];
    const expreg = expr.match(/(?<=\$<).*?(?=>)/g);
    if (expreg !== null) {
      // Substitute string for current value
      for (let i = 0; i < expreg.length; i += 1) {
        const attname = `$<${expreg[i]}>`;
        const attvalue = '';

        const regblocks = expreg[i].split(';');

        const regobject = {};
        regobject.index = regblocks[0];
        regobject.expr = regblocks[1];
        regobject.result = await auxMeth.autoParser(regblocks[1], attributes, itemattributes, false, true);
        regArray.push(regobject);

        expr = expr.replace(attname, attvalue);
      }

      const exprparse = expr.match(/(?<=\$)[0-9]+/g);

      for (let i = 0; i < exprparse.length; i += 1) {
        const regindex = exprparse[i];

        const attname = `$${regindex}`;
        const regObj = regArray.find((y) => y.index === regindex);

        let attvalue = '';
        if (regObj !== null) {
          attvalue = regObj.result;
        }

        expr = expr.replace(attname, attvalue);
      }
    }

    return expr;
  }

  static async parseDialogProps(expr, dialogProps) {
    const itemresult = expr.match(/(?<=\bd\{).*?(?=\})|(?<=\wd\{).*?(?=\})/g);
    if (itemresult !== null && dialogProps !== null) {
      // Substitute string for current value
      for (let i = 0; i < itemresult.length; i += 1) {
        const attname = `d{${itemresult[i]}}`;
        let attvalue;

        if (dialogProps[itemresult[i]] !== null) {
          attvalue = dialogProps[itemresult[i]].value;
        } else {
          attvalue = 0;
        }

        if ((attvalue !== false) && (attvalue !== true)) {
          if ((attvalue === '' || attvalue === null)) {
            attvalue = 0;
          }
        }

        if (attvalue === null) {
          attvalue = 0;
        }

        expr = expr.replace(attname, attvalue);
      }
    }

    return expr;
  }

  static async autoParser(expr, attributes, itemattributes, exprmode, noreg = false, number = 1) {
    if (typeof (expr) !== 'string') {
      return expr;
    }

    let toreturn = expr;
    let diff = await game.settings.get('sandbox', 'diff');
    if (diff === null || Number.isNaN(diff)) {
      diff = 0;
    }

    expr = expr.replace(/#{diff}/g, diff);

    // PARSE TO TEXT
    const textexpr = expr.match(/[|]/g);
    if (textexpr !== null && (expr.charAt(0) === '|')) {
      expr = expr.substr(1, expr.length);
      exprmode = true;
    }

    // Expression register. Recommended to avoid REgex shennanigans
    const regArray = [];
    let expreg;
    if (!noreg) {
      expreg = expr.match(/(?<=\$<).*?(?=>)/g);
    }

    if (expreg !== null) {
      // Substitute string for current value
      for (let i = 0; i < expreg.length; i += 1) {
        const attname = `$<${expreg[i]}>`;
        const attvalue = '';

        const regblocks = expreg[i].split(';');

        const regobject = {};
        regobject.index = regblocks[0];
        regobject.expr = expreg[i].replace(`${regblocks[0]};`, '');

        const internalvBle = regobject.expr.match(/(?<=\$)[0-9]+/g);
        if (internalvBle !== null) {
          for (let k = 0; k < internalvBle.length; k += 1) {
            const regindex = internalvBle[k];
            const regObj = regArray.find((y) => y.index === regindex);
            let vbvalue = '';

            if (regObj !== null) {
              vbvalue = regObj.result;
            }

            regobject.expr = regobject.expr.replace(`$${regindex}`, vbvalue);
          }
        }

        regobject.result = await auxMeth.autoParser(regobject.expr, attributes, itemattributes, false, true);
        regArray.push(regobject);

        expr = expr.replace(attname, attvalue);
      }

      const exprparse = expr.match(/(?<=\$)[0-9]+/g);
      if (exprparse !== null) {
        for (let i = 0; i < exprparse.length; i += 1) {
          const regindex = exprparse[i];

          const attname = `$${regindex}`;
          const regObj = regArray.find((y) => y.index === regindex);

          let attvalue = '';
          if (regObj !== null) {
            attvalue = regObj.result;
          }

          expr = expr.replace(attname, attvalue);
          expr = expr.trimStart();
        }
      }
    }

    // Parses last roll
    if (itemattributes !== null && expr.includes('#{roll}')) {
      expr = expr.replace(/#{roll}/g, itemattributes._lastroll);
    }

    // Parses number of citems
    if (itemattributes !== null && expr.includes('#{num}')) {
      expr = expr.replace(/#{num}/g, number);
    }

    if (itemattributes !== null && expr.includes('#{name}')) {
      expr = expr.replace(/#{name}/g, itemattributes.name);
    }

    expr = expr.toString();

    // PARSE ITEM ATTRIBUTES
    const itemresult = expr.match(/(?<=#\{).*?(?=\})/g);
    if (itemresult !== null && itemattributes !== null) {
      // Substitute string for current value
      for (let i = 0; i < itemresult.length; i += 1) {
        const attname = `#{${itemresult[i]}}`;
        let attvalue;

        if (itemattributes[itemresult[i]] !== null) {
          attvalue = itemattributes[itemresult[i]].value;
        } else {
          // ui.notifications.warn("cItem property " + itemresult[i] + " of cItem " + itemattributes.name +" does not exist");
          attvalue = 0;
        }

        if ((attvalue !== false) && (attvalue !== true)) {
          if ((attvalue === '' || attvalue === null)) {
            attvalue = 0;
          }
        }

        if (attvalue === null) {
          attvalue = 0;
        }

        if (!itemresult[i].includes('#{target|')) {
          expr = expr.replace(attname, attvalue);
        }
      }
    }

    // PARSE ACTOR ATTRIBUTES

    const result = expr.match(/(?<=@\{).*?(?=\})/g);
    if (result !== null) {
      // Substitute string for current value
      for (let i = 0; i < result.length; i += 1) {
        let rawattname = result[i];
        let attProp = 'value';
        let attTotal;

        if (rawattname.includes('.max')) {
          rawattname = rawattname.replace('.max', '');
          attProp = 'max';
        }

        if (rawattname.includes('.totals.')) {
          const splitter = rawattname.split('.');
          rawattname = splitter[0];
          attTotal = splitter[2];
          attProp = 'total';
        }

        const attname = `@{${result[i]}}`;
        let attvalue;

        if (attributes !== null) {
          let myatt = attributes[rawattname];

          if (myatt !== null) {
            if (attTotal !== null && attTotal !== '') {
              myatt = attributes[rawattname].totals[attTotal];
            }

            if (myatt !== null) {
              attvalue = myatt[attProp];
            }
          } else {
            let fromcItem = false;
            let mycitem = '';
            if (itemattributes !== null) {
              fromcItem = true;
              mycitem = ` from citem: ${itemattributes.name}`;
            }

            ui.notifications.warn(`Property ${rawattname}${mycitem} does not exist`);
          }

          if ((attvalue !== false) && (attvalue !== true)) {
            if ((attvalue === '' || attvalue === null)) {
              attvalue = 0;
            }
          }

          if (attvalue === null) {
            attvalue = 0;
          }
        } else {
          attvalue = 0;
        }

        expr = expr.replace(attname, attvalue);
      }
    }

    // PARSE ITEM ATTRIBUTE
    // console.log(expr);
    const attcresult = expr.match(/(?<=--)\S*?(?=--)/g);
    if (attcresult !== null) {
      // Substitute string for current value
      for (let i = 0; i < attcresult.length; i += 1) {
        const attname = `-= 1${attcresult[i]}-= 1`;
        let attvalue;
        if (itemattributes[attcresult[i]] !== null) {
          attvalue = itemattributes[attcresult[i]].value;
        }
        if (attvalue === '' || attvalue === null) {
          attvalue = 0;
        }
        // console.log(attname + " " + attvalue);
        const nonvalid = /,|\[|\]|\(|\)|;/g;
        const nonvalidexpr = attcresult[i].match(nonvalid);

        if (!nonvalidexpr) {
          expr = expr.replace(attname, attvalue);
        }
      }
    }

    // console.log(expr);

    // PARSE ACTOR ATTRIBUTE
    const attpresult = expr.match(/(?<=__)\S*?(?=__)/g);
    if (attpresult !== null) {
      // Substitute string for current value
      for (let i = 0; i < attpresult.length; i += 1) {
        const debugname = attpresult[i];
        // console.log(debugname);
        const attname = `__${attpresult[i]}__`;
        let attvalue = 0;
        if (attributes !== null) {
          if (attributes[attpresult[i]] !== null) {
            attvalue = attributes[attpresult[i]].value;
          }

          //                    if(attvalue=="")
          //                        attvalue = 0;
        }

        const nonvalid = /,|\[|\]|\(|\)|;/g;
        const nonvalidexpr = attpresult[i].match(nonvalid);

        if (!nonvalidexpr) {
          expr = expr.replace(attname, attvalue);
        }
      }
    }

    // NEW SMART PARSING
    let sumsAreNum = false;
    let safetyBreak = 0;

    while (!sumsAreNum) {
      sumsAreNum = true;
      if (safetyBreak > 7) {
        break;
      }

      // PARSE CEIL
      const ceilmatch = /\bceil\(/g;
      let ceilResultArray = ceilmatch.exec(expr);
      const ceilResult = [];

      while (ceilResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(ceilmatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        ceilResult.push(subb);
        ceilResultArray = ceilmatch.exec(expr);
      }

      if (ceilResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < ceilResult.length; i += 1) {
          const ceilExpr = ceilResult[i];
          const tochange = `ceil(${ceilExpr})`;

          const maxpresent = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bfloor\(|\bceil\(|\bcount[E|L|H]\(|\?\[|[a-zA-Z]/g;
          const maxpresentcheck = ceilExpr.match(maxpresent);

          if (!maxpresentcheck) {
            // if(Number.isNaN(ceilExpr)){
            //                            let roll = new Roll(ceilExpr).roll();
            //                            let finalvalue = roll.total;
            //                            expr = expr.replace(tochange,parseInt(finalvalue));

            // eslint-disable-next-line no-eval
            const test = eval(ceilExpr);
            let finalstring = `ceil(${test})`;
            const roll = new Roll(finalstring);
            await roll.evaluate({ async: true });
            finalstring = roll.total;
            expr = expr.replace(tochange, finalstring);
            // }
          }
        }
      }

      // console.log(expr);

      // PARSE FLOOR
      const floormatch = /\bfloor\(/g;
      let floorResultArray = floormatch.exec(expr);
      const floorResult = [];

      while (floorResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(floormatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        floorResult.push(subb);
        floorResultArray = floormatch.exec(expr);
      }

      if (floorResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < floorResult.length; i += 1) {
          const floorExpr = floorResult[i];
          const tochange = `floor(${floorExpr})`;

          const maxpresent = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bfloor\(|\bceil\(|\bcount[E|L|H]\(|\?\[|[a-zA-Z]/g;
          const maxpresentcheck = floorExpr.match(maxpresent);

          if (!maxpresentcheck) {
            if (Number.isNaN(floorExpr)) {
              //                            let roll = new Roll(floorExpr).roll();
              //                            let finalvalue = roll.total;
              //                            expr = expr.replace(tochange,parseInt(finalvalue));
              // console.log(floorExpr);

              // eslint-disable-next-line no-eval
              const test = eval(floorExpr);
              // console.log(test);
              let finalstring = `floor(${test})`;
              const roll = new Roll(finalstring);
              await roll.evaluate({ async: true });
              finalstring = roll.total;
              expr = expr.replace(tochange, finalstring);
            }
          }
        }
      }

      // console.log(expr);

      // PARSE MAX ROLL
      // let maxresult = expr.match(/(?<=\maxdie\().*?(?=\))/g);
      const mxmatch = /maxdie\(/g;
      let maxdieArray = mxmatch.exec(expr);
      const maxDie = [];

      while (maxdieArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(mxmatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        maxDie.push(subb);
        maxdieArray = mxmatch.exec(expr);
      }

      if (maxDie !== null) {
        for (let i = 0; i < maxDie.length; i += 1) {
          const tochange = `maxdie(${maxDie[i]})`;

          const mdieexpr = maxDie[i].split(';');
          const mdieroll = mdieexpr[0];
          let mdiemode = mdieexpr[1];

          if (mdiemode === null || mdiemode === 'true') {
            mdiemode = true;
          } else {
            mdiemode = false;
          }

          const newroll = new Roll(mdieroll);
          await newroll.evaluate({ async: true });

          let attvalue = 0;
          for (let j = 0; j < newroll.dice.length; j += 1) {
            const diceexp = newroll.dice[j];
            if (mdiemode) {
              attvalue += diceexp.results.length * parseInt(diceexp.faces, 10);
            } else {
              attvalue = parseInt(diceexp.faces, 10);
            }
          }

          expr = expr.replace(tochange, attvalue);
        }
      }

      // console.log(expr);

      // MAXOF
      // let maxResult = expr.match(/(?<=\max\().*?(?=\))/g);
      const mrmatch = /\bmax\(/g;
      let maxResultArray = mrmatch.exec(expr);
      const maxResult = [];

      while (maxResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(mrmatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        maxResult.push(subb);
        maxResultArray = mrmatch.exec(expr);
      }

      if (maxResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < maxResult.length; i += 1) {
          // console.log(maxResult[i]);
          const ifpresent = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bceil\(|\bfloor\(|\bcount[E|L|H]\(|\?\[/g;
          const ifpresentcheck = maxResult[i].match(ifpresent);

          if (!ifpresentcheck) {
            const blocks = maxResult[i].split(',');
            let finalvalue = 0;
            const valueToMax = [];
            let nonumber = false;
            for (let n = 0; n < blocks.length; n += 1) {
              let pushblock = blocks[n];
              const nonumsum = /[#@]{|%\[|if\[|\?\[/g;
              const checknonumsum = blocks[n].match(nonumsum);
              // console.log(pushblock);
              if (!checknonumsum) {
                if (Number.isNaN(pushblock)) {
                  const roll = new Roll(blocks[n]);
                  await roll.evaluate({ async: true });
                  pushblock = roll.total;
                }

                valueToMax.push(parseInt(pushblock, 10));
              } else {
                // console.log("nonumber");
                nonumber = true;
              }
            }
            if (!nonumber) {
              finalvalue = Math.max(valueToMax);
              const tochange = `max(${maxResult[i]})`;
              expr = expr.replace(tochange, parseInt(finalvalue, 10));
            } else {
              sumsAreNum = false;
            }
          } else {
            sumsAreNum = false;
          }
        }
      }

      // MINOF
      // let minResult = expr.match(/(?<=\min\().*?(?=\))/g);
      const minmatch = /\bmin\(/g;
      let minResultArray = minmatch.exec(expr);
      const minResult = [];

      while (minResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(minmatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        minResult.push(subb);
        minResultArray = minmatch.exec(expr);
      }

      if (minResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < minResult.length; i += 1) {
          const ifpresent = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bceil\(|\bfloor\(|\bcount[E|L|H]\(|\?\[/g;
          const ifpresentcheck = minResult[i].match(ifpresent);

          if (!ifpresentcheck) {
            const blocks = minResult[i].split(',');
            let finalvalue;
            const valueToMin = [];
            let nonumber = false;

            for (let n = 0; n < blocks.length; n += 1) {
              let pushblock = blocks[n];
              const nonumsum = /[#@]{|%\[|if\[|\?\[/g;
              const checknonumsum = blocks[n].match(nonumsum);

              if (!checknonumsum) {
                if (Number.isNaN(pushblock)) {
                  const roll = new Roll(blocks[n]);
                  await roll.evaluate({ async: true });
                  pushblock = roll.total;
                }

                valueToMin.push(parseInt(pushblock, 10));
              } else {
                nonumber = true;
              }
            }
            if (!nonumber) {
              finalvalue = Math.min(valueToMin);
              const tochange = `min(${minResult[i]})`;
              expr = expr.replace(tochange, parseInt(finalvalue, 10));
            } else {
              sumsAreNum = false;
            }
          } else {
            sumsAreNum = false;
          }
        }
      }

      // COUNTIF
      // console.log(expr);
      // let countIfResult = expr.match(/(?<=\bcountE\b\().*?(?=\))/g);
      const cifmatch = /\bcountE\(/g;
      let countIfResultArray = cifmatch.exec(expr);
      const countIfResult = [];

      while (countIfResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(cifmatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        countIfResult.push(subb);
        countIfResultArray = cifmatch.exec(expr);
      }

      if (countIfResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < countIfResult.length; i += 1) {
          //                let debugname = attpresult[i];

          const splitter = countIfResult[i].split(';');
          const comparer = countIfResult[i].replace(`${splitter[0]};`, '');
          const blocks = splitter[0].split(',');
          let finalvalue = 0;
          const valueIf = [];
          let nonumber = false;

          for (let n = 0; n < blocks.length; n += 1) {
            if (!Number.isNaN(blocks[n])) {
              valueIf.push(parseInt(blocks[n], 10));
            } else {
              nonumber = true;
            }
          }

          if (!nonumber) {
            for (let j = 0; j < valueIf.length; j += 1) {
              // console.log(valueIf[j] + " " + comparer)
              if (parseInt(valueIf[j], 10) === parseInt(comparer, 10)) {
                finalvalue += 1;
              }
            }

            const tochange = `countE(${countIfResult[i]})`;
            expr = expr.replace(tochange, parseInt(finalvalue, 10));
          } else {
            sumsAreNum = false;
          }
        }
      }
      // console.log(expr);

      // COUNTHIGHER
      // let countHighResult = expr.match(/(?<=\bcountH\b\().*?(?=\))/g);
      const chimatch = /\bcountH\(/g;
      let countHighResultArray = chimatch.exec(expr);
      const countHighResult = [];

      while (countHighResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(chimatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        countHighResult.push(subb);
        countHighResultArray = chimatch.exec(expr);
      }

      if (countHighResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < countHighResult.length; i += 1) {
          //                let debugname = attpresult[i];

          const splitter = countHighResult[i].split(';');
          // let comparer = splitter[1];
          const comparer = countHighResult[i].replace(`${splitter[0]};`, '');
          const blocks = splitter[0].split(',');
          let finalvalue = 0;
          const valueIf = [];
          let nonumber = false;
          for (let n = 0; n < blocks.length; n += 1) {
            if (!Number.isNaN(blocks[n])) {
              valueIf.push(parseInt(blocks[n], 10));
            } else {
              nonumber = true;
            }
          }
          if (!nonumber) {
            for (let j = 0; j < valueIf.length; j += 1) {
              if (valueIf[j] > comparer) {
                finalvalue += 1;
              }
            }

            const tochange = `countH(${countHighResult[i]})`;
            expr = expr.replace(tochange, parseInt(finalvalue, 10));
          } else {
            sumsAreNum = false;
          }
        }
      }

      // COUNTLOWER
      // let countLowResult = expr.match(/(?<=\bcountL\b\().*?(?=\))/g);
      const clomatch = /\bcountL\(/g;
      let countLowResultArray = clomatch.exec(expr);
      const countLowResult = [];

      while (countLowResultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(clomatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        countLowResult.push(subb);
        countLowResultArray = clomatch.exec(expr);
      }

      if (countLowResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < countLowResult.length; i += 1) {
          //                let debugname = attpresult[i];

          const splitter = countLowResult[i].split(';');
          // let comparer = parseInt(splitter[1]);
          const comparer = countLowResult[i].replace(`${splitter[0]};`, '');
          const blocks = splitter[0].split(',');
          let finalvalue = 0;
          const valueIf = [];

          let nonumber = false;
          for (let n = 0; n < blocks.length; n += 1) {
            if (!Number.isNaN(blocks[n])) {
              valueIf.push(parseInt(blocks[n], 10));
            } else {
              nonumber = true;
            }
          }
          if (!nonumber) {
            for (let j = 0; j < valueIf.length; j += 1) {
              if (valueIf[j] < comparer) {
                finalvalue += 1;
              }
            }

            const tochange = `countL(${countLowResult[i]})`;
            expr = expr.replace(tochange, parseInt(finalvalue, 10));
          } else {
            sumsAreNum = false;
          }
        }
      }

      // SUM
      // let sumResult = expr.match(/(?<=\bsum\b\().*?(?=\))/g);
      const summatch = /\bsum\(/g;
      let sumResultResultArray = summatch.exec(expr);
      const sumResult = [];

      while (sumResultResultArray) {
        const suba = expr.substring(summatch.lastIndex, expr.length);
        const subb = auxMeth.getParenthesString(suba);
        sumResult.push(subb);
        sumResultResultArray = summatch.exec(expr);
      }

      if (sumResult !== null) {
        // Substitute string for current value
        for (let i = 0; i < sumResult.length; i += 1) {
          //                let debugname = attpresult[i];

          const splitter = sumResult[i].split(';');
          const comparer = splitter[1];
          const blocks = splitter[0].split(',');
          let finalvalue = 0;
          const valueIf = [];
          let nonumber = false;
          const nonumsum = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bceil\(|\bfloor\(|\bcount[E|L|H]\(|\?\[/g;
          const hassubfunctions = sumResult[i].match(nonumsum);

          if (!hassubfunctions) {
            for (let n = 0; n < blocks.length; n += 1) {
              const checknonumsum = blocks[n].match(nonumsum);
              // console.log(blocks[n])
              if ((checknonumsum === null)) {
                let sumExpr = blocks[n];
                if (Number.isNaN(blocks[n])) {
                  // eslint-disable-next-line no-eval
                  sumExpr = eval(sumExpr);
                }

                finalvalue += parseInt(sumExpr, 10);
              } else {
                nonumber = true;
              }
            }
          } else {
            nonumber = true;
          }

          if (!nonumber) {
            const tochange = `sum(${sumResult[i]})`;
            expr = expr.replace(tochange, parseInt(finalvalue, 10));
          } else {
            sumsAreNum = false;
          }
        }
      }

      // PARSE SCALED AUTO VALUES
      // let scaleresult = expr.match(/(?<=%\[).*?(?=\])/g);
      const scmatch = /%\[/g;
      let scaleresultArray = scmatch.exec(expr);
      const scaleresult = [];

      while (scaleresultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(scmatch.lastIndex, expr.length);
        const subb = auxMeth.getBracketsString(suba);
        scaleresult.push(subb);
        scaleresultArray = scmatch.exec(expr);
      }

      if (scaleresult !== null && scaleresult.length > 0) {
        // Substitute string for current value
        for (let i = scaleresult.length - 1; i >= 0; i -= 1) {
          const nonvalidscale = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bceil\(|\bfloor\(|\bcount[E|L|H]\(|\?\[/g;
          const nonvalidscalecheck = scaleresult[i].match(nonvalidscale);

          if (!nonvalidscalecheck) {
            const limits = scaleresult[i].split(',');
            let value = limits[0];
            if (Number.isNaN(value) && !value.includes('$') && !value.includes('min') && !value.includes('max')) {
              const roll = new Roll(limits[0]);
              await roll.evaluate({ async: true });
              value = roll.total;
            }

            let valuemod = 0;

            const limitArray = [];

            for (let j = 1; j < limits.length; j += 1) {
              const splitter = limits[j].split(':');
              let scale = splitter[0];

              const noncondition = /\bif\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bfloor\(|\bceil\(|\bcount[E|L|H]\(|\?\[|[+\-*/]/g;
              const nonconditioncheck = scale.match(noncondition);

              if (nonconditioncheck) {
                //
                //                            }
                //
                //                            if(Number.isNaN(scale)  && !scale.includes("$") && !scale.includes("min") && !scale.includes("max") ){
                // if(Number.isNaN(scale) || scale.includes('+')|| scale.includes('-')|| scale.includes('/')|| scale.includes('*')){
                const newroll = new Roll(scale);
                await newroll.evaluate({ async: true });
                // expr = expr.replace(scale,newroll.total);
                scale = newroll.total;
              }

              const limitEl = {};
              limitEl.scale = scale;
              limitEl.value = splitter[1];
              limitArray.push(limitEl);
            }

            limitArray.sort((x, y) => x.scale - y.scale);
            valuemod = limitArray[0].value;

            for (let k = 0; k < limitArray.length; k += 1) {
              const checker = limitArray[k];
              const checkscale = Number(checker.scale);

              if (value >= checkscale) {
                valuemod = checker.value;
              }
            }

            if (Number.isNaN(valuemod)) {
              const nonum = /[#@]{|%\[|if\[/g;
              const checknonum = valuemod.match(nonum);

              if (checknonum !== null) {
                sumsAreNum = false;
              }
            }

            const attname = `%[${scaleresult[i]}]`;
            expr = expr.replace(attname, valuemod);
          } else {
            sumsAreNum = false;
          }
        }
      }

      // PARSE CONDITIONAL
      // let ifresult = expr.match(/(?<=if\[).*?(?=\])/g);
      const ifmatch = /if\[/g;
      let ifresultArray = ifmatch.exec(expr);
      const ifresult = [];

      while (ifresultArray) {
        // console.log(maxResultArray.index + ' ' + mrmatch.lastIndex);
        const suba = expr.substring(ifmatch.lastIndex, expr.length);
        const subb = auxMeth.getBracketsString(suba);
        ifresult.push(subb);
        ifresultArray = ifmatch.exec(expr);
      }

      if (ifresult !== null) {
        // Substitute string for current value
        for (let i = ifresult.length - 1; i >= 0; i -= 1) {
          const nonvalidif = /if\[|\bmax\(|\bmin\(|\bsum\(|%\[|\bceil\(|\bfloor\(|\bcount[E|L|H]\(|\?\[/g;
          const nonvalidifcheck = ifresult[i].match(nonvalidif);

          if (!nonvalidifcheck) {
            const nonumber = false;
            const limits = ifresult[i].split(',');
            const generalCond = limits[0];
            const truevalue = limits[1];

            let falsevalue = limits[2];
            let dontparse = false;
            falsevalue = falsevalue.replace('ELSE ', '');

            let checknonumcond;
            let nonumcond;

            let finalvalue = falsevalue;

            const findOR = generalCond.search(' OR ');
            const findAND = generalCond.search(' AND ');

            let orconditions;
            let andconditions;

            if (findOR !== -1) {
              orconditions = generalCond.split(' OR ');
              for (let j = 0; j < orconditions.length; j += 1) {
                const conditions = orconditions[j].split(':');
                let thiscondition = conditions[0];
                let checker = conditions[1];

                if (thiscondition === 'true' || thiscondition === 'false') {
                  thiscondition = (thiscondition === 'true');
                }

                if (checker === 'true' || checker === 'false') {
                  checker = (checker === 'true');
                }

                if (Number.isNaN(checker)) {
                  try {
                    const newroll = new Roll(checker);
                    await newroll.evaluate({ async: true });
                    checker = newroll.total;
                  } catch (err) {
                    console.error(err);
                  }
                }

                if (Number.isNaN(thiscondition)) {
                  nonumcond = /\+|-|\\|\*/g;
                  checknonumcond = thiscondition.match(nonumcond);
                }

                if (Number.isNaN(thiscondition) || checknonumcond !== null) {
                  try {
                    const newroll = new Roll(thiscondition);
                    await newroll.evaluate({ async: true });
                    thiscondition = newroll.total;
                  } catch (err) {
                    console.error(err);
                  }
                }

                if (thiscondition === checker) {
                  finalvalue = truevalue;
                }
              }
            } else if (findAND !== -1) {
              // console.log("AND");
              andconditions = generalCond.split(' AND ');
              finalvalue = truevalue;
              for (let j = 0; j < andconditions.length; j += 1) {
                const conditions = andconditions[j].split(':');
                let thiscondition = conditions[0];
                let checker = conditions[1];

                if (thiscondition === 'true' || thiscondition === 'false') {
                  thiscondition = (thiscondition === 'true');
                }

                if (checker === 'true' || checker === 'false') {
                  checker = (checker === 'true');
                }

                if (Number.isNaN(checker)) {
                  try {
                    const newroll = new Roll(checker);
                    await newroll.evaluate({ async: true });
                    checker = newroll.total;
                  } catch (err) {
                    dontparse = true;
                  }
                }

                if (Number.isNaN(thiscondition)) {
                  nonumcond = /\+|-|\\|\*/g;
                  checknonumcond = thiscondition.match(nonumcond);
                }

                if (Number.isNaN(thiscondition) || checknonumcond !== null) {
                  try {
                    const newroll = new Roll(thiscondition);
                    await newroll.evaluate({ async: true });
                    thiscondition = newroll.total;
                  } catch (err) {
                    dontparse = true;
                  }
                }

                // console.log(thiscondition + " " + checker);

                if (thiscondition !== checker) {
                  finalvalue = falsevalue;
                }
              }
            } else {
              // console.log("NONE");

              const conditions = generalCond.split(':');
              let thiscondition = conditions[0];
              let checker = conditions[1];
              // console.log(conditions);
              // console.log(checker);

              if (thiscondition === 'true' || thiscondition === 'false') {
                thiscondition = (thiscondition === 'true');
              }

              if (checker === 'true' || checker === 'false') {
                checker = (checker === 'true');
              }

              // console.log(thiscondition + " " + checker);

              if (Number.isNaN(checker)) {
                try {
                  //                                    let newroll = new Roll(checker);
                  //                                    await newroll.evaluate({async: true});
                  //                                    checker = newroll.total;

                  // eslint-disable-next-line no-eval
                  checker = eval(checker);
                } catch (err) {
                  dontparse = true;
                }
              }

              if (Number.isNaN(thiscondition)) {
                nonumcond = /\+|-|\\|\*/g;
                checknonumcond = thiscondition.match(nonumcond);
              }
              // console.log(thiscondition + " " + checker);

              if (Number.isNaN(thiscondition) || checknonumcond !== null) {
                try {
                  //                                    let newroll = new Roll(thiscondition);
                  //                                    await newroll.evaluate({async: true});
                  //                                    thiscondition = newroll.total;
                  // eslint-disable-next-line no-eval
                  checker = eval(checker);
                } catch (err) {
                  dontparse = true;
                }
              }

              // console.log(thiscondition + " " + checker);

              if (thiscondition.toString() === checker.toString()) {
                finalvalue = truevalue;
              }
            }

            // console.log(finalvalue);

            const attname = `if[${ifresult[i]}]`;

            const nonum = /[#@]{|%\[|if\[|\?\[/g;
            const checknonumtrue = falsevalue.match(nonum);
            const checknonumfalse = truevalue.match(nonum);

            if (checknonumtrue !== null || checknonumfalse !== null) {
              sumsAreNum = false;
            } else {
              expr = expr.replace(attname, finalvalue);
            }
          } else {
            sumsAreNum = false;
          }
        }
      }

      // console.log(expr);
      // MATH and ARITHMETIC CORRECTIONS
      const plusmin = /\+-/g;
      expr = expr.replace(plusmin, '-');
      const minmin = /--/g;
      expr = expr.replace(minmin, '+');
      const commazero = /,\s-\b0|,-\b0/g;
      expr = expr.replace(commazero, ',0');
      // let pluszero = /\+\s\b0|\+\b0/g;
      // expr = expr.replace(pluszero, "");
      // let minuszero = /\-\s\b0|\-\b0/g;
      // expr = expr.replace(minuszero, "");
      // console.log(expr);

      const zeroexplode = /0x0/g;
      expr = expr.replace(zeroexplode, '0');

      safetyBreak += 1;
    }

    // console.log(expr);
    // console.log(exprmode);

    // console.log("finished parsed")
    // console.log(expr);

    if (expr.charAt(0) === '|') {
      exprmode = true;
      expr = expr.replace('|', '');
    }

    toreturn = expr;

    if (Number.isNaN(expr)) {
      // console.log("nonumber");
      if (!exprmode) {
        // console.log("exprmode=false")
        try {
          const final = new Roll(expr);
          await final.evaluate({ async: true });

          // final.roll();
          // console.log(final);

          if (Number.isNaN(final.total) || final.total === null || final.total === false) {
            toreturn = expr;
          } else {
            toreturn = final.total;
          }

          // console.log(toreturn);
        } catch (err) {
          // console.log("Following Roll expression can not parse to number. String returned");
          // console.log(expr);
          // ui.notifications.warn("Roll expression can not parse to number");
          toreturn = expr;
        }
      } else {
        // PARSE BOOL
        if (expr === 'false') {
          expr = false;
        }

        if (expr === 'true') {
          expr = true;
        }

        toreturn = expr;
      }
    } else if (exprmode) {
      toreturn = expr;
    }
    // console.log(toreturn);
    return toreturn;
  }

  static getParenthesString(expr) {
    let openpar = 0;
    let closedpar = -1;
    let parsed = false;
    let finalexpr = '';

    for (let i = 0; i < expr.length; i += 1) {
      if (!parsed) {
        if (expr.charAt(i) === '(') {
          openpar += 1;
        }
        if (expr.charAt(i) === ')') {
          closedpar += 1;
        }

        if (openpar === closedpar) {
          parsed = true;
        } else {
          finalexpr += expr.charAt(i);
        }
      }
    }

    return finalexpr;
  }

  static getBracketsString(expr) {
    let openpar = 0;
    let closedpar = -1;
    let parsed = false;
    let finalexpr = '';

    for (let i = 0; i < expr.length; i += 1) {
      if (!parsed) {
        if (expr.charAt(i) === '[') {
          openpar += 1;
        }
        if (expr.charAt(i) === ']') {
          closedpar += 1;
        }

        if (openpar === closedpar) {
          parsed = true;
        } else {
          finalexpr += expr.charAt(i);
        }
      }
    }

    return finalexpr;
  }

  static dynamicSort(property) {
    let sortOrder = 1;
    if (property[0] === '-') {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      // eslint-disable-next-line no-nested-ternary
      const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    };
  }

  static aTdynamicSort(property, datatype) {
    let sortOrder = 1;
    if (property[0] === '-') {
      sortOrder = -1;
      property = property.substr(1);
    }

    return (a, b) => {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      if (hasProperty(a.attributes[property], 'value')) {
        let valA = a.attributes[property].value;
        let valB = b.attributes[property].value;

        if (datatype === 'simplenumeric') {
          valA = Number(valA);
          valB = Number(valB);
        }

        // eslint-disable-next-line no-nested-ternary
        const result = (valA < valB) ? -1 : (valA > valB) ? 1 : 0;
        return result * sortOrder;
      }

      return 1;
    };
  }

  static async rollToMenu(html = null) {
    if (!game.settings.get('sandbox', 'showLastRoll')) {
      return;
    }

    const hotbar = document.getElementsByClassName('dcroll-bar');
    if (!hotbar.length) {
      return;
    }

    // hotbar[0].className = "flexblock-left-nopad";

    const prevmenu = $(hotbar).find('.roll-menu');
    if (prevmenu !== null) {
      prevmenu.remove();
    }

    const tester = document.createElement('DIV');

    if (html === null) {
      let lastmessage;
      let found = false;

      for (let i = game.messages.size - 1; i >= 0; i -= 1) {
        const amessage = game.messages.contents[i];
        if (!found) {
          if (amessage.data.content.includes('roll-template')) {
            found = true;
            lastmessage = amessage;
          }
        }
      }

      if (lastmessage === null) {
        return;
      }

      const msgContent = lastmessage?.data?.content;
      tester.innerHTML = msgContent;
    } else {
      tester.innerHTML = html;
    }

    const trashcan = tester.getElementsByClassName('roll-delete-button');
    if (trashcan.length) {
      const first = trashcan[0];
      if (first.style !== null) {
        first.style.display = 'none';
      }
    }

    const rollextra = tester.querySelector('.roll-extra');
    if (rollextra) {
      rollextra.style.display = 'none';
    }

    const rollMenu = document.createElement('DIV');
    rollMenu.className = 'roll-menu';
    rollMenu.innerHTML = tester.innerHTML;
    hotbar[0].appendChild(rollMenu);
  }
}

export default auxMeth;
