/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
class multiPanel extends ItemSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['sandbox', 'sheet', 'item'],
      width: 520,
      height: 480,
      tabs: [{ navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'description' }],
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get template() {
    const path = 'systems/sandbox/templates/';
    return `${path}/${this.item.data.type}.html`;
  }

  /** @override */
  async getData() {
    const { item } = this;
    const data = super.getData();
    data.flags = item.data.flags;

    // Recheck contents
    if (!hasProperty(data.flags, 'panelarray')) {
      setProperty(data.flags, 'panelarray', []);
    }

    try {
      const panels = JSON.parse(item.data.data.panels);
      data.flags.panelarray = panels;
    } catch (err) {
      console.error(err);
    }

    this.checkItemsExisting();

    return data;
  }

  /* -------------------------------------------- */

  /** @override */

  activateListeners(html) {
    super.activateListeners(html);

    // Activate tabs
    const tabs = html.find('.tabs');
    const initial = this._sheetTab;
    const _ = new Tabs(tabs, {
      initial,
      callback: (clicked) => {
        this._sheetTab = clicked.data('tab');
      },
    });

    // Drag end event TEST
    //    let handler = ev => this._onEntityDragEnd(ev);
    //    document.addEventListener('dragend', handler);

    // Drop Event TEST
    this.form.ondrop = (ev) => this._onDrop(ev);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) {
      return;
    }

    const panels = this.item.data.flags.panelarray;

    // Update Inventory Item
    html.find('.item-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.property');
      const panel = panels[li.data('itemId')];
      const item = game.items.get(panel._id);
      this.updatePanels();
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click((ev) => {
      const li = $(ev.currentTarget).parents('.property');
      const panel = panels.splice(li.data('itemId'), 1);
      this.updatePanels();
      li.slideUp(200, () => this.render(false));
    });

    // Implementation of up/down arrows!!
    // Top Item
    html.find('.item-top').click((ev) => {
      const li = $(ev.currentTarget).parents('.property');
      const itemindex = li.data('itemId');
      if (itemindex > 0) { panels.splice(itemindex - 1, 0, panels.splice(itemindex, 1)[0]); }
      this.updatePanels();
    });

    // Bottom Item
    html.find('.item-bottom').click((ev) => {
      const li = $(ev.currentTarget).parents('.property');
      const itemindex = li.data('itemId');
      if (itemindex < panels.length - 1) { panels.splice(itemindex + 1, 0, panels.splice(itemindex, 1)[0]); }
      this.updatePanels();
    });
  }

  async checkItemsExisting() {
    const panels = this.item.data.flags.panelarray;
    let changed = false;

    for (let i = 0; i < panels.length; i += 1) {
      if (!game.items.get(panels[i]._id)) {
        const index = panels.indexOf(panels[i]);
        if (index > -1) {
          panels.splice(index, 1);
          changed = true;
        }
      }
    }

    if (changed) { this.updatePanels(); }
  }

  async _onDrop(event) {
    // Initial checks
    event.preventDefault();
    event.stopPropagation();

    let dropitem;

    try {
      const dropdata = JSON.parse(event.dataTransfer.getData('text/plain'));
      dropitem = game.items.get(dropdata.id);

      // You can only drop panels
      if (dropitem.data.type !== 'panel') {
        return Promise.reject();
      }
    } catch (err) {
      console.error('ItemCollection | drop error');
      console.error(event.dataTransfer.getData('text/plain'));
      console.error(err);
      return false;
    }

    // Add property id to panel
    const panels = this.item.data.flags.panelarray;
    for (let i = 0; i < panels.length; i += 1) {
      if (panels[i]._id === dropitem.data._id) {
        return Promise.reject();
      }
    }

    panels.push(dropitem);
    this.updatePanels();

    return true;
  }

  async updatePanels() {
    await this.item.update({ 'data.panels': JSON.stringify(this.item.data.flags.panelarray) });
    this.item.sheet.render(true);
  }

  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {
    super._updateObject(event, formData);
  }
}

export default multiPanel;
