import auxMeth from './auxmeth.js';

class gActor extends Actor {
  prepareData() {
    super.prepareData();

    // Get the Actor's data object
    const actorData = this.data;
    const { data } = actorData;
    const { flags } = actorData;

    if (!hasProperty(flags, 'ischeckingauto')) {
      setProperty(flags, 'ischeckingauto', false);
    }

    if (!hasProperty(flags, 'hasupdated')) {
      setProperty(flags, 'hasupdated', true);
    }

    if (!hasProperty(flags, 'scrolls')) {
      setProperty(flags, 'scrolls', {});
    }

    // Prepare Character data
    if (data.istemplate) {
      if (!hasProperty(flags, 'tabarray')) {
        setProperty(flags, 'tabarray', []);
      }

      if (!hasProperty(flags, 'rows')) {
        setProperty(flags, 'rows', 0);
        setProperty(flags, 'rwidth', 0);
      }
    }

    if (!hasProperty(flags, 'sandbox')) {
      setProperty(flags, 'sandbox', {});
    }

    if (!hasProperty(flags.sandbox, `scrolls_${game.user.id}_${this.id}`)) {
      setProperty(flags.sandbox, `scrolls_${game.user.id}_${this.id}`, 0);
    }
  }

  prepareDerivedData() {
    if (!hasProperty(this.data.flags, 'sbupdated')) {
      setProperty(this.data.flags, 'sbupdated', 0);
    }

    if (!hasProperty(this.data.data, 'biovisible')) {
      setProperty(this.data.data, 'biovisible', false);
    }
  }

  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    if (data.data !== null) {
      if (data.data.istemplate) {
        this.data.update({ 'data.istemplate': false });
      }
    }
  }

  async _preUpdate(updateData, options, userId) {
    //        let upclon = duplicate(updateData);
    if (
      this.data.permission.default >= CONST.ENTITY_PERMISSIONS.OBSERVER
      || this.data.permission[game.user.id] >= CONST.ENTITY_PERMISSIONS.OBSERVER
      || game.user.isGM
    ) {
      // const myuser = userId;
    } else {
      return;
    }

    let noTemplate = true;
    if (updateData.istemplate) {
      noTemplate = false;
    }

    if (!this.data.data.istemplate && updateData.data !== null && noTemplate) {
      const actor = duplicate(this.data);
      // let newtoken;

      // I AM TRYING TO MERGE UPDATE DATA TO A DUPLICATE ACTOR, TO RETURN THE RESULTING ACTOR
      const uData = await this.getFinalActorData(actor, updateData.data);

      // HERE I APPLY THE AUTO CALCULATIONS TO THE RETURNED ACTOR
      const adata = await this.actorUpdater(uData);

      // COMPARES RETURNED ACTOR DATA TO THE ORIGINAL UPDATEDATA, AND ADDS WHATERVER IS MISSING
      const newattributes = await this
        .compareKeys(this.data.data.attributes, adata.data.attributes);
      const newcitems = await this.comparecItems(this.data.data.citems, adata.data.citems);
      const newrolls = await this.compareValues(this.data.data.rolls, adata.data.rolls);

      const maindata = updateData.data;

      setProperty(maindata, 'selector', adata.data.selector);

      if (newattributes) {
        setProperty(maindata, 'attributes', newattributes);
      }

      if (newcitems.length > 0) {
        setProperty(maindata, 'citems', newcitems);
      } else if (updateData.data.citems) {
        updateData.data.citems = adata.data.citems;
      }

      if (newrolls) {
        setProperty(maindata, 'rolls', newrolls);
      }
    }

    super._preUpdate(updateData, options, userId);
  }

  async createAttProps(actorData, updateData, key) {
    const n = actorData.attributes;
    n[key] = {};
    const c = actorData.attributes[key];

    const a = updateData.attributes[key];
    Object.keys(a).forEach((f) => {
      const b = a[f];
      c[f] = b;
    });
  }

  async getFinalActorData(actor, updateData) {
    const actorData = actor.data;

    // MERGE ATTRIBUTES
    let attributes;
    const attKeys = Object.keys(actorData.attributes);

    if (updateData.attributes) {
      ({ attributes } = updateData);
    }

    if (updateData.istemplate) {
      actorData.istemplate = true;
    }

    if (updateData.gtemplate) {
      actorData.gtemplate = updateData.gtemplate;
    }

    // console.log(actorData.attributes);
    Object.keys(attributes).forEach(async (key) => {
      if (attKeys.includes(key)) {
        if (attributes[key].id !== null) {
          actorData.attributes[key].id = attributes[key].id;
        }

        // Checkbox group implementation
        if (attributes[key].checkgroup !== null) {
          actorData.attributes[key].checkgroup = attributes[key].checkgroup;
        }

        // if (actorData.attributes[key].checkgroup !== null) {
        //     if (attributes[key].value === "on")
        //         attributes[key].value = true;
        // }

        // Checkbox group implementation
        if (actorData.attributes[key].checkgroup !== null && attributes[key].modified) {
          if (actorData.attributes[key].checkgroup !== '' && !actorData.attributes[key].value) {
            const { checkgroup } = actorData.attributes[key];
            const chkgroupArray = checkgroup.split(';');

            Object.entries(actorData.attributes).forEach(async (propKey, propValues) => {
              const propKeyObj = game.items.find((y) => y.data.data.attKey === propKey);
              if (propKeyObj.data.data.datatype === 'checkbox' && propKey !== key) {
                const pointerchkgroupArray = propKeyObj.data.data.checkgroup.split(';');
                for (let z = 0; z < chkgroupArray.length; z += 1) {
                  const checkKey = chkgroupArray[z];
                  const parsedKey = await auxMeth.autoParser(checkKey, actorData.attributes, null, true);
                  if (pointerchkgroupArray.includes(parsedKey)) {
                    propValues.value = false;
                  }
                }
              }
            });

            // if (updateData.citems) {
            //   const checkProps = async (propKey, propValues) => {
            //     if (propKey !== propObj.data.data.attKey) {
            //       const propKeyObj = game.items.find((y) => y.data.data.attKey === propKey);
            //       if (propKeyObj !== null) {
            //         if (propKeyObj !== '') {
            //           const pointerchkgroupArray = propKeyObj.data.data.checkgroup.split(';');
            //           for (let z = 0; z < chkgroupArray.length; z += 1) {
            //             const checkKey = chkgroupArray[z];
            //             const parsedKey = await auxMeth.autoParser(checkKey, this.actor.data.data.attributes, citem.attributes, true);
            //             if (pointerchkgroupArray.includes(parsedKey)) {
            //               propValues.value = false;
            //             }
            //           }
            //         }
            //       }
            //     }
            //   };

            //   for (let r = 0; r < updateData.citems.length; r += 1) {
            //     Object.entries(updateData.citems[r].attributes).forEach(checkProps);
            //   }
            // }
          }
        }

        if (attributes[key].value !== null) {
          actorData.attributes[key].value = attributes[key].value;
        }

        if (attributes[key].modified !== null) {
          actorData.attributes[key].modified = attributes[key].modified;
        }
        if (attributes[key].max !== null) {
          actorData.attributes[key].max = attributes[key].max;
        }
        if (attributes[key].modmax !== null) {
          actorData.attributes[key].modmax = attributes[key].modmax;
        }
        if (attributes[key].maxadd !== null) {
          actorData.attributes[key].maxadd = attributes[key].maxadd;
        }

        if (attributes[key].listedit !== null) {
          actorData.attributes[key].listedit = attributes[key].listedit;
        }

        if (actorData.attributes[key].istable && hasProperty(attributes[key], 'tableitems')) {
          actorData.attributes[key].tableitems = attributes[key].tableitems;
        }

        if (actorData.attributes[key].istable && hasProperty(attributes[key], 'totals')) {
          actorData.attributes[key].totals = attributes[key].totals;
        }
      } else {
        await this.createAttProps(actorData, updateData, key);
      }
    });

    // MERGE ROLLS
    let rolls;
    const rollKeys = Object.keys(actorData.rolls);
    if (updateData.rolls) {
      ({ rolls } = updateData);
    }

    Object.keys(rolls).forEach((key) => {
      if (rollKeys.includes(key)) {
        actorData.rolls[key].value = rolls[key].value;
      }
    });

    // MERGE CITEMS
    let citems;

    if (updateData.citems) {
      ({ citems } = updateData);
      actorData.citems = citems;
    }

    return actor;
  }

  async compareValues(data1, data2) {
    const result = {};
    const keys = Object.keys(data1);

    Object.keys(data2).forEach((key) => {
      if (!keys.includes(key) || data1[key].value !== data2[key].value) {
        result[key] = {};
        result[key].value = data2[key].value;
      }
    });

    return result;
  }

  async compareKeys(data1, data2) {
    const result = {};
    Object.keys(data2).forEach((key) => {
      let noProp = false;

      if (data1[key] === null) {
        noProp = true;
      }

      const datum2 = data2[key];
      Object.keys(datum2).forEach((subkey) => {
        let createKey = false;
        // const noSubKey = false;

        if (!noProp) {
          if (data1[key][subkey] === null) {
            createKey = true;
          } else if (data1[key][subkey] !== data2[key][subkey]) {
            createKey = true;
          }
        } else {
          createKey = true;
        }

        if (createKey) {
          if (result[key] === null) {
            result[key] = {};
          }

          if (data2[key][subkey] !== null) {
            result[key][subkey] = data2[key][subkey];
          }
        }
      });
    });

    return result;
  }

  async comparecItems(data1, data2) {
    const result = [];
    // const keys = Object.keys(data1);

    for (let i = 0; i < data2.length; i += 1) {
      if (!data1.includes(data2[i])) {
        result.push(data2[i]);
      } else if (data2[i] !== data1[i]) {
        result.push(data2[i]);
      }
    }

    return result;
  }

  async listSheets() {
    const templates = await auxMeth.getSheets();
    this.data.data.sheets = templates;

    // let charsheet = document.getElementById("actor-"+this.id);

    let charsheet;
    if (this.token === null) {
      charsheet = document.getElementById(`actor-${this.id}`);
    } else {
      charsheet = document.getElementById(`actor-${this.id}-${this.token.data.id}`);
    }

    const sheets = charsheet.getElementsByClassName('selectsheet');
    if (sheets === null) {
      return;
    }

    const selector = sheets[0];
    if (selector === null) {
      return;
    }

    const { length } = selector.options;

    for (let j = length - 1; j >= 0; j -= 1) {
      selector.options[j] = null;
    }

    for (let k = 0; k < templates.length; k += 1) {
      const opt = document.createElement('option');
      opt.appendChild(document.createTextNode(templates[k]));
      opt.value = templates[k];
      selector.appendChild(opt);
    }

    selector.value = this.data.data.gtemplate;
  }

  async updateModifiedData(originaldata, extradata) {
    const existingData = await duplicate(originaldata);

    Object.keys(extradata).forEach((prop) => {
      if (extradata[prop] === null || extradata[prop] === undefined) {
        delete extradata[prop];
      }
      existingData[prop] = extradata[prop];
    });

    const newData = await this.actorUpdater(existingData);
    return newData;
  }

  // Overrides update method
  async update(data, options = {}) {
    let newdata = {};
    let scrollTop;

    if (data !== null) {
      if (data['data.citems'] !== null) {
        newdata = {};
        setProperty(newdata, 'data', {});
        newdata.data.citems = data['data.citems'];
      } else {
        newdata = data;
      }

      if (data.biovisible !== null) {
        options.diff = false;
      }

      // newdata["flags.sandbox." + "scrolls_" + game.user.id + "_" + this.id] = await this.sheet.scrollbarSet;
    }

    if (this.data.data.gtemplate !== null) {
      if (newdata === null) {
        setProperty(newdata, 'flags', {});
      }

      if (!newdata.flags) {
        const flag = `scrolls_${game.user.id}_${this.id}`;
        newdata[`flags.sandbox.${flag}`] = scrollTop;
      } else {
        if (!hasProperty(newdata.flags, 'sandbox')) {
          setProperty(newdata.flags, 'sandbox', {});
        }
        newdata.flags.sandbox[`scrolls_${game.user.id}_${this.id}`] = scrollTop;
      }
    }

    return super.update(newdata, options);
  }

  async addcItem(ciTem, addedBy = null, data = null, number = null) {
    if (data === null) {
      ({ data } = this);
    }

    // const newdata = duplicate(data);
    let { citems } = data.data;
    if (citems === null || citems.length === 0) {
      citems = [];
    }

    const { attributes } = data.data;
    let citemData;

    if (!hasProperty(ciTem.data.data.groups)) {
      citemData = ciTem.data._source.data;
    } else {
      citemData = ciTem.data.data;
    }

    const itemKey = '';
    const newItem = {};

    setProperty(newItem, itemKey, {});

    newItem[itemKey].id = ciTem.id;
    newItem[itemKey].ikey = itemKey;
    newItem[itemKey].name = ciTem.data.name;

    let { ciKey } = ciTem.data.data;
    if (ciKey === '') {
      ciKey = ciTem.id;
    }

    newItem[itemKey].ciKey = ciTem.data.data.ciKey;

    if (!data.data.istemplate) {
      if (number === null) {
        number = 1;
      }

      newItem[itemKey].number = number;
      newItem[itemKey].isactive = false;
      newItem[itemKey].isreset = true;

      // const isunik = citemData.isUnique;

      for (let j = 0; j < citemData.groups.length; j += 1) {
        // let _groupcheck = await game.items.get(ciTem.data.data.groups[j].id);
        const _groupcheck = await auxMeth.getTElement(citemData.groups[j].id, 'group', citemData.groups[j].ikey);
        const groupID = citemData.groups[j].id;

        if (_groupcheck.data.data.isUnique) {
          for (let i = citems.length - 1; i >= 0; i -= 1) {
            // let citemObj = game.items.get(citems[i].id);
            const citemObj = await auxMeth.getcItem(citems[i].id, citems[i].ciKey);
            const hasgroup = citemObj.data.data.groups.some((y) => y.id === groupID);

            if (hasgroup) {
              //                            newdata = await this.deletecItem(citems[i].id, true,data);
              //                            citems = newdata.data.citems;
              citems[i].todelete = true;
            }
          }
        }
      }

      // newItem[itemKey].attributes = ciTem.data.data.attributes;
      // newItem[itemKey].attributes = {};
      // IMPLEMENTATION FOR DEFVALUES IN CITEMS****
      newItem[itemKey].attributes = await duplicate(citemData.attributes);

      // for (var key in citemData.attributes) {
      //     let myprop = await auxMeth.getTElement("NONE", "property", key);
      //     if (myprop !== null) {
      //         let defvalue = myprop.data.data.defvalue;
      //         let isauto = defvalue.match(/@|{|,|;|%|\[/g);
      //         if (isauto) {
      //             let newvalue = await auxMeth.autoParser(defvalue, attributes, citemData.attributes, false);
      //             newItem[itemKey].attributes[key].value = newvalue;
      //         }
      //     }
      // }

      const keys = Object.keys(citemData.attributes);

      for (let i = 0; i < keys.length; i += 1) {
        const key = keys[i];
        const doContinue = keys[i] === 'name' || keys[i] === 'description';

        if (!doContinue) {
          const myprop = await auxMeth.getTElement('NONE', 'property', key);
          if (myprop !== null) {
            const { defvalue } = myprop.data.data;
            const isauto = defvalue.match(/@|{|,|;|%|\[/g);

            if (isauto) {
              const newvalue = await auxMeth.autoParser(defvalue, attributes, citemData.attributes, false);
              newItem[itemKey].attributes[key].value = newvalue;
            }
          }
        }
      }

      newItem[itemKey].attributes.name = ciTem.data.name;
      newItem[itemKey].rolls = {};
      newItem[itemKey].lastroll = 0;

      newItem[itemKey].groups = citemData.groups;
      newItem[itemKey].usetype = citemData.usetype;
      newItem[itemKey].ispermanent = citemData.ispermanent;
      newItem[itemKey].rechargable = citemData.rechargable;

      let { maxuses } = citemData;
      if (Number.isNaN(maxuses)) {
        maxuses = await auxMeth.autoParser(maxuses, attributes, citemData.attributes, false);
      }

      newItem[itemKey].maxuses = maxuses;
      newItem[itemKey].uses = parseInt(maxuses, 10);
      newItem[itemKey].icon = citemData.icon;
      newItem[itemKey].selfdestruct = citemData.selfdestruct;
      newItem[itemKey].mods = [];

      for (let i = 0; i < citemData.mods.length; i += 1) {
        const _mod = citemData.mods[i];
        await newItem[itemKey].mods.push({
          index: _mod.index,
          citem: ciTem.id,
          once: false,
          exec: false,
          attribute: _mod.attribute,
          expr: _mod.value,
          value: null,
        });
      }

      newItem[itemKey].disabledmods = [];

      if (addedBy) {
        newItem[itemKey].addedBy = addedBy;
      }
    }

    await citems.push(newItem[itemKey]);

    data.flags.haschanged = true;

    return citems;
  }

  async deletecItem(itemID, _cascading = false, thisData = null) {
    if (itemID === null || itemID === '') {
      return null;
    }

    let newdata;
    if (thisData === null) {
      newdata = duplicate(this.data);
    } else {
      newdata = thisData;
    }

    const { attributes } = newdata.data;
    const { citems } = newdata.data;
    const toRemove = citems.find((y) => y.id === itemID || y.ciKey === itemID);
    // let remObj = game.items.get(itemID);

    if (toRemove === null) {
      return newdata;
    }

    const remObj = await auxMeth.getcItem(itemID, toRemove.ciKey);
    if (
      remObj !== null && citems.length > 0
      && (toRemove.isactive || toRemove.usetype === 'PAS')
    ) {
      const toRemoveObj = remObj.data.data;

      // Remove values added to attributes
      const addsetmods = toRemoveObj.mods.filter((y) => y.type === 'ADD');
      for (let i = 0; i < addsetmods.length; i += 1) {
        const modID = addsetmods[i].index;
        const _basecitem = await citems.find((y) => y.id === itemID && y.mods.find((x) => x.index === modID));

        if (_basecitem !== null) {
          const _mod = await _basecitem.mods.find((x) => x.index === modID);

          let myAtt = _mod.attribute;
          const myAttValue = _mod.value;
          let attProp = 'value';

          if (myAtt !== null) {
            if (myAtt.includes('.max')) {
              attProp = 'max';
              myAtt = myAtt.replace('.max', '');
            }

            const actorAtt = attributes[myAtt];
            const seedprop = game.items.find((y) => y.data.data.attKey === myAtt);

            if (actorAtt !== null) {
              if (addsetmods[i].type === 'ADD') {
                const jumpmod = await this.checkModConditional(this.data, addsetmods[i], _basecitem);
                if (
                  (
                    (toRemove.isactive && !toRemoveObj.ispermanent)
                    || (toRemoveObj.usetype === 'PAS' && !toRemoveObj.selfdestruct)
                  ) && !jumpmod
                ) {
                  if (seedprop.data.data.datatype === 'list') {
                    const options = seedprop.data.data.listoptions.split(',');
                    const optIndex = options.indexOf(actorAtt[attProp]);
                    let newvalue = optIndex - myAttValue;

                    if (newvalue < 0) {
                      newvalue = 0;
                    }

                    actorAtt[attProp] = options[newvalue];
                  } else {
                    actorAtt[attProp] -= myAttValue;
                  }
                }
              }
            }
          }
        }
      }

      const createmods = toRemoveObj.mods.filter((y) => y.type === 'CREATE');
      for (let h = 0; h < createmods.length; h += 1) {
        const ccmodID = createmods[h].index;
        const _ccitem = await citems.find((y) => y.id === itemID && y.mods.find((x) => x.index === ccmodID));

        if (_ccitem !== null) {
          const ccmod = await _ccitem.mods.find((x) => x.index === ccmodID);
          const ccAtt = ccmod.attribute;

          attributes[ccAtt].value = attributes[ccAtt].prev;
        }
      }

      const setmods = toRemoveObj.mods.filter((y) => y.type === 'SET');
      for (let u = 0; u < setmods.length; u += 1) {
        const ccmodID = setmods[u].index;
        const _ccitem = await citems.find((y) => y.id === itemID && y.mods.find((x) => x.index === ccmodID));

        if (_ccitem !== null) {
          const ccmod = await _ccitem.mods.find((x) => x.index === ccmodID);
          const ccAtt = ccmod.attribute;

          if (ccmod.exec) {
            if (attributes[ccAtt] !== null) {
              attributes[ccAtt].value = attributes[ccAtt].prev;
            }
          }
        }
      }

      const listmods = toRemoveObj.mods.filter((y) => y.type === 'LIST');
      for (let j = 0; j < listmods.length; j += 1) {
        const modID = listmods[j];
        const myAtt = modID.attribute;
        const myAttValue = modID.value;
        const myAttListEdit = modID.listmod;
        const splitter = myAttValue.split(',');

        for (let k = 0; k < splitter.length; k += 1) {
          const myOpt = splitter[k];

          if (myAttListEdit === 'INCLUDE') {
            const optIndex = attributes[myAtt].listedit.add.indexOf(myOpt);
            attributes[myAtt].listedit.add.splice(optIndex, 1);
          }

          if (myAttListEdit === 'REMOVE') {
            const optIndex = attributes[myAtt].listedit.remove.indexOf(myOpt);
            attributes[myAtt].listedit.remove.splice(optIndex, 1);
          }
        }
      }

      const itemsadded = citems.filter((y) => y.addedBy === itemID);
      for (let j = 0; j < itemsadded.length; j += 1) {
        if (!toRemoveObj.ispermanent) {
          newdata = await this.deletecItem(itemsadded[j].id, true, newdata);
        }
      }
    }

    citems.splice(citems.indexOf(toRemove), 1);
    this.data.flags.haschanged = true;

    //        if(this.isToken){
    //
    //            let tokenId = this.token.id;
    //            let mytoken = canvas.tokens.get(tokenId);
    //            //console.log(mytoken);
    //            await mytoken.update({"data.citems":citems},{diff:false});
    //        }

    return newdata;
  }

  async updateCItems() {
    const { citems } = this.data.data;
    for (let i = 0; i < citems.length; i += 1) {
      const citem = citems[i];
      // let citemTemplate = game.items.get(citems[i].id);
      const citemTemplate = await auxMeth.getcItem(citems[i].id, citems[i].ciKey);

      if (citemTemplate !== null && hasProperty(citemTemplate.data.data, 'groups')) {
        for (let j = 0; j < citemTemplate.data.data.groups.length; j += 1) {
          const groupID = citemTemplate.data.data.groups[j];
          // let group = game.items.get(groupID.id);
          const group = await auxMeth.getTElement(groupID.id, 'group', groupID.ikey);

          if (group !== null) {
            const { properties: groupProps } = group.data.data;
            const { attributes: itemAttrs } = citem;
            const { attributes: templateAttrs } = citemTemplate.data.data;

            for (let y = 0; y < groupProps.length; y += 1) {
              const property = groupProps[y];
              if (
                property.isconstant && itemAttrs[property.ikey]
                  && itemAttrs[property.ikey] !== null
                  && templateAttrs[property.ikey] !== null
                  && itemAttrs[property.ikey].value !== templateAttrs[property.ikey].value
              ) {
                itemAttrs[property.ikey].value = templateAttrs[property.ikey].value;
              }
            }
          }
        }
      } else {
        citems.splice(citems.indexOf(citem), 1);
      }
    }
  }

  async checkAttConsistency(attributes, mods) {
    const attArray = [];
    // const attributes = this.data.data.attributes;

    for (let k = 0; k < mods.length; k += 1) {
      const mod = mods[k];
      if (!attArray.includes(mods.attribute) && mod.attribute !== '') {
        const moat = mod.attribute.replace('.max', '');
        await attArray.push(moat);
      }
    }

    for (let i = 0; i < attArray.length; i += 1) {
      const attribute = attArray[i];
      // let attID;

      const propertypool = await game.items.filter((y) => y.data.type === 'property' && y.data.data.attKey === attribute);
      const property = propertypool[0];

      if (property !== null) {
        if (!hasProperty(attributes, attribute)) {
          await setProperty(attributes, attribute, {});
        }

        if (!hasProperty(attributes[attribute], 'id')) {
          await setProperty(attributes[attribute], 'id', property.data.id);
          // attID = property.data.id;
        }

        const defvalue = await auxMeth.autoParser(property.data.data.defvalue, attributes, null, false);

        if (!hasProperty(attributes[attribute], 'value')) {
          await setProperty(attributes[attribute], 'value', defvalue);
        }

        if (!hasProperty(attributes[attribute], 'max')) {
          await setProperty(attributes[attribute], 'max', '');
        }

        if (!hasProperty(attributes[attribute], 'prev')) {
          await setProperty(attributes[attribute], 'prev', defvalue);
        }
      }
    }
  }

  async getMods(citemIDs) {
    const mods = [];
    for (let n = 0; n < citemIDs.length; n += 1) {
      const ciID = citemIDs[n].id;
      // let cikeyID = citemIDs[n].ciKey;

      // let citemObjBase = await game.items.get(ciID);
      const citemObjBase = await auxMeth.getcItem(ciID, citemIDs[n].ciKey);

      if (citemObjBase !== null) {
        let citemObj = citemObjBase.data.data;
        if (!hasProperty(citemObj, 'ciKey')) {
          citemObj = citemObjBase.data._source.data;
        }

        for (let i = 0; i < citemObj.mods.length; i += 1) {
          const toaddMod = duplicate(citemObj.mods[i]);
          toaddMod.citem = ciID;
          toaddMod.ciKey = citemIDs[n].ciKey;

          const actorCiMod = citemIDs[n].mods.find((y) => y.index === toaddMod.index);
          toaddMod.once = actorCiMod.once;
          toaddMod.exec = actorCiMod.exec;

          await mods.push(toaddMod);
        }
      }
    }

    return mods;
  }

  async execITEMmods(mods, data) {
    const result = {};
    let citemIDs = data.data.citems;
    // const newcitem = false;
    // const updatecItem = true;
    const itemmods = mods.filter((y) => y.type === 'ITEM');

    let selector = false;
    result.iterate = false;

    if (itemmods.length === 0) {
      return result;
    }

    for (let i = 0; i < itemmods.length; i += 1) {
      const mod = itemmods[i];

      // let _citem = game.items.get(mod.citem).data.data;
      const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
      let _citem = _citemfinder.data.data;
      if (!hasProperty(_citem, 'ciKey')) {
        _citem = _citemfinder.data._source.data;
      }

      const citem = citemIDs.find((y) => y.id === mod.citem || y.ciKey === mod.ciKey);
      const cItemExists = citem !== null;

      if (cItemExists) {
        const jumpmod = await this.checkModConditional(data, mod, citem);

        const _originalcitemmod = await _citem.mods.find((y) => y.index === mod.index);
        const _basecitem = await citemIDs.find((y) => y.id === mod.citem && y.ciKey === mod.ciKey && y.mods.find((x) => x.index === mod.index));
        const _mod = await _basecitem.mods.find((x) => x.index === mod.index);

        // if (!jumpmod) {
        if (mod.selectnum === 0) {
          for (let k = 0; k < mod.items.length; k += 1) {
            const itemtoadd = mod.items[k];
            const ispresent = citemIDs.some((y) => y.id === itemtoadd.id || y.ciKey === itemtoadd.ciKey);

            let ispresentObj;
            if (ispresent) {
              ispresentObj = citemIDs.find((y) => y.id === itemtoadd.id || y.ciKey === itemtoadd.ciKey);
            }

            if ((_citem.usetype === 'PAS' || citem.isactive) && !jumpmod) {
              if (!ispresent && !_mod.exec && !_mod.once) {
                const toadd = await auxMeth.getcItem(itemtoadd.id, itemtoadd.ciKey);
                // let newItem = game.items.get(itemtoadd.id);
                // let newItem = await auxMeth.getcItem(itemtoadd.id);
                citemIDs = await this.addcItem(toadd, mod.citem, data);
                result.iterate = true;

                // newcitem = true;
                // if (mod.once ) {
                //     _mod.exec = true;
                // }
                if (_originalcitemmod.once) {
                  _mod.once = true;
                }
              }
            } else if (ispresent && !_citem.ispermanent) {
              if (ispresentObj.addedBy === mod.citem) {
                // newcitem = true;
                // const citemmod = citemIDs.find((y) => y.id === itemtoadd.id);
                // const cindex = citemIDs.indexOf(citemmod);
                const duplicanto = await this.deletecItem(itemtoadd.id, true, data);
                citemIDs = duplicanto.data.citems;
                // await citemIDs.splice(cindex, 1);
                await mods.splice(mods.findIndex((e) => e.citem === itemtoadd.id), 1);
              }

              _mod.exec = false;
            }
          }
        } else if (!jumpmod) {
        // let thiscitem = citemIDs.find(y => y.id === mod.citem);

          if (!hasProperty(citem, 'selection')) {
            setProperty(citem, 'selection', []);
          }

          const selindex = citem.selection.find((y) => y.index === mod.index);

          if (selindex === null) {
            const newindex = {};
            newindex.index = mod.index;
            newindex.selected = false;
            citem.selection.push(newindex);
            selector = true;
          } else if (!selindex.selected) {
            selector = true;
          }
        }
      }
    }

    result.citems = citemIDs;
    result.selector = selector;

    return result;
  }

  async setInputColor() {
    const citemIDs = this.data.data.citems;

    for (let j = 0; j < citemIDs.length; j += 1) {
      const { mods } = citemIDs[j];
      if (mods !== null) {
        for (let i = 0; i < mods.length; i += 1) {
          if (mods[i].exec) {
            const thismod = mods[i];

            let charsheet;
            if (this.token === null) {
              charsheet = document.getElementById(`actor-${this.id}`);
            } else {
              charsheet = document.getElementById(`actor-${this.id}-${this.token.data.id}`);
            }

            if (charsheet !== null) {
              const attinput = charsheet.getElementsByClassName(thismod.attribute);

              if (attinput[0] !== null) {
                if (parseInt(thismod.value, 10) < 0) {
                  attinput[0].className += ' input-red';
                } else {
                  attinput[0].className += ' input-green';
                }
              }
            }
          }
        }
      }
    }
  }

  async checkModConditional(data, mod, actorcitem) {
    // const mycitem = auxMeth.getcItem(mod.citem,mod.ciKey);
    // const citemIDs = data.data.citems;
    const { attributes } = data.data;
    const condAtt = mod.condat;
    let jumpmod = false;

    if (condAtt !== null && condAtt !== '' && mod.condat !== '') {
      const condValue = await auxMeth.autoParser(mod.condvalue, attributes, actorcitem.attributes, false);
      let attIntValue;

      if (condAtt.includes('#{') || condAtt.includes('@{')) {
        attIntValue = await auxMeth.autoParser(condAtt, attributes, actorcitem.attributes, false);
      } else if (attIntValue === false || attIntValue === true) {
        attIntValue = attributes[condAtt].value;
      } else if (attributes[condAtt] !== null) {
        if (!Number.isNaN(attIntValue)) {
          attIntValue = parseInt(attributes[condAtt].value, 10);
        } else {
          attIntValue = attributes[condAtt].value;
        }
      } else {
        return jumpmod;
      }

      if (mod.condop === 'EQU') {
        if (attIntValue.toString() !== mod.condvalue.toString()) {
          jumpmod = true;
        }
      } else if (mod.condop === 'HIH') {
        if (!Number.isNaN(attIntValue) && !Number.isNaN(condValue)) {
          if (Number(attIntValue) <= Number(condValue)) {
            jumpmod = true;
          }
        }
      } else if (mod.condop === 'LOW') {
        if (!Number.isNaN(attIntValue) && !Number.isNaN(condValue)) {
          if (Number(attIntValue) >= Number(condValue)) {
            jumpmod = true;
          }
        }
      }
    }

    return jumpmod;
  }

  async setdefcItems(actorData) {
    // ADDS defauls CITEMS
    const citemIDs = actorData.data.citems;

    let citems;

    const mytemplate = actorData.data.gtemplate;
    if (mytemplate !== 'Default') {
      const _template = await game.actors.find((y) => y.data.data.istemplate && y.data.data.gtemplate === mytemplate);

      if (_template !== null) {
        for (let k = 0; k < _template.data.data.citems.length; k += 1) {
          const mycitemId = _template.data.data.citems[k].id;
          const mycitemiKey = _template.data.data.citems[k].ciKey;
          // let mycitem = game.items.get(mycitemId);
          const mycitem = await auxMeth.getcItem(mycitemId, mycitemiKey);
          const citeminActor = await citemIDs.find((y) => y.id === mycitemId || y.ciKey === mycitemiKey);

          if (!citeminActor && mycitem !== null) {
            citems = await this.addcItem(mycitem, null, actorData);
          }
        }
      }
    }

    if (citems === null) {
      citems = citemIDs;
    }

    return citems;
  }

  async removeDropcITems(actorData) {
    // Removes cITems marked for removal CITEMS
    const citemIDs = actorData.data.citems;
    // let toreturn;
    for (let k = 0; k < citemIDs.length; k += 1) {
      const mycitem = citemIDs[k];

      if (mycitem.todelete) {
        actorData = await this.deletecItem(mycitem.id, true, actorData);
      }
    }

    return actorData;
  }

  async checkcItemConsistency(actorData) {
    // Removes cITems marked for removal CITEMS
    const citemIDs = actorData.data.citems;
    const { attributes } = actorData.data;

    for (let k = 0; k < citemIDs.length; k += 1) {
      const mycitem = citemIDs[k];

      // let cIOrigTemplate = game.items.get(mycitem.id);
      const cIOrigTemplate = await auxMeth.getcItem(mycitem.id, mycitem.ciKey);
      // If no template remove cItem
      const hasTemplate = cIOrigTemplate !== null;
      if (!hasTemplate) {
        citemIDs.splice(citemIDs.indexOf(mycitem), 1);
      } else {
        if (mycitem.ciKey === null) {
          mycitem.ciKey = cIOrigTemplate.data.data.ciKey;
        }

        const cITemplate = duplicate(cIOrigTemplate);
        let requestUpdate = false;
        let updatecItem = false;

        // TODO CHECK FOR CHANGED ATTRIBUTES IN ORIGINAL CITEM
        for (let j = 0; j < cITemplate.data.groups.length; j += 1) {
          const myGroup = cITemplate.data.groups[j];
          // let myGroupTemp = game.items.get(myGroup.id);
          const myGroupTemp = await auxMeth.getTElement(myGroup.id, 'group', myGroup.ikey);
          // If no group remove cItem
          if (myGroupTemp === null) {
            citemIDs.splice(citemIDs.indexOf(k), 1);
            break;
          }

          const groupProps = myGroupTemp.data.data.properties;

          for (let l = 0; l < groupProps.length; l += 1) {
            const myPropId = groupProps[l].id;
            // let myProp = game.items.get(myPropId);
            const myProp = await auxMeth.getTElement(myPropId, 'property', groupProps[l].ikey);
            const att = myProp.data.data.attKey;
            const tempAtt = cITemplate.data.attributes[att];
            let newvalue;

            if (tempAtt !== null) {
              if (hasProperty(mycitem.attributes, att)) {
                if (groupProps[l].isconstant && tempAtt.value !== mycitem.attributes[att].value) {
                  if (actorData.permission.default >= CONST.ENTITY_PERMISSIONS.OBSERVER || actorData.permission[game.user.id] >= CONST.ENTITY_PERMISSIONS.OBSERVER || game.user.isGM) { mycitem.attributes[att].value = tempAtt.value; }
                }
              }

              if (game.user.isGM) {
                newvalue = tempAtt.value;
              }
            } else {
              updatecItem = true;
              if (myProp.data.data.datatype === 'simplenumeric') {
                newvalue = await auxMeth.autoParser(myProp.data.data.defvalue, attributes, cITemplate.data.attributes, false);
              } else {
                newvalue = await auxMeth.autoParser(myProp.data.data.defvalue, attributes, cITemplate.data.attributes, true);
              }

              // GUARDA Y LUEGO ACCEDE
              if (game.user.isGM) {
                setProperty(cITemplate.data.attributes, att, {});
                cITemplate.data.attributes[att].value = newvalue;
              } else {
                // REQUEST UPDATE
                requestUpdate = true;
              }
            }

            if (!hasProperty(mycitem.attributes, att)) {
              const { permission } = this.data;
              if (
                permission.default >= CONST.ENTITY_PERMISSIONS.OBSERVER
                || permission[game.user.id] >= CONST.ENTITY_PERMISSIONS.OBSERVER
                || game.user.isGM
              ) {
                setProperty(mycitem.attributes, att, {});
                setProperty(mycitem.attributes[att], 'value', newvalue);
              }
            }
          }
        }

        if (game.user.isGM) {
          if (updatecItem) {
            await cIOrigTemplate.update({ 'data.attributes': cITemplate.data.attributes });
          }

          for (let i = 0; i < cITemplate.data.mods.length; i += 1) {
            const originalmod = cITemplate.data.mods[i];
            const mymod = mycitem.mods.find((y) => y.index === originalmod.index);
            if (mymod === null) {
              mycitem.mods.push({
                index: originalmod.index,
                citem: mycitem.id,
                once: originalmod.once,
                exec: false,
                attribute: originalmod.attribute,
                expr: originalmod.value,
                value: null,
              });
            }
            // else {
            //     if (mymod.items !== null)
            //         if (mymod.items.length > 0) {
            //             for (let h = 0; h < mymod.items.length; h+= 1) {
            //                 if (mymod.items[h].ciKey === null) {
            //                     let toaddotem = await auxMeth.getcItem(mymod.items[h].id);
            //                     if (toaddotem)
            //                         mymod.items[h].ciKey = toaddotem.data.data.ciKey;
            //                 }

          //             }
          //         }
          // }
          }
        } else if (requestUpdate === true) {
          ui.notifications.warn('Please ask your GM to reload the template on your character, there are broken cItems');
        }
      }
    }

    return citemIDs;
  }

  async checkActorMods(citemIDs, mods) {
    for (let i = 0; i < citemIDs.length; i += 1) {
      const myCImods = citemIDs[i].mods;
      for (let j = 0; j < myCImods.length; j += 1) {
        const modexists = mods.find((y) => y.index === myCImods[j].index && y.citem === myCImods[j].citem);

        if (modexists === null) {
          delete myCImods[j];
        }
      }
    }
  }

  async checkPropAuto(actorData, repeat = false) {
    //        await this.update({"flags.ischeckingauto":true});
    //        this.data.flags.ischeckingauto = true;
    //        this.data.flags.hasupdated = false;
    // let newcitem = false;
    // const newroll = false;
    // let ithaschanged = false;

    const { attributes } = actorData.data;
    const attributearray = [];

    Object.keys(attributes).forEach(async (attrKey) => {
      const attdata = attributes[attrKey];

      if (Array.isArray(attdata.value)) {
        attdata.value = attdata.value[0];
      }

      await setProperty(attdata, 'isset', false);
      await setProperty(attdata, 'maxset', false);
      await setProperty(attdata, 'default', false);

      // TEST TO DELETE
      await setProperty(attdata, 'autoadd', 0);
      await setProperty(attdata, 'maxadd', 0);
      await setProperty(attdata, 'maxexec', false);

      attributearray.push(attrKey);
    });

    // CHECKING CI ITEMS
    actorData = await this.removeDropcITems(actorData);
    actorData.data.citems = await this.setdefcItems(actorData);
    actorData.data.citems = await this.checkcItemConsistency(actorData);

    let mods = [];
    let resMods = true;

    while (resMods) {
      mods = await this.getMods(actorData.data.citems);
      const newmodcitems = await this.execITEMmods(mods, actorData);
      resMods = newmodcitems.iterate;

      actorData.data.selector = newmodcitems.selector;
      if (resMods) {
        // newcitem = true;
        // ithaschanged = true;

        actorData.data.citems = newmodcitems.citems;
      }
    }

    // actorData.data.citems = this.checkActorMods(actorData.data.citems,mods);

    const citemIDs = actorData.data.citems;
    const originalcIDs = duplicate(citemIDs);

    await this.updateCItems();
    if (mods.length > 0) {
      await this.checkAttConsistency(attributes, mods);
    }

    const { rolls } = actorData.data;

    // CREATE MODS
    const createmods = mods.filter((y) => y.type === 'CREATE');
    for (let i = 0; i < createmods.length; i += 1) {
      const mod = createmods[i];
      const modAtt = mod.attribute;
      const modDefValue = mod.value;

      if (!hasProperty(attributes, modAtt)) {
        setProperty(attributes, modAtt, {});
        setProperty(attributes[modAtt], 'id', `${mod.citem}_${mod.index}`);
        setProperty(attributes[modAtt], 'value', modDefValue);
        setProperty(attributes[modAtt], 'prev', modDefValue);
        setProperty(attributes[modAtt], 'autoadd', 0);
        setProperty(attributes[modAtt], 'isset', false);
        setProperty(attributes[modAtt], 'created', true);
        setProperty(attributes[modAtt], 'hastotals', false);
      }
    }

    // CHECK DEFVALUES IF IS NOT AUTO!!
    for (let i = 0; i < attributearray.length; i += 1) {
      const attribute = attributearray[i];
      const attdata = attributes[attribute];

      // let property = await game.items.get(actorData.data.attributes[attribute].id);
      const property = await auxMeth.getTElement(actorData.data.attributes[attribute].id, 'property', attribute);
      const actorAtt = actorData.data.attributes[attribute];

      if (property !== null) {
        if (actorAtt.value === '' && property.data.data.auto === '' && !property.data.data.defvalue.includes('.max}')) {
          if (property.data.data.defvalue !== '' || (property.data.data.datatype === 'checkbox')) {
            let exprmode = false;
            if (property.data.data.datatype === 'simpletext' || property.data.data.datatype === 'textarea') {
              exprmode = true;
            }

            let newValue = await auxMeth.autoParser(property.data.data.defvalue, attributes, null, exprmode);
            if (property.data.data.datatype === 'checkbox') {
              if (newValue === null) {
                newValue = false;
              } else if (newValue === '' || newValue === 0 || newValue === 'false') {
                newValue = false;
              } else {
                newValue = true;
              }
            }

            // if (actorAtt.value !== newValue) {
            //   ithaschanged = true;
            // }

            actorAtt.value = newValue;
          }
        }

        // TODO DEFVALUE PARA MAX
        if (attdata.modmax) {
          attdata.maxblocked = true;
        }

        if (actorAtt.max === null || actorAtt.max === '') {
          attdata.maxblocked = false;
        }

        if (property.data.data.automax !== null) {
          if (property.data.data.automax !== '') {
            if (!hasProperty(attdata, 'maxblocked')) {
              attdata.maxblocked = false;
            }

            if (!attdata.maxblocked) {
              actorAtt.max = await auxMeth.autoParser(property.data.data.automax, attributes, null, false);
            }
          }
        }

        if (actorAtt.max === null || actorAtt.max === '') {
          attdata.maxblocked = false;
        }
      }
    }

    // CI SET MODS
    const setmods = mods.filter((y) => y.type === 'SET');
    for (let i = 0; i < setmods.length; i += 1) {
      const mod = setmods[i];
      let modAtt = mod.attribute;
      let attProp = 'value';
      // let modvable = 'modified';
      let setvble = 'isset';

      if (modAtt.includes('.max')) {
        modAtt = modAtt.replace('.max', '');
        attProp = 'max';
        // modvable = 'modmax';
        setvble = 'maxset';
      }

      const citem = citemIDs.find((y) => y.id === mod.citem);

      let jumpmod = false;
      if (mod.condop !== 'NON' && mod.condop !== null) {
        jumpmod = await this.checkModConditional(actorData, mod, citem);
      }

      if (hasProperty(attributes, modAtt)) {
        const { value } = mod;

        let finalvalue = value;

        // let _citem = await game.items.get(mod.citem).data.data; CUIDAO AQUI mod.cIKEy as puesto
        const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
        const _citem = _citemfinder.data.data;

        finalvalue = await auxMeth.autoParser(value, attributes, citem.attributes, true, false, citem.number);

        const myAtt = attributes[modAtt];

        const _basecitem = await citemIDs.find((y) => y.id === mod.citem && y.mods.find((x) => x.index === mod.index));
        const _mod = await _basecitem.mods.find((x) => x.index === mod.index);

        // Checks if mod has not changed. TODO METHOD TO CHECK THIS AND MOD EXISTING IN BETTER WAY
        // if(_mod.exec && (_mod.value!=finalvalue || _mod.attribute!=modAtt)){
        if (_mod.exec && (_mod.attribute !== modAtt)) {
          _mod.exec = false;
        }

        if (_mod.expr !== null) {
          if (finalvalue !== _mod.expr) {
            _mod.exec = false;
          }
        }

        _mod.expr = finalvalue;

        const textexpr = value.match(/[|]/g);
        if (textexpr === null && (value.charAt(0) !== '|')) {
          finalvalue = await auxMeth.autoParser(finalvalue, attributes, citem.attributes, false, false, citem.number);
        }

        if ((_citem.usetype === 'PAS' || citem.isactive) && !jumpmod) {
          // if (!_mod.exec)
          //     myAtt[setvble] = true;
          if (attProp !== 'max' || (attProp === 'max' && !myAtt.maxblocked)) {
            if (!_mod.exec) {
              myAtt.prev = myAtt[attProp];
            }

            _mod.exec = true;
            _mod.value = finalvalue;
            _mod.attribute = mod.attribute;
            // ithaschanged = true;
            myAtt[attProp] = finalvalue;
            myAtt[setvble] = true;
          }
        } else if (_mod.exec) {
          _mod.exec = false;
          myAtt[attProp] = myAtt.prev;
        }
      }
    }

    // CI ADD TO NON AUTO ATTR
    const addmods = mods.filter((y) => y.type === 'ADD');
    for (let i = 0; i < addmods.length; i += 1) {
      const mod = addmods[i];
      let modAtt = mod.attribute;
      let attProp = 'value';
      let modvable = 'modified';

      // let setvble = 'isset';
      if (modAtt.includes('.max')) {
        modAtt = modAtt.replace('.max', '');
        attProp = 'max';
        modvable = 'modmax';
        // setvble = 'maxset';
      }

      const citem = await citemIDs.find((y) => y.id === mod.citem);

      let jumpmod = false;
      if (mod.condop !== 'NON' && mod.condop !== null) {
        jumpmod = await this.checkModConditional(actorData, mod, citem);
      }

      // let _citem = await game.items.get(mod.citem).data.data;
      const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
      const _citem = _citemfinder.data.data;

      if (hasProperty(attributes, modAtt)) {
        const myAtt = attributes[modAtt];

        // let seedprop = game.items.get(myAtt.id);
        const seedprop = await auxMeth.getTElement(myAtt.id, 'property', modAtt);
        let checker = false;
        if (seedprop !== null) {
          const { automax, auto, datatype } = seedprop.data.data;
          if (((automax === '' && attProp === 'max') || (auto === '' && attProp === 'value')) && (datatype === 'simplenumeric' || datatype === 'radio' || datatype === 'badge' || datatype === 'list')) {
            checker = true;
          }
        }

        if (myAtt.created || checker) {
          let { value } = mod;
          if (value === null) {
            value = 0;
          }

          let finalvalue = value;
          if (value !== null) {
            if (Number.isNaN(value)) {
              if (value.charAt(0) === '|') {
                value = value.replace('|', '');
                finalvalue = await auxMeth.autoParser(value, attributes, citem.attributes, true, false, citem.number);
              } else {
                finalvalue = await auxMeth.autoParser(value, attributes, citem.attributes, false, false, citem.number);
              }
            }
          }

          finalvalue = Number(finalvalue);

          const _basecitem = await citemIDs.find((y) => y.id === mod.citem && y.mods.find((x) => x.index === mod.index));
          const _mod = await _basecitem.mods.find((x) => x.index === mod.index);
          const _origmod = await _citem.mods.find((x) => x.index === mod.index);

          if (citem.selfdestruct) {
            if (citem.usetype === 'PAS') {
              citem.ispermanent = true;
            }
          }

          let exprcheck = _mod.expr;
          if (exprcheck === undefined) {
            exprcheck = '';
          }

          let checkroll;
          if (exprcheck !== '' || exprcheck !== null) {
            checkroll = exprcheck.match(/(d[%@(])|(d[0-9]+)/g);
          }

          if (_mod.exec && ((_mod.value !== finalvalue && checkroll === null) || _mod.attribute !== modAtt)) {
            if (!citem.ispermanent) {
              if (!_mod.attribute.includes('.max')) {
                attributes[_mod.attribute].value = Number(attributes[_mod.attribute].value) - _mod.value;
              } else {
                attributes[_mod.attribute].max = Number(attributes[_mod.attribute].max) - _mod.value;
              }
            }

            _mod.exec = false;
          }

          if ((_citem.usetype === 'PAS' || citem.isactive) && !jumpmod) {
            if (!_mod.exec || (myAtt[modvable] && !_mod.once)) {
              myAtt.prev = myAtt[attProp];

              if (seedprop.data.data.datatype === 'list') {
                const options = seedprop.data.data.listoptions.split(',');
                const optIndex = options.indexOf(myAtt[attProp]);
                let newvalue = optIndex + finalvalue;

                if (newvalue + 1 > options.length) {
                  newvalue = options.length - 1;
                }

                myAtt[attProp] = options[newvalue];
              } else {
                myAtt[attProp] = parseInt(Number(myAtt[attProp]) + finalvalue, 10);
              }

              // Prueba
              if (_origmod.once) {
                _mod.once = true;
              }

              // ithaschanged = true;

              _mod.exec = true;
              _mod.value = finalvalue;
              _mod.attribute = modAtt;

              // TODO Combine
              if (seedprop !== null) {
                if (attProp === 'value' && myAtt.max !== '' && seedprop.data.data.automax !== '') {
                  if (myAtt[attProp] > myAtt.max && seedprop.data.data.maxtop) {
                    myAtt[attProp] = myAtt.max;
                    // ithaschanged = true;
                  }
                }
              }
            }
          } else if (
            (!citem.isreset || _citem.usetype === 'PAS')
            && _mod.exec
            && ((citem.isactive && jumpmod) || !citem.isactive)
            && !myAtt.default
            && !citem.ispermanent
          ) {
            _mod.exec = false;

            if (seedprop.data.data.datatype === 'list') {
              const options = seedprop.data.data.listoptions.split(',');
              const optIndex = options.indexOf(myAtt[attProp]);
              let newvalue = optIndex - finalvalue;

              if (newvalue < 0) {
                newvalue = 0;
              }

              myAtt[attProp] = options[newvalue];
            } else {
              myAtt[attProp] = Number(myAtt[attProp]) - Number(finalvalue);
            }

            // ithaschanged = true;
          }
        }
      }
    }

    // AUTO PROPERTIES PRE CALCULATIONS
    // ithaschanged = await this.autoCalculateAttributes(actorData,attributearray,attributes,true);

    // CI ADD TO AUTO ATTR
    for (let i = 0; i < addmods.length; i += 1) {
      const mod = addmods[i];
      let modAtt = mod.attribute;
      let attProp = 'value';
      // let modvable = 'modified';
      let setvble = 'isset';

      if (modAtt.includes('.max')) {
        modAtt = modAtt.replace('.max', '');
        attProp = 'max';
        // modvable = 'modmax';
        setvble = 'maxset';
      }

      const citem = citemIDs.find((y) => y.id === mod.citem);

      let jumpmod = false;
      if (mod.condop !== 'NON' && mod.condop !== null) {
        jumpmod = await this.checkModConditional(actorData, mod, citem);
      }

      // let _citem = game.items.get(mod.citem).data.data;
      const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
      const _citem = _citemfinder.data.data;

      if (hasProperty(attributes, modAtt)) {
        const myAtt = attributes[modAtt];

        // let seedprop = game.items.get(myAtt.id);
        const seedprop = await auxMeth.getTElement(myAtt.id, 'property', modAtt);
        let checker = false;
        if (seedprop !== null) {
          const { data } = seedprop.data;
          const { automax, auto, datatype } = data;

          if (
            ((automax !== '' && attProp === 'max') || (auto !== '' && attProp === 'value'))
            && (datatype === 'simplenumeric' || datatype === 'radio' || datatype === 'badge')
          ) {
            checker = true;
          }
        }

        if (checker) {
          let { value } = mod;
          let finalvalue = value;

          if (Number.isNaN(value)) {
            if (value.charAt(0) === '|') {
              value = value.replace('|', '');
              finalvalue = await auxMeth.autoParser(value, attributes, citem.attributes, true, false, parseInt(citem.number, 10));
            } else {
              finalvalue = await auxMeth.autoParser(value, attributes, citem.attributes, false, false, parseInt(citem.number, 10));
            }
          }

          const _basecitem = await citemIDs.find((y) => y.id === mod.citem && y.mods.find((x) => x.index === mod.index));
          if (_basecitem !== null) {
            const _mod = await _basecitem.mods.find((x) => x.index === mod.index);

            // if(_mod.exec && (_mod.value!=finalvalue || _mod.attribute!=modAtt)){

            const exprcheck = _mod.expr.toString();
            const checkroll = exprcheck.match(/(d[%@(])|(d[0-9]+)/g);

            if (_mod.exec && checkroll !== null) {
              finalvalue = _mod.value;
            }

            if (_mod.exec && ((_mod.value !== finalvalue && checkroll === null) || _mod.attribute !== modAtt)) {
              const special = attributes[modAtt];
              if (!citem.ispermanent) {
                if (!_mod.attribute.includes('.max')) {
                  special.value = Number(special.value) - _mod.value;
                }
                //                                else{
                //                                    //AQUI ESTA EL ERRIR!!!
                //                                    special.max = Number(special.max) - _mod.value;
                //                                }
              }

              _mod.exec = false;
            }

            if (myAtt[setvble]) {
              _mod.exec = false;
            }

            if ((_citem.usetype === 'PAS' || citem.isactive) && !jumpmod && !_mod.once) {
              // if(!_mod.exec || (myAtt[modvable] && !mod.once)){
              // if((seedprop.data.data.automax!="" && attProp=="max") || (seedprop.data.data.auto!="" && attProp=="value")){
              // ithaschanged = true;

              _mod.value = finalvalue;
              _mod.attribute = mod.attribute;

              // TEST TO REINSTATE
              // myAtt.isset = true;
              // myAtt[attProp] = await Number(myAtt[attProp]) + Number(finalvalue);

              // TEST TO DELETE

              if (attProp === 'value') {
                myAtt.autoadd += Number(finalvalue);
              }
              if (attProp === 'max') {
                myAtt.maxadd += Number(finalvalue);
              }

              if (seedprop !== null) {
                const { automax } = seedprop.data.data;
                if (attProp === 'value' && myAtt.max !== '' && automax !== '') {
                  const { maxtop } = seedprop.data.data;
                  if (myAtt[attProp] > myAtt.max && maxtop) {
                    myAtt[attProp] = myAtt.max;
                    // ithaschanged = true;
                  }
                }
              }

              _mod.exec = true;
              // Prueba
              if (mod.once) {
                _mod.once = true;
              }

              // }
            } else if ((!citem.isreset || _citem.usetype === 'PAS' || jumpmod) && !_citem.isactive) {
              // REMOVE PAS IF IT DOES NOT WORK
              // if (!myAtt.default && _mod.exec && !citem.ispermanent) {
              //   // myAtt[attProp] = Number(myAtt[attProp]) - Number(finalvalue);

              //   ithaschanged = true;

              // }

              _mod.exec = false;
              // myAtt[setvble] = false;
            }
          } else {
            // Error on citem,just remove it
            citemIDs.splice(citemIDs.indexOf(citem), 1);
          }
        }
      }
    }

    // ithaschanged = await this.autoCalculateAttributes(actorData, attributearray, attributes, true, false);

    // return;

    // ADD ROLLS
    const rollmods = mods.filter((y) => y.type === 'ROLL');

    Object.values(rolls).forEach((roll) => {
      // ithaschanged = true;

      roll.modified = false;
      setProperty(roll, 'value', '');
    });

    for (let i = 0; i < rollmods.length; i += 1) {
      const mod = rollmods[i];

      const rollID = mod.attribute;
      const rollvaluemod = mod.value;

      const citem = citemIDs.find((y) => y.id === mod.citem);
      // let _citem = game.items.get(mod.citem).data.data;
      const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
      const _citem = _citemfinder.data.data;

      let jumpmod = false;
      if (mod.condop !== 'NON' && mod.condop !== null) {
        jumpmod = await this.checkModConditional(actorData, mod, citem);
      }

      if (!jumpmod) {
        if (!hasProperty(rolls, rollID)) {
          setProperty(rolls, rollID, {});
          setProperty(rolls[rollID], 'value', '');
          // ithaschanged = true;
        }

        const toadd = await auxMeth.autoParser(rollvaluemod, attributes, citem.attributes, false, false, citem.number);
        const rExp = `+(${toadd})`;
        const _basecitem = await citemIDs.find((y) => y.id === citem.id && y.mods.find((x) => x.index === mod.index));

        const _mod = await _basecitem.mods.find((x) => x.index === mod.index);
        rolls[rollID].modified = true;

        if ((_citem.usetype === 'PAS' || citem.isactive)) {
          // if(!_mod.exec){
          _mod.exec = true;
          // ithaschanged = true;
          // rolls[rollID].value += parseInt(toadd);
          rolls[rollID].value += rExp;
          // }
        }
      }
    }

    // const counter = 0;

    // LIST EDITION "TESTING"
    const listmods = mods.filter((y) => y.type === 'LIST');

    for (let i = 0; i < listmods.length; i += 1) {
      const mod = listmods[i];
      const attKey = mod.attribute;
      const attValue = mod.value;
      const editType = mod.listmod;

      const citem = citemIDs.find((y) => y.id === mod.citem);
      // let _citem = game.items.get(mod.citem).data.data;
      const _citemfinder = await auxMeth.getcItem(mod.citem, mod.ciKey);
      // const _citem = _citemfinder.data.data;

      let jumpmod = false;
      if (mod.condop !== 'NON' && mod.condop !== null) {
        jumpmod = await this.checkModConditional(actorData, mod, citem);
      }

      if (!jumpmod) {
        const myAtt = attributes[attKey];
        // let seedprop = game.items.get(myAtt.id);
        const seedprop = await auxMeth.getTElement(myAtt.id, 'property', attKey);

        if (seedprop !== null) {
          if (seedprop.data.data.datatype === 'list') {
            if (attributes[attKey].listedit === null) { attributes[attKey].listedit = {}; }
            if (editType === 'INCLUDE') {
              if (attributes[attKey].listedit.add === null) {
                attributes[attKey].listedit.add = [];
              }

              const splitter = attValue.split(',');
              for (let j = 0; j < splitter.length; j += 1) {
                const myoption = splitter[j];
                const { add } = attributes[attKey].listedit;
                if (!add.includes(myoption)) {
                  add.push(myoption);
                }
              }
            }

            if (editType === 'REMOVE') {
              if (attributes[attKey].listedit.remove === null) {
                attributes[attKey].listedit.remove = [];
              }

              const splitter = attValue.split(',');
              for (let j = 0; j < splitter.length; j += 1) {
                const myoption = splitter[j];
                const { remove } = attributes[attKey].listedit;
                if (!remove.includes(myoption)) {
                  remove.push(myoption);
                }
              }
            }
          }
        }
      }
    }

    // PARSE VALUES TO INT
    for (let i = 0; i < attributearray.length; i += 1) {
      const attribute = attributearray[i];
      const attdata = attributes[attribute];

      // let property = await game.items.get(actorData.data.attributes[attribute].id);
      const property = await auxMeth.getTElement(actorData.data.attributes[attribute].id, 'property', attribute);
      const actorAtt = actorData.data.attributes[attribute];

      if (property !== null) {
        let mydefvalue = 0;

        const { datatype } = property.data.data;
        if (datatype === 'simplenumeric' || datatype === 'radio' || datatype === 'badge') {
          const { defvalue, auto } = property.data.data;
          if (defvalue !== '') {
            mydefvalue = defvalue;
          }

          if (auto === '' && actorAtt.value === '') {
            // ithaschanged = true;
            actorAtt.value = await auxMeth.autoParser(mydefvalue, attributes, null, false);
          }

          actorAtt.value = parseInt(actorAtt.value, 10);
          actorAtt.max = parseInt(actorAtt.max, 10);
        }

        if (datatype === 'checkbox') {
          if (!(actorAtt.value === true || actorAtt.value === false)) {
            if (actorAtt.value === 'false') {
              actorAtt.value = false;
            }

            if (actorAtt.value === 'true') {
              actorAtt.value = true;
            }
          }
        }
      } else if (!attdata.created) {
        delete actorData.data.attributes[attribute];

        // ithaschanged = true;
      } else {
        actorAtt.value = parseInt(actorAtt.value, 10);
      }

      if (attributearray[i] !== 'biography') {
        attdata.modified = false;
        attdata.modmax = false;
      }
    }

    // CONSUMABLES ACTIVE TURN BACK INTO INACTIVE, AND DELETE SELFDESTRUCTIBLE
    if (citemIDs !== null && !actorData.data.istemplate) {
      for (let n = citemIDs.length - 1; n >= 0; n -= 1) {
        const cItemTest = await auxMeth.getcItem(citemIDs[n].id, citemIDs[n].ciKey);
        if (cItemTest !== null) {
          // MANAGE CITEM USES & MAXUSES
          const citemObj = cItemTest.data.data;
          const citmAttr = citemIDs[n].attributes;
          const citmNum = citemIDs[n].number;

          const myMaxuses = await auxMeth.autoParser(citemObj.maxuses, attributes, citmAttr, false);
          const finalmaxuses = parseInt(citmNum * myMaxuses, 10);

          citemIDs[n].maxuses = finalmaxuses;
          if (citemIDs[n].uses > finalmaxuses) {
            citemIDs[n].uses = finalmaxuses;
          }

          // Calculate autos of citems  *** TEST **********************************
          const citemGroups = citemObj.groups;

          for (let z = 0; z < citemGroups.length; z += 1) {
            const citemGr = citemGroups[z];

            // let cigroup = game.items.get(citemGr.id);
            const cigroup = await auxMeth.getTElement(citemGr.id, 'group', citemGr.ikey);

            let groupprops = [];

            if (cigroup !== null) {
              groupprops = cigroup.data.data.properties;
            }

            for (let x = 0; x < groupprops.length; x += 1) {
              // let propdata = game.items.get(groupprops[x].id);
              const propdata = await auxMeth.getTElement(groupprops[x].id, 'property', groupprops[x].ikey);
              const { attKey: propKey, auto: propAuto, datatype } = propdata.data.data;

              if (propAuto !== '') {
                let rawvalue = await auxMeth.autoParser(propAuto, attributes, citmAttr, false, false, citmNum);

                if (Number.isNaN(rawvalue) && datatype !== 'simpletext') {
                  const afinal = new Roll(rawvalue);
                  await afinal.evaluate({ async: true });

                  if (!Number.isNaN(afinal.total)) {
                    rawvalue = afinal.total;
                  }
                }

                citmAttr[propKey].value = rawvalue;
              }
            }
          }

          //* ************************************************************************

          if (citemIDs[n].isactive) {
            if (citemObj.usetype === 'CON') {
              citemIDs[n].isactive = false;

              for (let j = 0; j < citemIDs[n].mods.length; j += 1) {
                citemIDs[n].mods[j].exec = false;
              }

              if (!citemIDs[n].rechargable && citemIDs[n].number <= 0) {
                actorData = await this.deletecItem(citemIDs[n].id, true, actorData);
              }
            } else {
              citemIDs[n].ispermanent = false;
            }
          } else {
            citemIDs[n].isreset = true;
          }

          if (citemObj.usetype === 'PAS') {
            if (citemIDs[n].number <= 0) {
              actorData = await this.deletecItem(citemIDs[n].id, true, actorData);
            }
          }

          if (citemIDs[n] !== null) {
            // Self destructible items
            if (citemIDs[n].selfdestruct === true) {
              actorData = await this.deletecItem(citemIDs[n].id, true, actorData);
            }
          }
        }
      }
    }

    // FREE TABLES autos of items  *** TEST **********************************

    Object.keys(attributes).forEach(async (tabAProp) => {
      if (attributes[tabAProp].istable) {
        const tProp = attributes[tabAProp];

        // let tableObj = game.items.get(t_Prop.id);
        const tableObj = await auxMeth.getTElement(tProp.id, 'property', tabAProp);

        if (tableObj === null) {
          return;
        }

        const totalGroupID = tableObj.data.data.group.id;
        // FREE TABLE AUTO PROP CALCULATION
        if (tProp.tableitems !== null) {
          // let groupObj = game.items.get(totalGroupID);
          const groupObj = await auxMeth.getTElement(totalGroupID, 'group', tableObj.data.data.group.ikey);
          if (groupObj === null) {
            return;
          }

          const groupProps = groupObj.data.data.properties;
          for (let k = 0; k < groupProps.length; k += 1) {
            // let tableProp = game.items.get(groupProps[k].id);
            const tableProp = await auxMeth.getTElement(groupProps[k].id, 'property', groupProps[k].ikey);
            const propauto = tableProp.data.data.auto;
            const freepropKey = tableProp.data.data.attKey;

            let freevalue;
            if (propauto !== '') {
              for (let d = 0; d < tProp.tableitems.length; d += 1) {
                freevalue = await auxMeth.autoParser(propauto, attributes, tProp.tableitems[d].attributes, false, false);
                tProp.tableitems[d].attributes[freepropKey].value = freevalue;
              }
            }
          }
        }

        // TOTAL CALCULATION

        let gcitems;

        if (!tableObj.data.data.isfreetable) {
          gcitems = citemIDs.filter((y) => y.groups.filter((x) => x.id === totalGroupID));
        } else {
          gcitems = tProp.tableitems;
        }

        const { totals } = tProp;
        Object.keys(totals).forEach((propKey) => {
          let newTotal = 0;

          for (let q = 0; q < gcitems.length; q += 1) {
            const cItemTotal = gcitems[q].attributes[propKey];
            if (cItemTotal !== null) {
              newTotal += Number(cItemTotal.value);
            }
          }

          tProp.totals[propKey].total = newTotal;
        });
      }
    });

    // CHECK FINAL AUTO VALUES -= 1 IS THERE A BETTER WAY???
    // ithaschanged = await this.autoCalculateAttributes(actorData, attributearray, attributes, true, true);

    // Execute selfdestruct items
    if (citemIDs !== null) {
      for (let n = citemIDs.length - 1; n >= 0; n -= 1) {
        const cItemTest = await auxMeth.getcItem(citemIDs[n].id, citemIDs[n].ciKey);
        if (cItemTest !== null) {
          // let citemObj = game.items.get(citemIDs[n].id).data.data;
          // let citemObjfinder = await auxMeth.getcItem(citemIDs[n].id,citemIDs[n].ciKey);

          const cItem = cItemTest.data.data;
          if (cItem.usetype === 'PAS' && cItem.selfdestruct) {
            for (let i = 0; i < cItem.mods.length; i += 1) {
              const mymod = cItem.mods[i];

              let attProp = 'value';
              let modAtt = mymod.attribute;

              if (modAtt.includes('.max')) {
                modAtt = modAtt.replace('.max', '');
                attProp = 'max';
              }

              if (mymod.type === 'ADD') {
                let { value } = mymod;
                let finalvalue;

                if (Number.isNaN(value)) {
                  if (value.charAt(0) === '|') {
                    value = value.replace('|', '');
                    finalvalue = await auxMeth.autoParser(value, attributes, cItem.attributes, true, false, parseInt(cItem.number, 10));
                  } else {
                    finalvalue = await auxMeth.autoParser(value, attributes, cItem.attributes, false, false, parseInt(cItem.number, 10));
                  }
                } else {
                  finalvalue = value;
                }

                const myAtt = actorData.data.attributes[modAtt];

                if (attProp === 'value') {
                  myAtt.value += Number(finalvalue);
                }
                if (attProp === 'max') {
                  myAtt.max += Number(finalvalue);
                }
              }
            }
          }
        }
      }
    }

    const checkmods = await this.getMods(originalcIDs);

    if (checkmods.length !== mods.length && !repeat) {
      // console.log("repeating");
      actorData = await this.checkPropAuto(actorData, true);
    }

    // console.log(citemIDs);
    // console.log(actorData);
    return actorData;
  }

  async autoCalculateAttributes(actorData, attributearray, attributes, _checker, secondround = false) {
    // Checking AUTO ATTRIBUTES -= 1 KEEP DEFAULT VALUE EMPTY THEN!!
    let ithaschanged = false;
    const parser = new DOMParser();

    const htmlcode = await auxMeth.getTempHTML(this.data.data.gtemplate);

    if (htmlcode === null) {
      ui.notifications.warn('Please rebuild character sheet before assigning, a-entity');
      return Promise.reject();
    }

    const form = await parser.parseFromString(htmlcode, 'text/html').querySelector('form');
    const inputs = await form.querySelectorAll('input,select,textarea,.radio-input');
    const sheetAtts = [];
    for (let i = 0; i < inputs.length; i += 1) {
      const newAtt = inputs[i];
      const attId = newAtt.getAttribute('attId');

      let attKey = newAtt.getAttribute('name');
      attKey = attKey.replace('data.attributes.', '');
      attKey = attKey.replace('.value', '');

      let properKey;
      if (attId !== null) {
        // properKey = game.items.get(attId);
        properKey = await auxMeth.getTElement(attId, 'property', attKey);
      }

      if (properKey !== null) {
        sheetAtts.push(properKey.data.data.attKey);
      }
    }

    for (let i = 0; i < attributearray.length; i += 1) {
      const attribute = attributearray[i];
      const findme = sheetAtts.filter((y) => y === attribute);

      if (actorData.data.attributes[attribute] !== null) {
        const attID = actorData.data.attributes[attribute].id;
        ithaschanged = await this.setAutoProp(attID, attributes, attribute, findme, ithaschanged, secondround);
      } else if (findme.length > 0) {
        ui.notifications.warn(`Please rebuild/reload template, attribute ${attribute} not found in actor`);
      }
    }

    return ithaschanged;
  }

  async setAutoProp(attID, attributes, attribute, findme, ithaschanged, _secondround = false) {
    if ((attribute !== null || attribute !== undefined) && findme.length > 0) {
      // const attdata = attributes[attribute];
      let rawexp = '';

      // let property = await game.items.get(attID);
      const property = await auxMeth.getTElement(attID, 'property', attribute);
      const actorAtt = attributes[attribute];

      // Check the Auto value
      if (property !== null) {
        let exprmode = false;
        const { datatype } = property.data.data;
        if (datatype !== 'simplenumeric' && datatype !== 'radio') {
          exprmode = true;
        }

        rawexp = property.data.data.auto;

        if (rawexp.includes('.totals')) {
          actorAtt.hastotals = true;
        }

        const propCheck = rawexp.match(/(?<=@\{).*?(?=\})/g);
        if (propCheck !== null) {
          for (let n = 0; n < propCheck.length; n += 1) {
            let _rawattname = propCheck[n];
            // let _attProp = 'value';
            // let _attvalue;
            let attTotal;

            if (_rawattname.includes('.max')) {
              _rawattname = _rawattname.replace('.max', '');
              // _attProp = 'max';
            }

            // if (_rawattname.includes('.totals.')) {
            //   const splitter = _rawattname.split('.');
            //   _rawattname = splitter[0];
            //   attTotal = splitter[2];
            //   _attProp = 'total';
            // }

            let _myatt = attributes[_rawattname];
            if (_myatt !== null) {
              if (attTotal !== null && attTotal !== '') {
                _myatt = attributes[_rawattname].totals[attTotal];
              }

              //                            if(!_myatt.isset){
              //                                actorAtt.isset = false;
              //                            }
            }
          }
        }

        if (rawexp !== '') {
          let newvalue = actorAtt.value;
          rawexp = await this.expandPropsP(rawexp, attributes);

          if (!actorAtt.isset) {
            newvalue = await auxMeth.autoParser(rawexp, attributes, null, exprmode);
            if (actorAtt.value !== newvalue) {
              ithaschanged = true;
            }
            actorAtt.default = true;
          }

          // TEST TO REINSTATE
          actorAtt.value = newvalue;
          // TEST TO DELETE
          // if (property.data.data.datatype !== "simpletext" && !secondround)
          if (property.data.data.datatype !== 'simpletext') {
            actorAtt.value = Number(newvalue) + Number(actorAtt.autoadd);
          }
        }

        const { automax } = property.data.data;
        if (automax !== '') {
          rawexp = automax;

          rawexp = await this.expandPropsP(rawexp, attributes);

          let maxval = actorAtt.max;

          if (!actorAtt.maxblocked && !actorAtt.maxset) { maxval = await auxMeth.autoParser(rawexp, attributes, null, false); }

          // TEST TO DELETE
          if (datatype !== 'simpletext') {
            maxval = Number(maxval) + Number(actorAtt.maxadd);
          }

          if (!actorAtt.maxexec) {
            actorAtt.maxexec = true;

            if (actorAtt.max === '' || !actorAtt.maxblocked) {
              actorAtt.max = parseInt(maxval, 10);
              actorAtt.maxblocked = false;
              ithaschanged = true;
            }
          }

          if (parseInt(actorAtt.value, 10) > actorAtt.max && property.data.data.maxtop) {
            actorAtt.value = actorAtt.max;
          }
        }
      }
    }
    return ithaschanged;
  }

  async expandPropsP(rawexp, attributes) {
    rawexp = await this.parseRegs(rawexp, attributes);

    const propCheck = rawexp.match(/(?<=@\{).*?(?=\})/g);
    if (propCheck !== null) {
      for (let n = 0; n < propCheck.length; n += 1) {
        let _rawattname = propCheck[n];
        const tochange = `@{${_rawattname}}`;
        let _attProp = 'value';
        let _attAuto = 'auto';
        // let _attvalue;

        if (_rawattname.includes('.max')) {
          _rawattname = _rawattname.replace('.max', '');
          _attProp = 'max';
          _attAuto = 'automax';
        }

        const propertybase = await game.items.filter((y) => y.data.type === 'property' && y.data.data.attKey === _rawattname);
        const property = propertybase[0];

        //                if(attributes[_rawattname]==null)
        //                    ui.notifications.warn("Attribute " + _rawattname + " used in expression not found in actor");

        if (property !== null && attributes[_rawattname] !== null) {
          let exchanger = attributes[_rawattname][_attProp];
          const attAutoAdd = Number(attributes[_rawattname].autoadd);
          const { data } = property.data;

          if (data[_attAuto] !== '') {
            if (data[_attAuto].includes('$<')) {
              rawexp = await this.parseRegs(rawexp, attributes);
            }

            if (!attributes[_rawattname].isset) {
              exchanger = await this.expandPropsP(data[_attAuto], attributes);
            } else {
              exchanger = attributes[_rawattname].value;
            }
          }

          const { datatype } = data;
          if (datatype !== 'simpletext' && attAutoAdd !== null && attAutoAdd !== 0) {
            exchanger = `((${exchanger})+(${attAutoAdd}))`;
          }

          rawexp = rawexp.replace(tochange, exchanger);
        }
      }
    }

    return rawexp;
  }

  async parseRegs(expr, attributes) {
    const regArray = [];
    const expreg = expr.match(/(?<=\$<).*?(?=>)/g);

    if (expreg !== null) {
      // Substitute string for current value
      for (let i = 0; i < expreg.length; i += 1) {
        const attname = `$<${expreg[i]}>`;
        const attvalue = '';

        const regblocks = expreg[i].split(';');

        const regobject = {};
        regobject.index = regblocks[0];
        regobject.expr = expreg[i].replace(`${regblocks[0]};`, '');

        const internalvBle = regobject.expr.match(/(?<=\$)[0-9]+/g);
        if (internalvBle !== null) {
          for (let k = 0; k < internalvBle.length; k += 1) {
            const regindex = internalvBle[k];
            const regObj = await regArray.find((y) => y.index === regindex);
            let vbvalue = '';

            if (regObj !== null) {
              vbvalue = regObj.result;
            }

            regobject.expr = regobject.expr.replace(`$${regindex}`, vbvalue);
          }
        }

        // TO REVERT HAS METIDO ESTO!!!!

        regobject.expr = await this.expandPropsP(regobject.expr, attributes);
        regobject.expr = await auxMeth.autoParser(regobject.expr, attributes, null, false);

        //                let parseexp = /\if\[|\bmax\(|\bmin\(|\bsum\(|\%\[|\bfloor\(|\bceil\(|\bcount[E|L|H]\(/g;
        //                let parsecheck = regobject.expr.match(parseexp);
        //                let numbexp = /^[0-9]*$/g;
        //                let numbcheck = regobject.expr.match(numbexp);
        //
        //                if(!parsecheck && numbcheck){
        //                    regobject.expr = eval(regobject.expr);
        //                }

        regobject.result = regobject.expr;
        await regArray.push(regobject);

        expr = expr.replace(attname, attvalue);
      }

      const exprparse = expr.match(/(?<=\$)[0-9]+/g);
      if (exprparse !== null) {
        for (let i = 0; i < exprparse.length; i += 1) {
          const regindex = exprparse[i];

          const attname = `$${regindex}`;
          const regObj = regArray.find((y) => y.index === regindex);

          let attvalue = '';
          if (regObj !== null) {
            attvalue = regObj.result;
          }

          expr = expr.replace(attname, attvalue);
        }
      }
    }
    return expr;
  }

  async actorUpdater(data = null) {
    //        if(!this.owner)
    //            return;

    let newData = data;
    if (newData === null) {
      ({ data } = this);
    }

    if (!data.data.istemplate) {
      newData = await this.checkPropAuto(data);
    }
    // newData = await this.checkPropAuto(data);

    return newData;
  }

  async rollSheetDice(rollexp, rollname, rollid, actorattributes, citemattributes, number = 1, target = null, rollcitemID = null, tokenID = null) {
    let initiative = false;
    let gmmode = false;
    let blindmode = false;
    let nochat = false;

    if (rollexp.includes('~blind~')) {
      blindmode = true;
    }

    let linkmode = false;

    if (rollcitemID) {
      linkmode = true;
    }

    let ToGM = false;
    let rolltotal = 0;
    let conditionalText = '';
    let diff = await game.settings.get('sandbox', 'diff');

    if (diff === null) {
      diff = 0;
    }

    if (Number.isNaN(diff)) {
      diff = 0;
    }

    let rollformula = rollexp;

    // Roll modifiers generated by MODs of ROLL type
    const actorrolls = this.data.data.rolls;

    // Rolls defined by expression
    const subrolls = [];

    // Check roll mode
    let { rollmode } = this.data.data;
    if (citemattributes !== null) {
      rollname = rollname.replace(/#{name}/g, citemattributes.name);
    }

    // Parse roll difficulty in name, and general atts
    rollname = rollname.replace(/#{diff}/g, diff);
    rollname = await auxMeth.autoParser(rollname, actorattributes, citemattributes, true, false, number);

    // Parse roll difficulty
    rollexp = rollexp.replace(/#{diff}/g, diff);
    if (citemattributes !== null) {
      rollexp = await rollexp.replace(/#{name}/g, citemattributes.name);
    }

    // Parse target attributes
    const targetexp = rollexp.match(/(?<=#{target\|)\S*?(?=\})/g);
    if (targetexp !== null) {
      for (let j = 0; j < targetexp.length; j += 1) {
        const idexpr = targetexp[j];
        const idtoreplace = `#{target|${targetexp[j]}}`;
        let newid;

        if (target !== null) {
          const targetattributes = target.actor.data.data.attributes;
          newid = await auxMeth.autoParser(`__${idexpr}__`, targetattributes, null, true);
        }

        if (newid === null) {
          newid = 0;
        }

        rollexp = rollexp.replace(idtoreplace, newid);
        rollformula = rollformula.replace(idtoreplace, newid);
      }
    }

    // Preparsing TO CHECK IF VALID EXPRESSION!!!
    rollexp = await auxMeth.autoParser(rollexp, actorattributes, citemattributes, true, false, number);
    while (rollexp.match(/(?<=\broll\b\().*?(?=\))/g) !== null) {
      const rollmatch = /\broll\(/g;
      let rollResultResultArray = rollmatch.exec(rollexp);
      const rollResult = [];

      while (rollResultResultArray) {
        const suba = rollexp.substring(rollmatch.lastIndex, rollexp.length);
        const subb = auxMeth.getParenthesString(suba);
        rollResult.push(subb);
        rollResultResultArray = rollmatch.exec(rollexp);
      }

      const subrollsexpb = rollResult;

      // Parse Roll
      const tochange = `roll(${subrollsexpb[0]})`;
      const blocks = subrollsexpb[0].split(';');

      // Definition of sub Roll
      const sRoll = {};

      sRoll.name = blocks[0];
      sRoll.numdice = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
      sRoll.numdice = parseInt(sRoll.numdice, 10);
      sRoll.faces = await auxMeth.autoParser(blocks[2], actorattributes, citemattributes, false, false, number);
      sRoll.color = blocks[4] || '';
      sRoll.exploding = blocks[3];

      if (parseInt(sRoll.numdice, 10) > 0) {
        let exploder = '';
        if (sRoll.exploding === 'true' || sRoll.exploding === 'add') {
          exploder = `x${sRoll.faces}`;
        }

        sRoll.expr = `${sRoll.numdice}d${sRoll.faces}${exploder}${sRoll.color}`;

        if (sRoll.numdice < 1) {
          sRoll.expr = '0';
        }

        // 1d0 roll protection
        sRoll.expr = sRoll.expr.replace(/[0-9]+d0/g, '0');
        sRoll.expr = sRoll.expr.replace(/(?<![0-9])0x\d+/g, '0');
        const partroll = new Roll(sRoll.expr);

        const finalroll = await partroll.evaluate({ async: true });

        finalroll.extraroll = true;

        if (game.dice3d !== null) {
          await game.dice3d.showForRoll(partroll, game.user, true, ToGM, blindmode);
        }

        sRoll.results = finalroll;

        await subrolls.push(sRoll);
      }

      rollexp = rollexp.replace(tochange, '');
      rollformula = rollformula.replace(tochange, `${sRoll.numdice}d${sRoll.faces}`);

      const exptochange = `\\?\\[\\b${sRoll.name}\\]`;

      const re = new RegExp(exptochange, 'g');

      const mysubRoll = subrolls.find((y) => y.name === sRoll.name);
      let finalvalue = '';

      if (sRoll.results !== null) {
        for (let j = 0; j < sRoll.results.dice.length; j += 1) {
          const dicearray = sRoll.results.dice[j].results;

          for (let k = 0; k < dicearray.length; k += 1) {
            if (k > 0) {
              finalvalue += ',';
            }

            let rollvalue = dicearray[k].result;

            if (mysubRoll.exploding === 'add') {
              while (dicearray[k].exploded && k < dicearray.length) {
                k += 1;
                rollvalue += dicearray[k].result;
              }
            }

            finalvalue += rollvalue;
          }
        }

        if (sRoll.results.dice.length === 0) {
          finalvalue += '0';
        }
      } else {
        finalvalue = 0;
      }

      rollformula = rollformula.replace(re, `${sRoll.numdice}d${sRoll.faces}`);
      rollexp = rollexp.replace(re, finalvalue);
      rollexp = await auxMeth.autoParser(rollexp, actorattributes, citemattributes, true, false, number);
      rollformula = rollexp;
      // }
    }

    rollexp = await auxMeth.autoParser(rollexp, actorattributes, citemattributes, true, false, number);

    // PARSING FOLL FORMULA, TO IMPROVE!!!
    const sumResult = rollformula.match(/(?<=\bsum\b\().*?(?=\))/g);
    if (sumResult !== null) {
      // Substitute string for current value
      for (let i = 0; i < sumResult.length; i += 1) {
        const splitter = sumResult[i].split(';');
        const comparer = splitter[0];
        const tochange = `sum(${sumResult[i]})`;
        rollformula = rollformula.replace(tochange, comparer);
      }
    }
    rollformula = rollformula.replace(/\bsum\b\(.*?\)/g, '');

    const countHResult = rollformula.match(/(?<=\bcountH\b\().*?(?=\))/g);
    if (countHResult !== null) {
      // Substitute string for current value
      for (let i = 0; i < countHResult.length; i += 1) {
        const splitter = countHResult[i].split(';');
        const comparer = splitter[0];
        const tochange = `countH(${countHResult[i]})`;
        rollformula = rollformula.replace(tochange, comparer);
      }
    }
    rollformula = rollformula.replace(/\bcountH\b\(.*?\)/g, '');

    const countLResult = rollformula.match(/(?<=\bcountL\b\().*?(?=\))/g);
    if (countLResult !== null) {
      // Substitute string for current value
      for (let i = 0; i < countLResult.length; i += 1) {
        const splitter = countLResult[i].split(';');
        const comparer = splitter[0];
        const tochange = `countL(${countLResult[i]})`;
        rollformula = rollformula.replace(tochange, comparer);
      }
    }

    rollformula = rollformula.replace(/\bcountL\b\(.*?\)/g, '');

    const countEResult = rollformula.match(/(?<=\bcountE\b\().*?(?=\))/g);
    if (countEResult !== null) {
      // Substitute string for current value
      for (let i = 0; i < countEResult.length; i += 1) {
        const splitter = countEResult[i].split(';');
        const comparer = splitter[0];
        const tochange = `countE(${countEResult[i]})`;
        rollformula = rollformula.replace(tochange, comparer);
      }
    }

    rollformula = rollformula.replace(/\bcountE\b\(.*?\)/g, '');

    // Check roll ids
    if (rollid === null) {
      rollid = [];
    }

    if (rollid === '') {
      rollid = [];
    }

    for (let n = 0; n < rollid.length; n += 1) {
      // console.log(rollid[n]);
      if (rollid[n] === 'init') {
        initiative = true;
      }

      if (rollid[n] === 'gm') {
        gmmode = true;
      }

      if (rollid[n] === 'blind') {
        blindmode = true;
      }

      if (rollid[n] === 'nochat') {
        nochat = true;
      }
    }

    // Remove rollIDs and save them
    const parseid = rollexp.match(/(?<=~)\S*?(?=~)/g);

    // ADV & DIS to rolls

    let findIF = rollexp.search('if');
    const findADV = rollexp.search('~ADV~');
    const findDIS = rollexp.search('~DIS~');

    // Checks if it is an IF and does not have any ADV/DIS modifier in the formula
    if (findADV === -1 && findDIS === -1) {
      // In this case it allows to parse the manual MOD in case there is any
      findIF = -1;
    }

    if (parseid !== null) {
      for (let j = 0; j < parseid.length; j += 1) {
        const idexpr = parseid[j];
        const idtoreplace = `~${parseid[j]}~`;
        const newid = await auxMeth.autoParser(idexpr, actorattributes, citemattributes, true, number);

        if (newid !== '') {
          rollid.push(newid);
        }

        if (parseid[j] === 'init') {
          initiative = true;
        }

        if (parseid[j] === 'gm') {
          gmmode = true;
        }

        if (parseid[j] === 'blind') {
          blindmode = true;
        }

        if (parseid[j] === 'nochat') {
          nochat = true;
        }

        if (findIF !== -1) {
          // We don't do anything - We will parse this into the IF function inside autoParser
        } else {
          if (parseid[j] === 'ADV') {
            rollmode = 'ADV';
          }

          if (parseid[j] === 'DIS') {
            rollmode = 'DIS';
          }

          rollexp = rollexp.replace(idtoreplace, '');
          rollformula = rollformula.replace(idtoreplace, '');
        }
      }
    }

    // Set ADV or DIS
    if (findIF !== -1) {
      // We don't do anything - We will parse this into the IF function inside autoParser
    } else {
      if (rollmode === 'ADV') {
        rollexp = rollexp.replace(/1d20/g, '2d20kh');
      }

      if (rollmode === 'DIS') {
        rollexp = rollexp.replace(/1d20/g, '2d20kl');
      }
    }

    if (gmmode) {
      ToGM = ChatMessage.getWhisperRecipients('GM');
    }

    // Parse Roll
    rollexp = await auxMeth.autoParser(rollexp, actorattributes, citemattributes, true, false, number);

    // //ADDer to target implementation - add(property, value)
    // let is_adding = false
    // let addblock = {};
    // let adder = rollexp.match(/(?<=\badd\b\().*?(?=\))/g);
    // if (adder !== null) {
    //     is_adding = true;
    //     for (let i = 0; i < adder.length; i+= 1) {
    //         let tochange = "add(" + adder[i] + ")";
    //         let blocks = adder[i].split(";");
    //         addblock.addprop = await auxMeth.autoParser(blocks[0], actorattributes, citemattributes, true, false, number);
    //         addblock.addvalue = 0;
    //         addblock.addvalue = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
    //         addblock.addvalue = Number(addblock.addvalue);

    //         rollexp = rollexp.replace(tochange, "");
    //         rollformula = rollformula.replace(tochange, "");
    //     }

    // }

    // // ADDER implementatin
    // if (target !== null && is_adding) {

    //     let targetattributes = target.actor.data.data.attributes;
    //     if (targetattributes[addblock.addprop] !== null) {
    //         let attvalue = parseInt(targetattributes[addblock.addprop].value);
    //         attvalue += parseInt(addblock.addvalue);

    //         let tokenId = target.id;

    //         this.requestToGM(this, tokenId, addblock.addprop, attvalue);
    //     }

    // }

    // //SETer to target implementation - set(property, value)
    // let is_seting = false
    // let setblock = {};
    // let setter = rollexp.match(/(?<=\bset\b\().*?(?=\))/g);
    // if (setter !== null) {
    //     is_seting = true;
    //     for (let i = 0; i < setter.length; i+= 1) {
    //         let tochange = "set(" + setter[i] + ")";
    //         let blocks = setter[i].split(";");
    //         setblock.setprop = await auxMeth.autoParser(blocks[0], actorattributes, citemattributes, true, false, number);
    //         setblock.setvalue = 0;
    //         setblock.setvalue = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
    //         setblock.setvalue = Number(setblock.setvalue);

    //         rollexp = rollexp.replace(tochange, "");
    //         rollformula = rollformula.replace(tochange, "");
    //     }

    // }

    // // SETER implementatin
    // if (target !== null && is_seting) {

    //     let targetattributes = target.actor.data.data.attributes;
    //     if (targetattributes[setblock.setprop] !== null) {
    //         //targetattributes[setblock.setprop].value = setblock.setvalue;
    //         //console.log("changing token prop " + setblock.setprop + " to " + targetattributes[setblock.setprop].value);

    //         let tokenId = target.id;
    //         //let mytoken = canvas.tokens.get(tokenId);
    //         this.requestToGM(this, tokenId, setblock.setprop, setblock.setvalue);
    //         //await mytoken.update({"data.attributes":targetattributes},{diff:false});
    //     }

    // }

    // Remove conditionalexp and save it
    const condid = rollexp.match(/(?<=&&)(.*?)(?=&&)/g);
    if (condid !== null) {
      for (let j = 0; j < condid.length; j += 1) {
        const condidexpr = condid[j];
        if (condidexpr.length > 2) {
          const conddtoreplace = `&&${condid[j]}&&`;
          let separador = '';

          if (j < condid.length - 1) {
            separador = '|';
          }

          conditionalText += condidexpr + separador;
          rollexp = rollexp.replace(conddtoreplace, '');
        }
      }
    }

    rollformula = rollformula.replace(/&&.*?&&/g, '');
    rollexp = rollexp.trim();

    const multiroll = [];

    // PARSE SUBROLLS
    const attpresult = rollexp.match(/(?<=··!)\S*?(?=!)/g);
    if (attpresult !== null) {
      // Substitute string for current value
      for (let i = 0; i < attpresult.length; i += 1) {
        //                let debugname = attpresult[i];
        const attname = `··!${attpresult[i]}!`;
        const attindex = attpresult[i];
        const attvalue = subrolls[parseInt(attindex, 10)].total;

        rollexp = rollexp.replace(attname, attvalue);
        rollformula = rollformula.replace(attname, subrolls[parseInt(attindex, 10)].expr);
      }
    }

    // Add ROLL MODS
    const extramod = 0;
    const extramodstring = '';
    for (let k = 0; k < rollid.length; k += 1) {
      if (rollid[k] !== '' && hasProperty(actorrolls, rollid[k])) {
        rollformula += actorrolls[rollid[k]].value;
        rollexp += actorrolls[rollid[k]].value;
      }
    }

    // EXPRESSION CATCHERS, REMOVE ANYTHING THAT DOES NOT NEED TO BE PARSED FOR RESULT

    // ADDer to target implementation - add(property;value)
    // ALONDAAR MOVED PARSING TO AFTER ROLLTOTAL
    const adder = rollexp.match(/(?<=\badd\b\().*?(?=\))/g);
    if (adder !== null) {
      for (let i = 0; i < adder.length; i += 1) {
        const tochange = `add(${adder[i]})`;
        rollexp = rollexp.replace(tochange, '');
        rollformula = rollformula.replace(tochange, '');
      }
    }

    // SETer to target implementation - set(property, value)
    // BASED ON ALONDAAR CODE
    const setter = rollexp.match(/(?<=\bset\b\().*?(?=\))/g);
    if (setter !== null) {
      for (let i = 0; i < setter.length; i += 1) {
        const tochange = `set(${setter[i]})`;
        rollexp = rollexp.replace(tochange, '');
        rollformula = rollformula.replace(tochange, '');
      }
    }

    // ALONDAAR -= 1 Rollable Table from expression: callRollTable(table_name;optional_value)
    const getRollableTables = rollexp.match(/(?<=\btable\b\().*?(?=\))/g);
    if (getRollableTables !== null) {
      for (let i = 0; i < getRollableTables.length; i += 1) {
        const tochange = `table(${getRollableTables[i]})`;
        rollexp = rollexp.replace(tochange, '');
        rollformula = rollformula.replace(tochange, '');
      }
    }

    // FIX FORMULA
    rollformula = await auxMeth.autoParser(rollformula, actorattributes, citemattributes, true, false, number);
    let formula = rollformula.replace(/\s[0]\s\+/g, '');
    formula = formula.replace(/(?<=~)(.*)(?=~)/g, '');
    formula = formula.replace(/~/g, '');

    // ROLL EXPRESSION - ROLL TOTAL
    const partroll = new Roll(rollexp);
    const roll = await partroll.evaluate({ async: true });
    if (game.dice3d !== null) {
      let rollblind = blindmode;
      let noshow = true;

      if (game.user.isGM) {
        rollblind = false;
      }

      if (blindmode) {
        noshow = false;
      }

      await game.dice3d.showForRoll(partroll, game.user, noshow, ToGM, rollblind);
    }

    rolltotal = roll.total;
    if (this.data.data.mod === '' || this.data.data.mod === null) {
      this.data.data.mod = 0;
    }
    rolltotal = parseInt(rolltotal, 10) + parseInt(this.data.data.mod, 10) + extramod;

    if (roll.formula.charAt(0) !== '-' || roll.formula.charAt(0) !== '0') {
      multiroll.push(roll);
    }

    // ALONDAAR REPLACE TOTAL IN ADDer WITH rolltotal, and applies all add() to target
    if (adder !== null) {
      for (let i = 0; i < adder.length; i += 1) {
        const blocks = adder[i].split(';');
        const parseprop = await auxMeth.autoParser(blocks[0], actorattributes, citemattributes, true, false, number);

        let parsevalue = 0;
        blocks[1] = blocks[1].replace(/\btotal\b/g, rolltotal);

        parsevalue = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
        parsevalue = Number(parsevalue);

        const parsedblock = {
          addprop: parseprop,
          addvalue: parsevalue,
        };

        // Apply ADDer to Target
        if (target !== null) {
          const targetattributes = target.actor.data.data.attributes;
          if (targetattributes[parsedblock.addprop] !== null) {
            let attvalue = parseInt(targetattributes[parsedblock.addprop].value, 10);
            attvalue += parseInt(parsedblock.addvalue, 10);

            const tokenId = target.id;
            await this.requestToGM(this, tokenId, parsedblock.addprop, attvalue);
          }
        }
      }
    }

    // ALONDAAR REPLACE TOTAL IN SETter WITH rolltotal, and applies all set() to target
    if (setter !== null) {
      for (let i = 0; i < setter.length; i += 1) {
        const blocks = setter[i].split(';');
        const parseprop = await auxMeth.autoParser(blocks[0], actorattributes, citemattributes, true, false, number);
        let parsevalue = 0;
        blocks[1] = blocks[1].replace(/\btotal\b/g, rolltotal);

        parsevalue = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
        parsevalue = Number(parsevalue);

        const parsedblock = {
          setprop: parseprop,
          setvalue: parsevalue,
        };

        // Apply ADDer to Target
        if (target !== null) {
          const targetattributes = target.actor.data.data.attributes;
          if (targetattributes[parsedblock.setprop] !== null) {
            let attvalue = parseInt(targetattributes[parsedblock.setprop].value, 10);
            attvalue = parseInt(parsedblock.setvalue, 10);

            const tokenId = target.id;
            await this.requestToGM(this, tokenId, parsedblock.setprop, attvalue);
          }
        }
      }
    }

    // CHECK CRITS AND FUMBLES TO COLOR THE ROLL
    let hascrit = false;
    let hasfumble = false;
    let rolldice;

    for (let j = 0; j < multiroll.length; j += 1) {
      const multirolldice = multiroll[j].dice;

      if (!hasProperty(multiroll[j], 'extraroll') && multirolldice.length > 0) {
        if (rolldice === null) {
          rolldice = multirolldice;
        } else {
          rolldice.push(multirolldice[0]);
        }
      }

      for (let i = 0; i < multirolldice.length; i += 1) {
        const maxres = multirolldice[i].faces;

        const _hascrit = multirolldice[i].results.includes(maxres);
        const _hasfumble = multirolldice[i].results.includes(1);

        if (_hascrit) {
          hascrit = true;
        }

        if (_hasfumble) {
          hasfumble = true;
        }
      }
    }

    // TEXT MANAGMENET
    let convalue = '';
    if (conditionalText !== '') {
      const blocks = conditionalText.split('|');

      for (let i = 0; i < blocks.length; i += 1) {
        let thiscond = blocks[i];
        if (thiscond.length > 1) {
          thiscond = thiscond.replace(/(?<=[\s|;|+|\-|*|/(|&|:])total(?=[\s|;|+|\-|*|/)])/g, rolltotal);

          const condblocks = thiscond.split(';');
          let checktype = condblocks[0];
          let mycondition = 0;

          checktype = checktype.replace(/(?<=[\s|;|+|\-|*|/(|&|:])total(?=[\s|;|+|\-|*|/)])/g, rolltotal);

          if (checktype === 'total') {
            mycondition += rolltotal;
          } else {
            mycondition = await auxMeth.autoParser(checktype, actorattributes, citemattributes, false, false, number);
          }

          let myeval = '';
          for (let j = 1; j < condblocks.length; j += 1) {
            let comma = '';
            if (j < condblocks.length - 1) {
              comma = ',';
            }

            myeval += condblocks[j] + comma;
          }

          const finaleval = `%[${mycondition},${myeval}]`;
          let finalevalvalue = await auxMeth.autoParser(finaleval, actorattributes, citemattributes, false, false, number);

          // REMOVES ARITHMETICAL EXPRESSION IN CONDITIONAL TEXTS!!!
          const parmatch = /\(/g;
          let parArray = parmatch.exec(finalevalvalue);
          const parresult = [];

          while (parArray) {
            const suba = finalevalvalue.substring(parmatch.lastIndex, finalevalvalue.length);
            const subb = auxMeth.getParenthesString(suba);
            parresult.push(subb);
            parArray = parmatch.exec(finalevalvalue);
          }

          if (parresult !== null) {
            // Substitute string for current value
            for (let j = 0; j < parresult.length; j += 1) {
              const hasletters = /[A-Za-z]+/g;
              const hassubfunctions = parresult[j].match(hasletters);

              if (!hassubfunctions) {
                // eslint-disable-next-line no-eval
                const parsedres = eval(parresult[j]);
                const tochax = `(${parresult[j]})`;
                finalevalvalue = finalevalvalue.replace(tochax, parsedres);
              }
            }
          }

          finalevalvalue = await auxMeth.autoParser(finalevalvalue, actorattributes, citemattributes, false, false, number);
          convalue += `${finalevalvalue} `;
        }
      }
    }

    // PREPARE TYE ROLL TEMPLATE
    const rollData = {
      token: {
        img: this.img,
        name: this.name,
      },
      actor: this.name,
      flavor: rollname,
      formula: formula + extramodstring,
      mod: this.data.data.mod,
      result: rolltotal,
      dice: rolldice,
      subdice: subrolls,
      user: game.user.name,
      conditional: convalue,
      iscrit: hascrit,
      isfumble: hasfumble,
      blind: blindmode,
      link: linkmode,
    };

    if (!nochat) {
      renderTemplate('systems/sandbox/templates/dice.html', rollData).then((html) => {
        const rolltype = document.getElementsByClassName('roll-type-select');
        const rtypevalue = rolltype[0].value;

        let rvalue = 0;
        if (rtypevalue === 'gmroll') {
          rvalue = 1;
        }

        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        const cilink = wrapper.querySelector('.roll-citemlink');

        if (cilink !== null) {
          cilink.setAttribute('id', rollcitemID);
        }

        const messageData = {
          content: wrapper.innerHTML,
          type: rvalue,
          blind: blindmode,
        };

        if (gmmode) {
          messageData.whisper = ChatMessage.getWhisperRecipients('GM');
        }

        const newmessage = ChatMessage.create(messageData);

        // if(game.user.isGM){
        auxMeth.rollToMenu(html);
        // }
      });
    }

    if (initiative) {
      await this.setInit(rollData.result, tokenID);
    }

    // ALONDAAR -= 1 Find and roll on the rollable table(s)
    if (getRollableTables !== null) {
      for (let i = 0; i < getRollableTables.length; i += 1) {
        const blocks = getRollableTables[i].split(';');
        const table = game.tables.getName(blocks[0]);

        let rmode = '';
        if (gmmode) {
          rmode += 'gmroll';
        }
        if (blindmode) {
          rmode += 'blindroll';
        }

        // If an optional value is supplied, use that instead of the Default Table Roll
        if (blocks[1] !== null) {
          if (blocks[1].includes('total')) {
            blocks[1] = blocks[1].replaceAll(/total/ig, rolltotal);
          }

          let parsevalue = 0;
          parsevalue = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
          parsevalue = Number(parsevalue);
          // For some reason it doesn't like just a value
          const tableRoll = await new Roll(`0+${parsevalue}`);

          table.draw({ roll: tableRoll, rollMode: rmode });
        } else {
          table.draw({ rollMode: rmode });
        }
      }
    }

    return rollData.result;
  }

  sendMsgChat(flavor, msg, submsg) {
    const rollData = {
      token: {
        img: this.img,
        name: this.name,
      },
      actor: this.name,
      flavor,
      msg,
      user: game.user.name,
      submsg,
    };

    renderTemplate('systems/sandbox/templates/msg.html', rollData).then((html) => {
      ChatMessage.create({
        content: html,
      });

      // if(game.user.isGM){
      auxMeth.rollToMenu(html);
      // }
    });
  }

  async requestToGM(myactor, tokenId, attkey, attvalue) {
    const mytoken = canvas.tokens.get(tokenId);
    if (mytoken === null) { return; }

    if (game.user.isGM) {
      if (myactor.istoken) {
        await mytoken.update({ [`actorData.data.attributes.${attkey}.value`]: attvalue });
      } else {
        // console.log("updating linked token");
        // let actorref = game.actors.get(myactor.data.id);
        await mytoken.actor.update({ [`data.attributes.${attkey}.value`]: attvalue });
        // actorref.data.data.attributes[attkey].value = attvalue;
        // await actorref.update(actorref.data,{diff:false});
      }
    } else {
      game.socket.emit('system.sandbox', {
        op: 'target_edit',
        user: game.user.id,
        scene: canvas.scene.id,
        actorId: mytoken.actor.id,
        tokenId,
        attkey,
        attvalue,
        istoken: myactor.istoken,
      });
    }
  }

  async requestTransferToGM(actorID, ownerID, citemID, number) {
    game.socket.emit('system.sandbox', {
      op: 'transfer_edit',
      user: game.user.id,
      actorID,
      ownerID,
      citemID,
      number,
    });
  }

  async handleTransferEdit(data) {
    if (!game.user.isGM) {
      return Promise.reject();
    }

    const actorOwner = game.actors.get(data.ownerID);
    const ownercItems = duplicate(actorOwner.data.data.citems);

    const cItem = ownercItems.find((y) => y.id === data.citemID);
    cItem.number -= data.number;

    const actorReceiver = game.actors.get(data.actorID);
    const receivercItems = duplicate(actorReceiver.data.data.citems);

    try {
      await actorOwner.update({ 'data.citems': ownercItems });
      return true;
    } catch (err) {
      const cItemRec = receivercItems.find((y) => y.id === data.citemID);

      if (cItemRec !== null) {
        cItemRec.number -= data.number;
      }

      await actorReceiver.update({ 'data.citems': receivercItems });
    }

    return Promise.reject();
  }

  async handleTargetRequest(data) {
    if (!game.user.isGM) {
      return Promise.reject();
    }

    const mytoken = canvas.tokens.get(data.tokenId);
    if (data.istoken) {
      await mytoken.update({ [`actorData.data.attributes.${data.attkey}.value`]: data.attvalue });
    } else {
      await mytoken.actor.update({ [`data.attributes.${data.attkey}.value`]: data.attvalue });
    }

    return Promise.resolve();
  }

  async setInit(roll, tokenID = null) {
    const tokens = canvas.tokens.ownedTokens;

    let combatants;
    const initCombatant = (combatKey) => {
      const _combatant = combatants.get(combatKey);
      if (_combatant.token.id === tokenID) {
        game.combat.updateEmbeddedDocuments('Combatant', [{ _id: _combatant.id, initiative: roll }]);
        // _combatant.rollInitiative(roll);
      }
    };

    for (let i = 0; i < tokens.length; i += 1) {
      const token = tokens[i];
      const tokenactor = token.actor;
      const myactor = this;

      if (myactor.id === tokenactor.id) {
        if (!tokenactor.isToken && tokenID === null) {
          tokenID = token.id;
        }

        // The following is for initiative
        ({ combatants } = game.combat);
        Object.keys(combatants).forEach(initCombatant);
      }
    }

    // THIS IS THE MACRO FOR NPCS ROLLS/INITIATIVE!!!
    //        ( async () => {
    //        let rollexp = "ROLEXPRESION";
    //        let rollname = "INICIATIVA";
    //            const selected = canvas.tokens.controlledTokens;
    //
    //            for(let i=0;i<selected.length;i+= 1){
    //                let token = selected[i];
    //                const actor = token.actor;
    //                let result = await actor.rollSheetDice(rollexp,rollname,null,actor.data.data.attributes,null);
    //                //The following is for initiative
    //                const combatants = game.combat.combatants;
    //                for(let j=0;j<combatants.length;j+= 1){
    //                    let _combatant = game.combat.combatants[j];
    //
    //                    if(_combatant.tokenId === token.data.id){
    //
    //                        game.combat.updateCombatant({_id: _combatant.id, initiative: result});
    //                    }
    //
    //                }
    //
    //            }
    //        }
    //        )();

    // THIS IS THE MACRO FOR CITEM NPCS ROLLS!!!
    //        ( async () => {
    //            let propKey = "tiradaataquepnj";
    //            let citemname = "Ataque 1";
    //
    //            let property = game.items.find(y=>y.type=="property" && y.data.data.attKey==propKey);
    //            let citemattributes;
    //            const selected = canvas.tokens.controlledTokens;
    //
    //            for(let i=0;i<selected.length;i+= 1){
    //                let token = selected[i];
    //                const actor = token.actor;
    //
    //                let citem = actor.data.data.citems.find(y=>y.name === citemname);
    //                if(citem==null)
    //                    return;
    //
    //                citemattributes = citem.attributes;
    //
    //                let rollexp = property.data.data.rollexp;
    //                let rollname = property.data.data.rollname;
    //                rollname = rollname.replace("#{name}",citem.name);
    //                let result = await actor.rollSheetDice(rollexp,rollname,null,actor.data.data.attributes,citemattributes);
    //
    //            }
    //        }
    //        )();

    // THIS IS THE MACRO FOR NPC ATTRIBUTE ROLLS!
    //        ( async () => {
    //            let propKey = "punteria";
    //
    //            let property;
    //            let citemattributes;
    //            const selected = canvas.tokens.controlled;
    //
    //            for(let i=0;i<selected.length;i+= 1){
    //                let token = selected[i];
    //                const actor = token.actor;
    //
    //                property = game.items.find(y=>y.type=="property" && y.data.data.attKey==propKey);
    //                if(property==null)
    //                    return;
    //
    //                let rollexp = property.data.data.rollexp;
    //                let rollname = property.data.data.rollname;
    //                rollname = rollname.replace("#{name}",citem.name);
    //                let result = await actor.rollSheetDice(rollexp,rollname,null,actor.data.data.attributes,citemattributes);
    //
    //            }
    //        }
    //        )();
  }
}

export default gActor;
