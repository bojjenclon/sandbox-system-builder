import auxMeth from './auxmeth.js';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
class gActorSheet extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['sandbox', 'sheet', 'actor'],
      scrollY: ['.sheet-body', '.scrollable', '.tab'],
      width: 650,
      height: 600,
      tabs: [{ navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'description' }],
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
    const { actor } = this;
    const data = super.getData();
    const { flags } = actor.data;

    return data;
  }

  /* -------------------------------------------- */

  /** @override */
  //    get template() {
  //        return this.getHTMLPath();
  //    }

  async maximize() {
    const _mytemplate = await game.actors.find((y) => y.data.data.istemplate && y.data.data.gtemplate === this.actor.data.data.gtemplate);
    if (_mytemplate !== null) {
      this.position.height = _mytemplate.data.data.setheight;
    }
    super.maximize();
  }

  async _renderInner(data, options) {
    let templateHTML = await auxMeth.getTempHTML(this.actor.data.data.gtemplate, this.actor.data.data.istemplate);

    // IMPORTANT!! ANY CHECKBOX IN TEMPLATE NEEDS THIS!!!
    templateHTML = templateHTML.replace('{{checked="" actor.data.biovisible}}=""', '{{checked actor.data.biovisible}}');
    templateHTML = templateHTML.replace('{{checked="" actor.data.resizable}}=""', '{{checked actor.data.resizable}}');
    templateHTML = templateHTML.replace('{{checked="" actor.data.istemplate}}=""', '{{checked actor.data.istemplate}}');

    const template = await Handlebars.compile(templateHTML);

    const html = template(duplicate(data));
    this.form = $(html)[0];

    if (html === '') {
      throw new Error('No data was returned from template');
    }

    return $(html);
  }

  async getTemplateHTML(_html) {
    if (this.actor.data.data.istemplate && this.actor.data.data.gtemplate !== 'Default') {
      const _template = game.actors.find((y) => y.data.data.istemplate && y.data.data.gtemplate === this.actor.data.data.gtemplate);
      const html = _template.data.data._html;
      return html;
    }

    return _html;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    // console.log(html);
    super.activateListeners(html);

    const { actor } = this;

    // Activate tabs
    const tabs = html.find('.tabs');
    const initial = this._sheetTab;

    const _ = new TabsV2(tabs, {
      initial,
      callback: (clicked) => {
        this._sheetTab = clicked.data('tab');
        const li = clicked.parents('.tabs');
        const alltabs = li.children();
        //                for(let i=0;i<alltabs.length;i += 1){
        //                    let tab = alltabs[i];
        //                    let datatab = tab.getAttribute("data-tab");
        //                    if(datatab==clicked.data("tab")){
        //                        actor.data.flags.selectedtab = i;
        //                    }
        //
        //                }
      },
    });

    html.find('.tab-button').click((evt) => {
      evt.preventDefault();

      const tabButtons = $(this._element)[0].getElementsByClassName('tab-button');
      let firstpassed = false;

      for (let x = 0; x < tabButtons.length; x += 1) {
        if (tabButtons[x].classList.contains('underlined')) {
          tabButtons[x].className = tabButtons[x].className.replace('underlined', '');
        }

        if (tabButtons[x].classList.contains('visible-tab') && !firstpassed) {
          firstpassed = true;
          this._tabs[0].firstvisible = tabButtons[x].dataset.tab;
        }
      }

      const thistab = $(evt.currentTarget);
      thistab[0].className += ' underlined';
    });

    html.find('.macrobutton').click((evt) => {
      evt.preventDefault();
      const li = $(evt.currentTarget);
      const macroId = $(evt.currentTarget).attr('macroid');
      const macro = game.macros.get(macroId);
      macro.execute();
    });

    html.find('.badge-click').click(async (evt) => {
      evt.preventDefault();
      const { attributes } = this.actor.data.data;

      const attKey = $(evt.currentTarget).attr('attKey');
      const attId = $(evt.currentTarget).attr('attId');
      // let property = game.items.get(attId);
      const property = await auxMeth.getTElement(attId, 'property', attKey);

      const oldvalue = parseInt(attributes[attKey].value, 10);
      if (oldvalue < 1) {
        return;
      }

      let newvalue = oldvalue - 1;
      if (newvalue < 0) {
        newvalue = 0;
      }

      if (newvalue > attributes[attKey].max) {
        newvalue = attributes[attKey].max;
      }

      let stringvalue = '';
      stringvalue = newvalue.toString();

      await this.actor.update({ [`data.attributes.${attKey}.value`]: stringvalue });

      this.actor.sendMsgChat('USES 1 ', property.data.data.tag, `TOTAL: ${newvalue}`);
      this._onRollCheck(attId, attKey, null, null, false);
      // this.actor.sendMsgChat("Utiliza 1",property.data.data.tag, "Le quedan " + newvalue); to  this.actor.sendMsgChat("Uses 1",property.data.data.tag, "Remains " + newvalue);
    });

    html.find('.badge-clickgm').click(async (evt) => {
      evt.preventDefault();
      const { attributes } = this.actor.data.data;

      const attKey = $(evt.currentTarget).attr('attKey');
      let newvalue = parseInt(attributes[attKey].value, 10) + 1;
      if (newvalue > attributes[attKey].max) {
        newvalue = attributes[attKey].max;
      }

      let stringvalue = '';
      stringvalue = newvalue.toString();

      await this.actor.update({ [`data.attributes.${attKey}.value`]: stringvalue });
    });

    html.find('.arrup').click(async (evt) => {
      evt.preventDefault();
      const { attributes } = this.actor.data.data;

      const attKey = evt.target.parentElement.getAttribute('attKey');
      const newvalue = parseInt(attributes[attKey].value, 10) + 1;

      let stringvalue = '';
      stringvalue = newvalue.toString();

      await this.actor.update({ [`data.attributes.${attKey}.value`]: stringvalue });
    });

    html.find('.arrdown').click(async (evt) => {
      evt.preventDefault();
      const { attributes } = this.actor.data.data;

      const attKey = evt.target.parentElement.getAttribute('attKey');
      const newvalue = parseInt(attributes[attKey].value, 10) - 1;

      let stringvalue = '';
      stringvalue = newvalue.toString();

      await this.actor.update({ [`data.attributes.${attKey}.value`]: stringvalue });
    });

    html.find('.propheader').click((evt) => {
      evt.preventDefault();

      const attKey = $(evt.currentTarget).attr('attKey');
      const tableKey = evt.target.parentElement.getAttribute('tableKey');
      if (this.sortOption === null) {
        this.sortOption = {};
      }
      this.sortOption[tableKey] = attKey;
      this.render(true);
    });

    html.find('.nameheader').click((evt) => {
      evt.preventDefault();

      const attKey = $(evt.currentTarget).attr('attKey');
      const tableKey = evt.target.parentElement.getAttribute('tableKey');
      if (this.sortOption === null) {
        this.sortOption = {};
      }
      this.sortOption[tableKey] = 'name';
      this.render(true);
    });

    html.find('.rollable').click((evt) => {
      evt.preventDefault();
      const attId = $(evt.currentTarget).attr('attid');
      const citemId = $(evt.currentTarget).attr('item_id');
      const attKey = $(evt.currentTarget).attr('id');
      this._onRollCheck(attId, attKey, citemId, null, false);
    });

    html.find('.customcheck').click((evt) => {
      evt.preventDefault();

      const attKey = $(evt.currentTarget).attr('attKey');
      if (this.actor.data.data.attributes[attKey] === null) {
        return;
      }

      const currentvalue = this.actor.data.data.attributes[attKey].value;
      let finalvalue = true;
      if (currentvalue) {
        finalvalue = false;
      }

      this.actor.update({ [`data.attributes.${attKey}.value`]: finalvalue, [`data.attributes.${attKey}.modified`]: true });
    });

    html.find('.roll-mode').click((evt) => {
      evt.preventDefault();

      const elemCode = $(evt.currentTarget)[0].children[0];
      const actorData = this.actor.data.data;

      if (elemCode.textContent === '1d20') {
        actorData.rollmode = 'ADV';
      } else if (elemCode.textContent === 'ADV') {
        actorData.rollmode = 'DIS';
      } else {
        actorData.rollmode = '1d20';
      }

      this.actor.update({ 'data.rollmode': actorData.rollmode }, { diff: false });
    });

    html.find('.tab-prev').click((evt) => {
      evt.preventDefault();

      // this.displaceTabs(true,html);
      this.displaceTabs2('prev', html);
    });

    html.find('.tab-next').click((evt) => {
      evt.preventDefault();

      // this.displaceTabs(false,html);
      this.displaceTabs2('next', html);
    });

    html.find('.roll-free').click((evt) => {
      evt.preventDefault();

      const d = new Dialog({
        title: 'Select Items',
        content: '<input class="dialog-dice" type=text id="dialog-dice" value=1d6>',
        buttons: {
          one: {
            icon: '<i class="fas fa-check"></i>',
            label: 'OK',
            callback: async () => {
              const diceexpr = html[0].getElementsByClassName('dialog-dice');
              // console.log(diceexpr[0]);
              const finalroll = this.actor.rollSheetDice(diceexpr[0].value, 'Free Roll', '', this.actor.data.data.attributes, null);
              // let finalroll = this.actor.rollSheetDice(rollexp,rollname,rollid,actorattributes,citemattributes,number,tokenid);
            },
          },
          two: {
            icon: '<i class="fas fa-times"></i>',
            label: 'Cancel',
            callback: () => {
              console.log('canceling dice');
            },
          },
        },
        default: 'one',
        close: () => {
          console.log('Item roll dialog was shown to player.');
        },
      });
      d.render(true);
    });

    html.find('.mod-selector').click(async (evt) => {
      evt.preventDefault();

      // Get items
      const { citems } = this.actor.data.data;
      const allselitems = citems.filter((y) => y.selection !== null);
      const selectcitems = allselitems.find((y) => y.selection.find((x) => !x.selected));
      if (selectcitems === null) {
        return;
      }

      // let citemplate = game.items.get(selectcitems.id);
      const citemplate = await auxMeth.getcItem(selectcitems.id, selectcitems.ciKey);
      const acitem = selectcitems.selection.find((y) => !y.selected);

      const modindex = acitem.index;
      const mod = citemplate.data.data.mods.find((y) => y.index === modindex);

      // Right Content
      const newList = document.createElement('DIV');
      newList.className = 'item-dialog';
      newList.setAttribute('actorId', this.actor.id);

      // Fill options
      if (mod.type === 'ITEM') {
        const finalnum = await auxMeth.autoParser(mod.selectnum, this.actor.data.data.attributes, acitem.attributes, false);
        newList.setAttribute('selectnum', finalnum);
        const text = document.createElement('DIV');

        text.className = 'centertext';
        text.textContent = `Please select ${finalnum} items:`;
        newList.appendChild(text);

        for (let n = 0; n < mod.items.length; n += 1) {
          const ispresent = citems.some((y) => y.id === mod.items[n].id);

          if (!ispresent) {
            const newItem = document.createElement('DIV');
            newItem.className = 'flexblock-center-nopad';

            const newcheckBox = document.createElement('INPUT');
            newcheckBox.className = 'dialog-check';
            newcheckBox.setAttribute('type', 'checkbox');
            newcheckBox.setAttribute('itemId', mod.items[n].id);
            newcheckBox.setAttribute('ciKey', mod.items[n].ciKey);

            const itemDescription = document.createElement('LABEL');
            itemDescription.textContent = mod.items[n].name;
            itemDescription.className = 'linkable';
            itemDescription.setAttribute('itemId', mod.items[n].id);
            itemDescription.setAttribute('ciKey', mod.items[n].ciKey);

            newItem.appendChild(newcheckBox);
            newItem.appendChild(itemDescription);
            newList.appendChild(newItem);
          }
        }
      }

      const d = new Dialog({
        title: mod.name,
        content: newList.outerHTML,
        buttons: {
          one: {
            icon: '<i class="fas fa-check"></i>',
            label: 'OK',
            callback: async () => {
              const { flags } = this.actor.data;

              let subitems;
              const checkedBoxes = html.find('.dialog-check');

              for (let i = 0; i < checkedBoxes.length; i += 1) {
                if (checkedBoxes[i].checked) {
                  const citemId = checkedBoxes[i].getAttribute('itemid');
                  const citemIkey = checkedBoxes[i].getAttribute('cikey');
                  acitem.selected = true;
                  // let selcitem = game.items.get(citemId);
                  const selcitem = await auxMeth.getcItem(citemId, citemIkey);
                  subitems = await this.actor.addcItem(selcitem, selectcitems.id);
                }
              }

              if (subitems) {
                await this.updateSubItems(false, subitems);
              }
            },
          },
          two: {
            icon: '<i class="fas fa-times"></i>',
            label: 'Cancel',
            callback: () => {
              console.log('canceling selection');
            },
          },
        },
        default: 'one',
        close: () => console.log('cItem selection dialog was shown to player.'),
        citemdialog: true,
      });
      d.render(true);
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) {
      return;
    }

    // Drop Event TEST
    this.form.ondrop = (evt) => this._onDrop(evt);

    const stabs = duplicate(actor.data.data.tabs);
    const { citems } = actor.data.data;
    const { istemplate } = actor.data.data;

    // Edit Tab item
    html.find('.item-edit').click(async (evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const tab = stabs[li.data('itemId')];
      // const item = game.items.get(tab.id);
      const item = await auxMeth.getTElement(tab.id, 'sheettab', tab.ikey);
      item.sheet.render(true);
    });

    // Delete tab Item
    html.find('.item-delete').click((evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const todelete = li.data('itemId');
      const prop = stabs.splice(todelete, 1);

      this.actor.update({ 'data.tabs': stabs });
      li.slideUp(200, () => this.render(false));
    });

    // Edit citem
    html.find('.citem-edit').click(async (evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const tab = citems[li.data('itemId')];
      // const item = game.items.get(tab.id);
      const item = await auxMeth.getcItem(tab.id, tab.ciKey);
      item.sheet.render(true);
    });

    // Delete cItem
    html.find('.citem-delete').click((evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const itemid = evt.target.parentElement.getAttribute('citemid');
      this.deleteCItem(itemid);
      li.slideUp(200, () => this.render(false));
    });

    // Top Item
    html.find('.item-top').click((evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const itemindex = li.data('itemId');
      if (itemindex > 0) {
        stabs.splice(itemindex - 1, 0, stabs.splice(itemindex, 1)[0]);
      }
      this.updateSubItems(true, stabs);
    });

    // Bottom Item
    html.find('.item-bottom').click((evt) => {
      const li = $(evt.currentTarget).parents('.property');
      const itemindex = li.data('itemId');
      if (itemindex < stabs.length - 1) {
        stabs.splice(itemindex + 1, 0, stabs.splice(itemindex, 1)[0]);
      }
      this.updateSubItems(true, stabs);
    });

    // Rebuild Sheet
    html.find('.item-refresh').click((evt) => {
      this.buildSheet();
    });

    // Change sheet and set attribute ids
    html.find('.selectsheet').change((evt) => {
      evt.preventDefault();
      const li = $(evt.currentTarget);

      const actorData = duplicate(this.actor.data);
      this.setTemplate(li[0].value, actorData);

      // this.refreshSheet(li[0].value);
      // this.actor.update({"data.gtemplate": li[0].value});
    });

    html.find('.sheet-reload').click((evt) => {
      evt.preventDefault();
      this.setTemplate(this.actor.data.data.gtemplate, null);
    });
  }

  async generateRollDialog(dialogID, dialogName, rollexp, rollname, rollid, actorattributes, citemattributes, number, rollcitemID, targets, useData) {
    // let dialogPanel = await game.items.get(dialogID);
    const dialogPanel = await auxMeth.getTElement(dialogID, 'panel', dialogName);

    if (dialogPanel === null || dialogPanel === undefined) {
      console.log(`${dialogName} not found by ID`);
      ui.notifications.warn(`Please readd dialog panel to roll ${rollname}`);
    }

    let finalContent = '';

    if (dialogPanel.data.type === 'multipanel') {
      let multiClass;

      let multiClassName = 'col-1-2';

      if (dialogPanel.data.data.width === '1') {
        multiClassName = 'multi-col-1-1';
      } else if (dialogPanel.data.data.width === '1/3') {
        multiClassName = 'multi-col-1-3';
      } else if (dialogPanel.data.data.width === '2/3') {
        multiClassName = 'multi-col-2-3';
      } else if (dialogPanel.data.data.width === '3/4') {
        multiClassName = 'multi-col-3-4';
      } else if (dialogPanel.data.data.width === '5/6') {
        multiClassName = 'multi-col-5-6';
      } else if (dialogPanel.data.data.width === '1/2') {
        multiClassName = 'multi-col-1-2';
      } else if (dialogPanel.data.data.width === '1/4') {
        multiClassName = 'multi-col-1-4';
      } else if (dialogPanel.data.data.width === '1/6') {
        multiClassName = 'multi-col-1-6';
      } else if (dialogPanel.data.data.width === '1/8') {
        multiClassName = 'multi-col-1-8';
      } else if (dialogPanel.data.data.width === '3/10') {
        multiClassName = 'multi-col-3-10';
      } else if (dialogPanel.data.data.width === '1/16') {
        multiClassName = 'multi-col-1-16';
      } else if (dialogPanel.data.data.width === '5/8') {
        multiClassName = 'multi-col-5-8';
      } else if (dialogPanel.data.data.width === '3/8') {
        multiClassName = 'multi-col-3-8';
      } else {
        multiClassName = 'multi-col-1-1';
      }

      const multiWrapper = `
<div class="${multiClassName} multiwrapper">
`;
      const wrapperEnd = `
</div>`;

      finalContent += multiWrapper;

      for (let i = 0; i < dialogPanel.data.data.panels.length; i += 1) {
        const myp = dialogPanel.data.data.panels[i];
        // let getPanel = game.items.get(myp.id);
        const getPanel = await auxMeth.getTElement(myp.id, 'panel', myp.ikey);

        finalContent += await this.generateDialogPanelHTML(getPanel);
      }

      finalContent += wrapperEnd;
    } else {
      finalContent = await this.generateDialogPanelHTML(dialogPanel);
    }

    const d = new Dialog({
      title: dialogPanel.data.data.title,
      content: finalContent,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: 'OK',
          callback: async (html) => {
            const dialogvalues = html[0].getElementsByClassName('rdialogInput');
            const dialogProps = {};

            for (let k = 0; k < dialogvalues.length; k += 1) {
              const myKey = dialogvalues[k].getAttribute('attKey');
              setProperty(dialogProps, myKey, {});
              if (dialogvalues[k].type === 'checkbox') {
                dialogProps[myKey].value = dialogvalues[k].checked;
              } else {
                dialogProps[myKey].value = dialogvalues[k].value;
              }
            }
            // console.log(dialogProps);
            this.rollExpression(rollexp, rollname, rollid, actorattributes, citemattributes, number, rollcitemID, targets, dialogProps, useData);
          },
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: 'Cancel',
          callback: () => {
            console.log('canceling selection');
          },
        },
      },
      default: 'one',
      rollDialog: true,
      actorattributes,
      citemattributes,
      number,
      close: () => console.log('cItem selection dialog was shown to player.'),
    }, { width: null });
    d.render(true);
  }

  async generateDialogPanelHTML(dialogPanel) {
    let divclassName = 'col-1-2';

    if (dialogPanel.data.data.width === '1') {
      divclassName = 'col-1-1';
    } else if (dialogPanel.data.data.width === '1/3') {
      divclassName = 'col-1-3';
    } else if (dialogPanel.data.data.width === '2/3') {
      divclassName = 'col-2-3';
    } else if (dialogPanel.data.data.width === '3/4') {
      divclassName = 'col-3-4';
    } else if (dialogPanel.data.data.width === '5/6') {
      divclassName = 'col-5-6';
    } else if (dialogPanel.data.data.width === '1/2') {
      divclassName = 'col-1-2';
    } else if (dialogPanel.data.data.width === '1/4') {
      divclassName = 'col-1-4';
    } else if (dialogPanel.data.data.width === '1/6') {
      divclassName = 'col-1-6';
    } else if (dialogPanel.data.data.width === '1/8') {
      divclassName = 'col-1-8';
    } else if (dialogPanel.data.data.width === '3/10') {
      divclassName = 'col-3-10';
    } else if (dialogPanel.data.data.width === '1/16') {
      divclassName = 'col-1-16';
    } else if (dialogPanel.data.data.width === '5/8') {
      divclassName = 'col-5-8';
    } else if (dialogPanel.data.data.width === '3/8') {
      divclassName = 'col-3-8';
    } else {
      divclassName = 'col-1-1';
    }
    let alignment = '';
    if (dialogPanel.data.data.contentalign === 'center') {
      alignment = 'centercontent';
    } else if (dialogPanel.data.data.contentalign === 'right') {
      alignment = 'righcontent';
    }

    let textalignment = '';
    if (dialogPanel.data.data.alignment === 'center') {
      textalignment = 'centertext';
    } else if (dialogPanel.data.data.alignment === 'right') {
      textalignment = 'rightext';
    } else {
      textalignment = 'lefttext';
    }

    let finalContent = `
<div class="${divclassName} ${dialogPanel.data.data.panelKey}">
`;
    const endDiv = `
</div>

`;

    if (dialogPanel.data.data.title !== '') {
      finalContent += `
            <div class="panelheader ${dialogPanel.data.data.headergroup}">
${dialogPanel.data.data.title}
            </div>
            `;
    }

    const maxcolumns = dialogPanel.data.data.columns;
    let currentCol = 0;
    for (let i = 0; i < parseInt(dialogPanel.data.data.properties.length, 10); i += 1) {
      const panelPropertyRef = dialogPanel.data.data.properties[i];
      // let panelProperty = game.items.get(panelPropertyRef.id);
      const panelProperty = await auxMeth.getTElement(panelPropertyRef.id, 'property', panelPropertyRef.ikey);

      if (currentCol === 0) {
        // Create first Row
        finalContent += `<div class="new-row  ${alignment}">`;
      }

      let labelwidth = '';
      let inputwidth = '';

      if (panelProperty.data.data.labelsize === 'F') {
        labelwidth = ' label-free';
      } else if (panelProperty.data.data.labelsize === 'S') {
        labelwidth = ' label-small';
      } else if (panelProperty.data.data.labelsize === 'T') {
        labelwidth = ' label-tiny';
      } else if (panelProperty.data.data.labelsize === 'M') {
        labelwidth = ' label-med';
      } else if (panelProperty.data.data.labelsize === 'L') {
        labelwidth = ' label-medlarge';
      }

      if (panelProperty.data.data.inputsize === 'F') {
        inputwidth = 'input-free';
      } else if (panelProperty.data.data.inputsize === 'S') {
        inputwidth = 'input-small';
      } else if (panelProperty.data.data.inputsize === 'M') {
        inputwidth = 'input-med';
      } else if (panelProperty.data.data.inputsize === 'L') {
        inputwidth = 'input-large';
      } else if (panelProperty.data.data.inputsize === 'T') {
        inputwidth = 'input-tiny';
      }

      let defvalue = '';
      if (panelProperty.data.data.defvalue !== '') {
        defvalue = 'defvalue';
      }

      if (panelProperty.data.data.datatype !== 'table' && panelProperty.data.data.datatype !== 'textarea' && panelProperty.data.data.datatype !== 'badge' && !panelProperty.data.data.ishidden) {
        if (panelProperty.data.data.haslabel) {
          finalContent += `<label class="${labelwidth} ${textalignment} ${panelProperty.data.data.fontgroup} " title="${panelProperty.data.data.tooltip}">${panelProperty.data.data.tag}</label>`;
        }

        if (panelProperty.data.data.datatype === 'checkbox') {
          finalContent += `<input class="rdialogInput checkbox check-${panelProperty.data.data.attKey} ${panelProperty.data.data.inputgroup} ${defvalue}" title="${panelProperty.data.data.tooltip}" checkGroup ="${panelProperty.data.data.checkgroup}" attKey ="${panelProperty.data.data.attKey}" type="checkbox">`;
        } else if (panelProperty.data.data.datatype === 'list') {
          finalContent += `<select  class="rdialogInput select-${panelProperty.data.data.attKey} ${panelProperty.data.data.inputgroup} ${defvalue}" title="${panelProperty.data.data.tooltip}" attKey ="${panelProperty.data.data.attKey}"  data-type="String">`;
          const options = panelProperty.data.data.listoptions.split(',');
          for (let j = 0; j < options.length; j += 1) {
            finalContent += `<option  value="${options[j]}">${options[j]}</option>`;
          }
          finalContent += '</select>';
        } else if (panelProperty.data.data.datatype === 'label') {
          // Empty
        } else {
          let isauto = '';
          let arrows = '';
          if (panelProperty.data.data.auto !== '') {
            isauto = 'isauto';
          }

          if (panelProperty.data.data.arrows) {
            arrows = 'hasarrows';
          }

          finalContent += `<input class="rdialogInput ${inputwidth} ${panelProperty.data.data.inputgroup} ${isauto} ${defvalue} ${arrows}" attKey ="${panelProperty.data.data.attKey}" type="text" value="${panelProperty.data.data.defvalue}">`;
        }

        currentCol += 1;
      }

      if (currentCol === maxcolumns || i === parseInt(dialogPanel.data.data.properties.length - 1, 10)) {
        finalContent += endDiv;
        currentCol = 0;
      }
    }

    finalContent += endDiv;

    return finalContent;
  }

  async _onRollCheck(attrID, attKey, citemID, citemKey = null, ciRoll = false, isFree = false, tableKey = null, useData = null) {
    const actorattributes = this.actor.data.data.attributes;

    let citemattributes;
    let rollexp;
    let rollname;
    const rollid = [];
    let hasDialog = false;
    let dialogID;
    let dialogName;
    let citem;
    let property;
    const initiative = false;

    let findcitem;
    let number;
    let rollcitemID;

    if (citemID !== null) {
      if (!isFree) {
        // citem = await game.items.get(citemID);
        citem = await await auxMeth.getcItem(citemID, citemKey);
        findcitem = this.actor.data.data.citems.find((y) => y.id === citemID);

        if (findcitem !== null) {
          citemattributes = findcitem.attributes;
        }

        if (citem !== null) {
          rollcitemID = citemID;
        }
      } else if (tableKey !== null) {
        const tableItems = actorattributes[tableKey].tableitems;
        const myFreeItem = tableItems.find((y) => y.id === citemID);
        citemattributes = myFreeItem.attributes;
      }
    }

    if (!ciRoll) {
      property = await auxMeth.getTElement(attrID, 'property', attKey);
      ({ rollexp } = property.data.data);
      ({ rollname } = property.data.data);
      ({ hasDialog } = property.data.data);
      ({ dialogID } = property.data.data);
      ({ dialogName } = property.data.data);

      rollid.push(property.data.data.rollid);
    } else {
      rollexp = citem.data.data.roll;
      ({ rollname } = citem.data.data);
      ({ hasDialog } = citem.data.data);
      ({ dialogID } = citem.data.data);
      ({ dialogName } = citem.data.data);

      rollid.push(citem.data.data.rollid);
    }

    const targets = game.user.targets.ids;

    if (findcitem !== null) {
      ({ number } = findcitem);
    }

    if (hasDialog) {
      this.generateRollDialog(dialogID, dialogName, rollexp, rollname, rollid, actorattributes, citemattributes, number, rollcitemID, targets, useData);
    } else {
      this.rollExpression(rollexp, rollname, rollid, actorattributes, citemattributes, number, rollcitemID, targets, null, useData);
    }

    // return finalroll;
  }

  async rollExpression(rollexp, rollname, rollid, actorattributes, citemattributes, number, rollcitemID, targets, dialogProps = null, useData = null) {
    rollexp = await auxMeth.parseDialogProps(rollexp, dialogProps);

    rollname = await auxMeth.parseDialogProps(rollname, dialogProps);

    let tokenid;

    // console.log(rollexp);

    let finalroll;

    if (targets.length > 0 && ((rollexp.includes('#{target|') || rollexp.includes('add(')) || rollexp.includes('set('))) {
      for (let i = 0; i < targets.length; i += 1) {
        tokenid = canvas.tokens.placeables.find((y) => y.id === targets[i]);
        finalroll = await this.actor.rollSheetDice(rollexp, rollname, rollid, actorattributes, citemattributes, number, tokenid, rollcitemID);
      }
    } else {
      if (this.actor.isToken && this.token !== null) {
        tokenid = this.token.id;
      }
      finalroll = await this.actor.rollSheetDice(rollexp, rollname, rollid, actorattributes, citemattributes, number, null, rollcitemID, tokenid);
    }

    if (useData !== null) {
      await this.activateCI(useData.id, useData.value, useData.iscon, finalroll);
    }
  }

  // Creates the attributes the first time a template is chosen for a character
  async refreshSheet(gtemplate) {
    // Gets all game properties

    // console.log(gtemplate);

    // Finds master property
    await this.actor.update({ 'data.gtemplate': gtemplate });

    // await this.actor.update(this.actor.data);
    // await this.actor.actorUpdater();
  }

  async setTemplate(gtemplate, actorData) {
    console.log('setting sheet');
    // console.log(actorData);

    const propitems = game.items.filter((y) => y.data.type === 'property');

    if (actorData === null) {
      actorData = duplicate(this.actor.data);
    }

    const attData = actorData.data.attributes;
    if (gtemplate === '' || gtemplate === null) {
      gtemplate = 'Default';
    }
    actorData.data.gtemplate = gtemplate;

    // Looks for template and finds inputs

    const parser = new DOMParser();
    // let htmlcode = await fetch(this.getHTMLPath()).then(resp => resp.text());

    const htmlcode = await auxMeth.getTempHTML(gtemplate);
    actorData.data._html = htmlcode;
    // console.log(htmlcode);
    const form = await parser.parseFromString(htmlcode, 'text/html').querySelector('form');
    // console.log(form);
    // Loops the inputs and creates the related attributes

    if (form === null) {
      ui.notifications.warn('Please rebuild character sheet before assigning');
    }

    const inputs = await form.querySelectorAll('input,select,textarea');
    for (let i = 0; i < inputs.length; i += 1) {
      const newAtt = inputs[i];

      const attId = newAtt.getAttribute('attId');
      // console.log(newAtt);
      let attKey = newAtt.getAttribute('name');
      attKey = attKey.replace('data.attributes.', '');
      attKey = attKey.replace('.value', '');
      if (attId !== null) {
        await this.setAttributeValues(attId, attData, attKey);
      }
    }

    // For special case of radioinputs
    const radioinputs = form.getElementsByClassName('radio-input');
    for (let i = 0; i < radioinputs.length; i += 1) {
      const newAtt = radioinputs[i];
      const attId = newAtt.getAttribute('attId');
      let attKey = newAtt.getAttribute('name');
      attKey = attKey.replace('data.attributes.', '');
      attKey = attKey.replace('.value', '');
      await this.setAttributeValues(attId, attData, attKey);
    }

    // For special cases of badges
    const badgeinputs = form.getElementsByClassName('badge-click');
    for (let i = 0; i < badgeinputs.length; i += 1) {
      const newAtt = badgeinputs[i];
      const attId = newAtt.getAttribute('attId');
      const attKey = newAtt.getAttribute('attKey');
      await this.setAttributeValues(attId, attData, attKey);
    }

    // For special cases of tables
    const tableinputs = form.getElementsByClassName('sbtable');
    for (let i = 0; i < tableinputs.length; i += 1) {
      const newAtt = tableinputs[i];
      const attId = newAtt.getAttribute('attId');
      let attKey = newAtt.getAttribute('name');
      attKey = attKey.replace('data.attributes.', '');
      attKey = attKey.replace('.value', '');
      await this.setAttributeValues(attId, attData, attKey);
    }

    // For special cases of custom checboxes
    const custominputs = form.getElementsByClassName('customcheck');
    for (let i = 0; i < custominputs.length; i += 1) {
      const newAtt = custominputs[i];
      const attId = newAtt.getAttribute('attId');
      let attKey = newAtt.getAttribute('name');
      if (attKey !== null) {
        attKey = attKey.replace('data.attributes.', '');
        attKey = attKey.replace('.value', '');
      } else {
        attKey = newAtt.getAttribute('attkey');
      }

      await this.setAttributeValues(attId, attData, attKey);
    }

    // Get token settings
    // Set token mode
    const tokenbar = form.getElementsByClassName('token-bar1');
    const bar1Att = tokenbar[0].getAttribute('tkvalue');

    const tokenname = form.getElementsByClassName('token-displayName');
    const displayName = tokenname[0].getAttribute('tkvalue');

    const tokenshield = form.getElementsByClassName('token-shieldstat');
    const shield = tokenshield[0].getAttribute('tkvalue');

    const biofield = form.getElementsByClassName('check-biovisible');
    let biovisible = biofield[0].getAttribute('biovisible');

    const resizefield = form.getElementsByClassName('check-resizable');
    let resizable = biofield[0].getAttribute('resizable');

    const visifield = form.getElementsByClassName('token-visitabs');
    const visitabs = visifield[0].getAttribute('visitabs');

    actorData.data.displayName = CONST.TOKEN_DISPLAY_MODES[displayName];
    actorData.data.tokenbar1 = `attributes.${bar1Att}`;
    actorData.data.tokenshield = shield;

    if (biovisible === 'false') {
      biovisible = false;
    }

    if (biovisible === 'true') {
      biovisible = true;
    }

    if (resizable === 'false') {
      resizable = false;
    }

    if (resizable === 'true') {
      resizable = true;
    }

    actorData.data.biovisible = biovisible;
    actorData.data.resizable = resizable;
    actorData.data.visitabs = parseInt(visitabs, 10);

    const mytoken = await this.setTokenOptions(actorData);

    await this.actor.update({ data: actorData.data, token: mytoken }, { diff: false });
    await this.actor.update({ data: actorData.data, token: mytoken });
  }

  async setAttributeValues(attID, attData, propName) {
    // reference to attribute
    // const attData = this.actor.data.data.attributes;
    // const property = await game.items.get(attID);
    const property = await auxMeth.getTElement(attID, 'property', propName);

    const attribute = property.data.data.attKey;
    const idkey = attData[attribute];

    let populate = false;
    if (idkey === null) {
      populate = true;
    } else {
      if (idkey.id === null) {
        populate = true;
      }

      //            else{
      //                if(idkey.value==null)
      //                    populate = true;
      //            }

      if (property.data.data.datatype === 'radio' && (idkey.max === null || idkey.max === '' || idkey.value === '' || idkey.value === null)) {
        populate = true;
      }

      //            if(property.data.data.maxtop){
      //                console.log("setting");
      //                setProperty(attData[attribute],"maxblocked", true);
      //            }
      //            else{
      //                setProperty(attData[attribute],"maxblocked", false);
      //            }
    }

    if (property.data.data.datatype === 'table') {
      // if (!hasProperty(attData[attribute], "tableitems")) {
      populate = true;
      // }
    }

    if (property.data.data.datatype === 'checkbox') {
      if (attData[attribute] !== null) {
        setProperty(attData[attribute], 'checkgroup', property.data.data.checkgroup);
      }
    }

    if (!hasProperty(attData, attribute) || Object.keys(attData[attribute]).length === 0 || populate) {
      attData[attribute] = {};
      setProperty(attData[attribute], 'id', '');
      attData[attribute].id = attID;

      // Sets id and auto
      if (property.data.data.datatype !== 'table') {
        if (!hasProperty(attData[attribute], 'value')) {
          setProperty(attData[attribute], 'value', '');
        }

        setProperty(attData[attribute], 'prev', '');
        await setProperty(attData[attribute], 'isset', false);

        // Sets auto, auto max, and max
        if (property.data.data.automax !== '' || property.data.data.datatype === 'radio') {
          setProperty(attData[attribute], 'max', '');
        }

        if (property.data.data.datatype === 'checkbox') {
          setProperty(attData[attribute], 'checkgroup', property.data.data.checkgroup);
        }
      } else {
        // console.log("setting table " + attribute);
        const tablegroup = property.data.data.group;
        // let groupObj = await game.items.get(tablegroup.id);
        // console.log(tablegroup);
        const groupObj = await auxMeth.getTElement(tablegroup.id, 'group', tablegroup.ikey);
        if (groupObj === null) {
          ui.notifications.warn(`Please reassign group to table ${attribute}`);
          console.log(`Error:Please reassign group to table ${attribute}`);
        }

        const groupprops = groupObj.data.data.properties;
        // console.log(groupprops);
        setProperty(attData[attribute], 'istable', true);
        setProperty(attData[attribute], 'totals', {});

        if (!hasProperty(this.actor.data.data.attributes[attribute], 'tableitems')) {
          setProperty(attData[attribute], 'tableitems', []);
        }

        const attTableKey = attData[attribute];
        for (let i = 0; i < groupprops.length; i += 1) {
          const propId = groupprops[i].id;
          // let propData = game.items.get(propId);
          const propData = await auxMeth.getTElement(propId, 'property', groupprops[i].ikey);
          const propKey = propData.data.data.attKey;

          setProperty(attTableKey.totals, propKey, {});
          const tableAtt = attTableKey.totals[propKey];
          setProperty(tableAtt, 'id', propId);

          if (propData.data.data.totalize) {
            setProperty(tableAtt, 'total', '');
            setProperty(tableAtt, 'prev', '');
          }
          // TO FIX IN FUTURE
          // for(let j=0;j<this.actor.data.data.attributes[attribute].tableitems.length;j += 1){
          //     let tableItemProp = this.actor.data.data.attributes[attribute].tableitems[j].attributes;
          //     if(tableItemProp[propKey]==null){
          //         setProperty(attData[attribute], "tableitems", []);
          //         let newtableBlock = attData[attribute];
          //         let newtableItem = newtableBlock.tableitems;
          //         newtableItem = this.actor.data.data.attributes[attribute].tableitems[j];
          //         newtableItem.attributes[propKey] = {};
          //         newtableItem.attributes[propKey].value = propData.data.data.defvalue;
          //     }

          // }
        }
      }
    }

    // console.log(attData[attribute]);

    // return attData;
  }

  async checkTabsExisting() {
    // Check Tabs
    const tabs = this.actor.data.flags.tabarray;
    let changed = false;
    const { items } = game;

    if (tabs !== null) {
      for (let i = 0; i < tabs.length; i += 1) {
        if (!game.items.get(tabs[i].id)) {
          const index = tabs.indexOf(tabs[i]);
          if (index > -1) {
            tabs.splice(index, 1);
            changed = true;
          }
        }
      }
    }

    if (changed) {
      this.updateTabs();
    }
  }

  /*  -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 */

  /**
   * HTML Editors
   */

  async addNewTab(newHTML, tabitem, index) {
    console.log('adding Tabs');

    const wrapper = document.createElement('div');

    if (newHTML === null) {
      wrapper.innerHTML = this.actor.data.data._html;
    } else {
      wrapper.innerHTML = newHTML;
    }

    const deftemplate = wrapper;
    // console.log(deftemplate);

    const tabname = tabitem.data.title;
    const { tabKey } = tabitem.data;

    // Tab selector
    const p = deftemplate.querySelector('#tabs');

    const c = deftemplate.querySelector('#tab-last');

    const cindex = Array.from(p.children).indexOf(c);
    const totaltabs = parseInt(p.getAttribute('tabs'), 10);
    const newElement = document.createElement('a');
    newElement.className = 'item tab-button';
    newElement.setAttribute('id', `tab-${index}`);
    newElement.setAttribute('data-tab', tabKey);
    newElement.textContent = tabname;
    p.insertBefore(newElement, p.children[cindex]);
    p.setAttribute('tabs', totaltabs + 1);

    // ADD VISIBILITY RULES TO TAB
    if (tabitem.data.condop !== 'NON') {
      let attProp = '.value';
      if (tabitem.data.condat !== null) {
        if (tabitem.data.condat.includes('max')) {
          attProp = '';
        }
      }

      if (tabitem.data.condop === 'EQU') {
        if (tabitem.data.condvalue === 'true' || tabitem.data.condvalue === 'false' || tabitem.data.condvalue === true || tabitem.data.condvalue === false) {
          newElement.insertAdjacentHTML('beforebegin', `{{#if actor.data.attributes.${tabitem.data.condat}${attProp}}}`);
          newElement.insertAdjacentHTML('afterend', '{{/if}}');
        } else {
          newElement.insertAdjacentHTML('afterbegin', `{{#ifCond actor.data.attributes.${tabitem.data.condat}${attProp} '${tabitem.data.condvalue}'}}`);
          newElement.insertAdjacentHTML('beforeend', '{{/ifCond}}');
        }
      } else if (tabitem.data.condop === 'HIH') {
        newElement.insertAdjacentHTML('afterbegin', `{{#ifGreater actor.data.attributes.${tabitem.data.condat}${attProp} '${tabitem.data.condvalue}'}}`);
        newElement.insertAdjacentHTML('beforeend', '{{/ifGreater}}');
      } else if (tabitem.data.condop === 'LOW') {
        newElement.insertAdjacentHTML('afterbegin', `{{#ifLess actor.data.attributes.${tabitem.data.condat}${attProp} '${tabitem.data.condvalue}'}}`);
        newElement.insertAdjacentHTML('beforeend', '{{/ifLess}}');
      } else if (tabitem.data.condop === 'NOT') {
        newElement.insertAdjacentHTML('afterbegin', `{{#ifNot actor.data.attributes.${tabitem.data.condat}${attProp} '${tabitem.data.condvalue}'}}`);
        newElement.insertAdjacentHTML('beforeend', '{{/ifNot}}');
      }
    }

    if (tabitem.data.controlby === 'gamemaster') {
      newElement.insertAdjacentHTML('beforebegin', '{{#isGM}}');
      newElement.insertAdjacentHTML('afterend', '{{/isGM}}');
    } else {
      newElement.className += ' player-tab';
    }

    // Tab content
    const parentNode = deftemplate.querySelector('#sheet-body');
    const div5 = document.createElement('DIV');
    div5.className = `tab scrollable ${tabKey}_tab`;
    div5.setAttribute('id', `${tabKey}_Def`);
    div5.setAttribute('data-group', 'primary');
    div5.setAttribute('data-tab', tabKey);
    parentNode.appendChild(div5);

    const div9 = document.createElement('DIV');
    div9.className = 'new-column sbbody';
    div9.setAttribute('id', `${tabKey}Body`);
    div5.appendChild(div9);

    // Set token mode
    const tokenbar = deftemplate.getElementsByClassName('token-bar1');
    const tokenshield = deftemplate.getElementsByClassName('token-shieldstat');
    const tokenname = deftemplate.getElementsByClassName('token-displayName');

    let { displayName } = this.actor.data.data;
    console.log(displayName);
    if (displayName === null) {
      displayName = 'NONE';
    }

    tokenbar[0].setAttribute('tkvalue', this.actor.data.data.tokenbar1);
    tokenname[0].setAttribute('tkvalue', displayName);
    tokenshield[0].setAttribute('tkvalue', this.actor.data.data.shieldstat);

    const biovisiblefield = deftemplate.getElementsByClassName('check-biovisible');
    const resizablefield = deftemplate.getElementsByClassName('check-resizable');
    console.log(biovisiblefield);
    biovisiblefield[0].setAttribute('biovisible', this.actor.data.data.biovisible);
    resizablefield[0].setAttribute('resizable', this.actor.data.data.resizable);

    const visitabfield = deftemplate.getElementsByClassName('token-visitabs');
    visitabfield[0].setAttribute('visitabs', this.actor.data.data.visitabs);

    const finalreturn = new XMLSerializer().serializeToString(deftemplate);
    return finalreturn;
  }

  async addNewPanel(newHTML, tabpanel, tabKey, tabname, firstmrow, multiID = null, multiName = null, _paneldata = null, multiheadergroup = null) {
    // Variables
    console.log(`adding Panel ${tabpanel.name} in ${tabKey}`);
    console.log(tabpanel);

    //        if(tabpanel.data==null)
    //            return;

    const wrapper = document.createElement('div');
    if (newHTML === null) {
      wrapper.innerHTML = this.actor.data.data._html;
    } else {
      wrapper.innerHTML = newHTML;
    }

    // let deftemplate= wrapper;
    const deftemplate = new DOMParser().parseFromString(newHTML, 'text/html');
    const { actor } = this;
    const { flags } = this.actor.data;
    const parentNode = deftemplate.querySelector(`#${tabKey}Body`);
    // console.log(tabpanel);
    // console.log(deftemplate);

    let fontgroup = '';
    let inputgroup = '';

    if (tabpanel.data.fontgroup !== null) {
      ({ fontgroup } = tabpanel.data);
    }

    if (tabpanel.data.inputgroup !== null) {
      ({ inputgroup } = tabpanel.data);
    }

    //        let ({ fontgroup } = tabpanel.data);
    //        let ({ inputgroup } = tabpanel.data);

    let initial = false;

    if (multiID === null) {
      console.log(`INITIAL _ ${tabpanel.name} width: ${flags.rwidth} rows: ${flags.rows}`);
    } else {
      console.log(`INITIAL _ ${tabpanel.name} maxrows: ${flags.maxrows} multiwidth: ${flags.multiwidth}maxwidth: ${flags.maxwidth}`);
    }

    if (flags.rwidth >= 1) {
      if (multiID === null) {
        flags.rows += 1;
        flags.rwidth = 0;
      } else if (firstmrow) {
        flags.rwidth = 0;
        flags.rows += 1;
      }
    } else if (firstmrow && flags.rwidth === 0 && flags.rows > 1) {
      flags.rows += 1;
    }

    // if (flags.multiwidth >= flags.maxwidth) {
    //   console.log('newmultirow');
    //   flags.multiwidth === 0;
    // }

    if (flags.multiwidth === 0 && multiID !== null) {
      flags.maxrows += 1;
      initial = true;
    }

    const div6 = deftemplate.createElement('DIV');

    if (firstmrow) {
      if (flags.rwidth === 0 || flags.rwidth === 1 || (flags.multiwidth === 0 && multiID !== null)) {
        initial = true;
      }
    }

    let labelwidth;
    const { columns } = tabpanel.data;

    // Set panel width
    if (tabpanel.data.width === '1') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-1';
      }
      if (multiID === null) {
        flags.rwidth += 1;
      } else {
        flags.multiwidth += 1;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/3') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-3';
      }
      if (multiID === null) {
        flags.rwidth += 0.333;
      } else {
        flags.multiwidth += 0.333;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '2/3') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-2-3';
      }
      if (multiID === null) {
        flags.rwidth += 0.666;
      } else {
        flags.multiwidth += 0.666;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '3/4') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-3-4';
      }
      if (multiID === null) {
        flags.rwidth += 0.75;
      } else {
        flags.multiwidth += 0.75;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '5/6') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-5-6';
      }
      if (multiID === null) {
        flags.rwidth += 0.833;
      } else {
        flags.multiwidth += 0.833;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/2') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-2';
      }
      if (multiID === null) {
        flags.rwidth += 0.5;
      } else {
        flags.multiwidth += 0.5;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/4') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-4';
      }
      if (multiID === null) {
        flags.rwidth += 0.25;
      } else {
        flags.multiwidth += 0.25;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/6') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-6';
      }
      if (multiID === null) {
        flags.rwidth += 0.166;
      } else {
        flags.multiwidth += 0.166;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/8') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-8';
      }
      if (multiID === null) {
        flags.rwidth += 0.125;
      } else {
        flags.multiwidth += 0.125;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '3/10') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-3-10';
      }
      if (multiID === null) {
        flags.rwidth += 0.3;
      } else {
        flags.multiwidth += 0.3;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '1/16') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-16';
      }
      if (multiID === null) {
        flags.rwidth += 0.0625;
      } else {
        flags.multiwidth += 0.0625;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '5/8') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-5-8';
      }
      if (multiID === null) {
        flags.rwidth += 0.625;
      } else {
        flags.multiwidth += 0.625;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else if (tabpanel.data.width === '3/8') {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-3-8';
      }
      if (multiID === null) {
        flags.rwidth += 0.375;
      } else {
        flags.multiwidth += 0.375;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    } else {
      if ((firstmrow && multiID === null) || (multiID !== null)) {
        div6.className = 'col-1-1';
      }
      if (multiID === null) {
        flags.rwidth += 1;
      } else {
        flags.multiwidth += 1;
        div6.className = this.getmultiWidthClass(tabpanel.data.width);
      }
    }

    if (multiID === null) {
      console.log(`PRE _ ${tabpanel.name} width: ${flags.rwidth} rows: ${flags.rows}`);
    } else {
      console.log(`PRE _ ${tabpanel.name} maxrows: ${flags.maxrows} multiwidth: ${flags.multiwidth}maxwidth: ${flags.maxwidth}`);
    }

    div6.className += ` ${tabpanel.data.panelKey}_container`;

    if (flags.rwidth > 0.95 && flags.rwidth <= 1) {
      flags.rwidth = 1.015;
    }

    console.log(`firstmrow: ${firstmrow}`);
    if (flags.rwidth > 1.015) {
      // flags.rwidth -= 1;
      // if (flags.rwidth < 0.1)
      flags.rwidth = 0;
      if (firstmrow && multiID === null) {
        flags.rows += 1;
        initial = true;
      }
    }

    console.log(`rows: ${flags.rows}`);

    if (flags.multiwidth > 0.95 && flags.multiwidth <= 1) {
      flags.multiwidth = 1;
    }

    if (multiID !== null) {
      if (flags.multiwidth > flags.maxwidth) {
        flags.multiwidth -= flags.maxwidth;
        if (flags.multiwidth < 0.1) {
          flags.multiwidth = 0;
        }
        flags.maxrows += 1;
        initial = true;
      }
    }

    if (multiID === null) {
      console.log(`POST _ ${tabpanel.name} width: ${flags.rwidth} rows: ${flags.rows} initial:${initial}`);
    } else {
      console.log(`POST _ ${tabpanel.name} maxrows: ${flags.maxrows} multiwidth: ${flags.multiwidth}maxwidth: ${flags.maxwidth}`);
    }

    if (initial) {
      console.log('creating row initial true');
    } else {
      console.log('getting multirow');
    }

    // console.log(tabpanel.name + "post  width: " +flags.rwidth + " rows:" + flags.rows);

    if (tabpanel.data.title !== '') {
      const newHeader = deftemplate.createElement('DIV');

      if (tabpanel.data.backg === 'T') {
        newHeader.className = 'panelheader-t';
      } else {
        newHeader.className = 'panelheader';
      }

      if (tabpanel.data.headergroup !== '') {
        newHeader.className += ` ${tabpanel.data.headergroup}`;
      }

      newHeader.textContent = tabpanel.data.title;
      div6.appendChild(newHeader);
    }

    const { properties } = tabpanel.data;

    let count = 0;
    let divtemp;
    const newRow = deftemplate.createElement('DIV');

    // LOAD THE PROPERTIES INPUT FIELDS
    // await properties.forEach(function (rawproperty) {
    for (let n = 0; n < properties.length; n += 1) {
      const rawproperty = properties[n];

      // label alignment
      if (tabpanel.data.alignment === 'right') {
        labelwidth = 'righttext';
      } else if (tabpanel.data.alignment === 'center') {
        labelwidth = 'centertext';
      } else {
        labelwidth = '';
      }

      console.log(rawproperty);
      // let propertybase = game.items.get(rawproperty.id);
      const propertybase = await auxMeth.getTElement(rawproperty.id, 'property', rawproperty.ikey);

      if (propertybase === null) {
        ui.notifications.warn(`The property ${rawproperty.name} in panel ${tabpanel.name} does not exist anymore. Please remove the reference to it`);
        throw new Error('No property!');
      }

      const property = propertybase.data;

      if (property.data.attKey === null || property.data.attKey === '') {
        ui.notifications.warn(`The property ${rawproperty.name} in panel ${tabpanel.name} does not have a key`);
        throw new Error('No property Key!');
      }

      ({ fontgroup } = tabpanel.data);
      ({ inputgroup } = tabpanel.data);

      if (property.data.fontgroup !== '') {
        ({ fontgroup } = property.data);
      }

      if (property.data.inputgroup !== '') {
        ({ inputgroup } = property.data);
      }

      if (fontgroup === null) {
        ({ fontgroup } = tabpanel.data);
      }
      if (inputgroup === null) {
        ({ inputgroup } = tabpanel.data);
      }

      if (count === 0) {
        newRow.className = 'new-row ';
        newRow.className += tabpanel.data.panelKey;
        divtemp = deftemplate.createElement('DIV');

        if (tabpanel.data.contentalign === 'center') {
          divtemp.className = `flexblock-center ${tabpanel.data.panelKey}_row`;
        } else if (tabpanel.data.contentalign === 'right') {
          divtemp.className = `flexblock-right ${tabpanel.data.panelKey}_row`;
        } else {
          divtemp.className = `flexblock-left ${tabpanel.data.panelKey}_row`;
        }

        div6.appendChild(newRow);
        newRow.appendChild(divtemp);
      }

      // Attribute input
      let sInput;
      let sInputMax;
      let sInputArrows;

      // Set Label
      if (property.data.haslabel && property.data.datatype !== 'table' && property.data.datatype !== 'badge' && property.data.datatype !== 'button') {
        // Attribute label
        const sLabel = deftemplate.createElement('H3');

        if (property.data.labelsize === 'F') {
          labelwidth += ' label-free';
        } else if (property.data.labelsize === 'S') {
          labelwidth += ' label-small';
        } else if (property.data.labelsize === 'T') {
          labelwidth += ' label-tiny';
        } else if (property.data.labelsize === 'M') {
          labelwidth += ' label-med';
        } else if (property.data.labelsize === 'L') {
          labelwidth += ' label-medlarge';
        }

        sLabel.className = `${labelwidth} ${property.data.attKey}_label`;
        sLabel.textContent = property.data.tag;

        if (property.data.tooltip !== null) {
          if (property.data.tooltip !== '') {
            if (property.data.tooltip.length > 0) {
              sLabel.title = property.data.tooltip;
            }
          }
        }

        divtemp.appendChild(sLabel);

        // Adds identifier
        sLabel.setAttribute('id', property.data.attKey);
        sLabel.setAttribute('attid', rawproperty.id);

        if (property.data.labelformat === 'B') {
          sLabel.className += ' boldtext';
        } else if (property.data.labelformat === 'D') {
          sLabel.textContent = '';

          const dieContainer = deftemplate.createElement('DIV');
          dieContainer.setAttribute('title', property.data.tag);

          const dieSymbol = deftemplate.createElement('i');
          dieSymbol.className = 'fas fa-dice-d20';
          dieContainer.appendChild(dieSymbol);

          sLabel.appendChild(dieContainer);
        } else if (property.data.labelformat === 'S') {
          sLabel.className += ' smalltext';
        }

        // Sets class required for rolling
        if (property.data.hasroll) {
          sLabel.className += ' rollable';
        }

        if (fontgroup !== '') {
          sLabel.className += ` ${fontgroup}`;
        }

        console.log(`${sLabel.className} ${sLabel.textContent}`);
      }

      // Check property type
      if (property.data.datatype === 'checkbox') {
        if (!property.data.customcheck && (property.data.onPath === '' || property.data.offPath === '')) {
          sInput = deftemplate.createElement('INPUT');
          sInput.className = 'input-small';
          if (property.data.labelsize === 'T') {
            sInput.className = 'input-tiny';
          }
          sInput.setAttribute('name', `data.attributes.${property.data.attKey}.value`);
          sInput.setAttribute('type', 'checkbox');
          sInput.setAttribute('toparse', `{{checked actor.data.attributes.${property.data.attKey}.value}}~~`);
        } else {
          sInput = deftemplate.createElement('DIV');
          sInput.className = 'input-small';
          if (property.data.inputsize === 'T') {
            sInput.className = 'input-tiny';
          }
          sInput.setAttribute('attKey', property.data.attKey);
          sInput.setAttribute('onPath', property.data.onPath);
          sInput.setAttribute('offPath', property.data.offPath);
          sInput.className += ' customcheck';
        }
      } else if (property.data.datatype === 'radio') {
        // Check property type
        sInput = deftemplate.createElement('DIV');
        sInput.className = 'radio-input';
        sInput.setAttribute('name', property.data.attKey);
      } else if (property.data.datatype === 'textarea') {
        sInput = deftemplate.createElement('TEXTAREA');
        if (property.data.inputsize === 'S') {
          sInput.className = 'texteditor-small';
        } else if (property.data.inputsize === 'L') {
          sInput.className = 'texteditor-large';
        } else {
          sInput.className = 'texteditor-med';
        }

        sInput.setAttribute('name', `data.attributes.${property.data.attKey}.value`);
        sInput.textContent = `{{data.data.attributes.${property.data.attKey}.value}}`;
      } else if (property.data.datatype === 'badge') {
        sInput = deftemplate.createElement('DIV');
        sInput.className = 'badge-block centertext';
        sInput.setAttribute('name', property.data.attKey);

        const badgelabel = deftemplate.createElement('LABEL');
        badgelabel.className = 'badgelabel';
        badgelabel.textContent = property.data.tag;

        if (property.data.tooltip !== null) {
          if (property.data.tooltip !== '') {
            if (property.data.tooltip.length > 0) {
              badgelabel.title = property.data.tooltip;
            }
          }
        }

        sInput.appendChild(badgelabel);

        const extraDiv = deftemplate.createElement('DIV');
        extraDiv.className = 'badge-container';

        const badgea = deftemplate.createElement('a');
        badgea.className = 'badge-image';
        badgea.className += ` badge-${property.data.attKey}`;

        const badgei = deftemplate.createElement('i');
        badgei.className = 'badge-click';
        badgei.setAttribute('attKey', property.data.attKey);
        badgei.setAttribute('attId', property._id);
        badgea.appendChild(badgei);

        extraDiv.appendChild(badgea);

        if (game.user.isGM) {
          const gmbadgea = deftemplate.createElement('a');
          gmbadgea.setAttribute('attKey', property.data.attKey);
          gmbadgea.setAttribute('attId', property._id);
          gmbadgea.className = 'badge-clickgm';

          const gmbadgei = deftemplate.createElement('i');
          gmbadgei.className = 'fas fa-plus-circle';

          gmbadgea.appendChild(gmbadgei);
          extraDiv.appendChild(gmbadgea);
        }

        sInput.appendChild(extraDiv);
      } else if (property.data.datatype === 'list') {
        sInput = deftemplate.createElement('SELECT');
        if (property.data.inputsize === 'F') {
          sInput.className = 'input-free';
        } else if (property.data.labelsize === 'S') {
          sInput.className = 'input-small';
        } else if (property.data.labelsize === 'T') {
          sInput.className = 'input-tiny';
        } else if (property.data.labelsize === 'M') {
          sInput.className = 'input-med';
        } else if (property.data.labelsize === 'L') {
          sInput.className = 'input-medlarge';
        } else {
          sInput.className = 'input-med';
        }

        // sInput.className = "input-med";
        sInput.setAttribute('name', `data.attributes.${property.data.attKey}.value`);
        sInput.insertAdjacentHTML('beforeend', `{{#select data.data.attributes.${property.data.attKey}.value}}`);

        // IM ON IT
        const rawlist = property.data.listoptions;
        const listobjects = rawlist.split(',');

        for (let i = 0; i < listobjects.length; i += 1) {
          const nOption = deftemplate.createElement('OPTION');
          nOption.setAttribute('value', listobjects[i]);
          nOption.textContent = listobjects[i];
          sInput.appendChild(nOption);
        }

        sInput.insertAdjacentHTML('beforeend', '{{/select}}');
      } else if (property.data.datatype === 'button') {
        sInput = deftemplate.createElement('a');
        sInput.className = 'sbbutton';
        const buttonContent = deftemplate.createElement('i');
        buttonContent.className = `${property.data.attKey}_button macrobutton`;
        buttonContent.textContent = property.data.tag;
        buttonContent.setAttribute('macroid', property.data.macroid);
        sInput.appendChild(buttonContent);
      } else if (property.data.datatype === 'table') {
        newRow.className = `table-row ${property.data.attKey}_row`;

        // TABLE LAYOUT
        sInput = deftemplate.createElement('TABLE');
        if (property.data.tableheight === 'S') {
          sInput.className = 'table-small';
        } else if (property.data.tableheight === 'M') {
          sInput.className = 'table-med';
        } else if (property.data.tableheight === 'T') {
          sInput.className = 'table-tall';
        } else {
          sInput.className = 'table-free';
        }

        sInput.className += ' sbtable';

        sInput.setAttribute('name', `data.attributes.${property.data.attKey}`);
        sInput.setAttribute('inputgroup', inputgroup);
        sInput.setAttribute('value', `{{data.data.attributes.${property.data.attKey}.value}}`);

        sInput.innerHTML = '';

        // get group
        // const group = game.items.get(property.data.group.id);
        const group = await auxMeth.getTElement(property.data.group.id, 'group', property.data.group.ikey);

        // Create header
        const header = deftemplate.createElement('THEAD');
        if (!property.data.hasheader) {
          header.style.display = 'none';
        }
        sInput.appendChild(header);
        const headerRow = deftemplate.createElement('TR');
        headerRow.className += ` ${fontgroup}`;
        headerRow.setAttribute('tableKey', property.data.attKey);
        header.appendChild(headerRow);

        // Add name ta
        if ((property.data.onlynames === 'DEFAULT' || property.data.onlynames === 'ONLY_NAMES') && !property.data.isfreetable) {
          if (!property.data.namecolumn) {
            property.data.namecolumn = 'Item';
          }

          const hnameCell = deftemplate.createElement('TH');
          // hnameCell.className = "input-free";
          hnameCell.className = 'label-large';
          hnameCell.textContent = property.data.namecolumn;
          hnameCell.className += ' tableheader nameheader';
          headerRow.appendChild(hnameCell);
        }

        if (property.data.onlynames !== 'ONLY_NAMES') {
          if (property.data.hasactivation && !property.data.isfreetable) {
            const hactiveCell = deftemplate.createElement('TH');
            hactiveCell.className = 'input-min';
            hactiveCell.className += ' tableheader';
            hactiveCell.textContent = 'Active';
            headerRow.appendChild(hactiveCell);
          }

          if (property.data.hasunits && !property.data.isfreetable) {
            const hnumberCell = deftemplate.createElement('TH');
            hnumberCell.className = 'input-min';
            hnumberCell.className += ' tableheader';
            hnumberCell.textContent = 'Num';
            headerRow.appendChild(hnumberCell);
          }

          // REMOVE USES WORKSTREAM
          if (property.data.hasuses && property.data.hasactivation && !property.data.isfreetable) {
            const husesCell = deftemplate.createElement('TH');
            husesCell.className = 'input-uses';
            husesCell.className += ' tableheader';
            husesCell.textContent = 'Uses';
            headerRow.appendChild(husesCell);
          }

          if (group !== null) {
            const groupprops = group.data.data.properties;
            // let isfirstFree = true;
            for (let i = 0; i < groupprops.length; i += 1) {
              console.log(`${groupprops[i].id} key:${groupprops[i].ikey} name:${groupprops[i].name}`);

              // let propTable = game.items.get(groupprops[i].id);
              const propTable = await auxMeth.getTElement(groupprops[i].id, 'property', groupprops[i].ikey);
              const hCell = deftemplate.createElement('TH');

              hCell.className = 'input-med';

              if (propTable.data.data.labelsize === 'F') {
                hCell.className = 'label-free';
              } else if (propTable.data.data.labelsize === 'S') {
                hCell.className = 'label-small';
              } else if (propTable.data.data.labelsize === 'T') {
                hCell.className = 'label-tiny';
              } else if (propTable.data.data.labelsize === 'L' && propTable.data.data.inputsize === 'M') {
                hCell.className = 'label-medlarge';
              } else if (propTable.data.data.labelsize === 'L' && propTable.data.data.inputsize === 'L') {
                hCell.className = 'label-big';
              } else if (propTable.data.data.labelsize === 'L') {
                hCell.className = 'label-large';
              } else {
                hCell.className = 'label-med';
              }

              // if(property.data.isfreetable && isfirstFree){
              //     hCell.className += " firstcol";
              //     isfirstFree = false;
              // }

              hCell.className += ' tableheader propheader';
              hCell.setAttribute('attKey', propTable.data.data.attKey);
              hCell.textContent = propTable.data.data.tag;

              if (!propTable.data.data.ishidden) {
                headerRow.appendChild(hCell);
              }
            }
          }
        }

        // Add transfer column
        if (property.data.transferrable) {
          const transferCell = deftemplate.createElement('TH');
          transferCell.className = 'input-min tableheader';
          headerRow.appendChild(transferCell);
        }

        // Add delete column
        const deleteCell = deftemplate.createElement('TH');
        deleteCell.className = ' tableheader cell-empty';
        headerRow.appendChild(deleteCell);

        const tbody = deftemplate.createElement('TBODY');
        tbody.className = 'table';
        tbody.className += ` ${inputgroup}`;
        tbody.setAttribute('id', property._id);
        sInput.appendChild(tbody);
      } else {
        sInput = deftemplate.createElement('INPUT');

        sInput.setAttribute('name', `data.attributes.${property.data.attKey}.value`);
        sInput.setAttribute('value', `{{data.data.attributes.${property.data.attKey}.value}}`);

        if (property.data.datatype === 'simplenumeric') {
          sInput.setAttribute('type', 'text');
          sInput.className = 'input-min';

          if (property.data.inputsize === 'M') {
            sInput.className = 'input-med';
          }

          if (!hasProperty(property.data, 'maxvisible')) {
            property.data.maxvisible = true;
          }

          if (property.data.automax !== '' && property.data.maxvisible) {
            sInputMax = await deftemplate.createElement('INPUT');
            sInputMax.setAttribute('type', 'text');
            sInput.className = 'input-ahalf ';
            sInputMax.className = `input-bhalf input-disabled inputGM ${property.data.attKey}.max`;
            sInputMax.setAttribute('name', `data.attributes.${property.data.attKey}.max`);
            sInputMax.setAttribute('value', `{{data.data.attributes.${property.data.attKey}.max}}`);
          }

          if (property.data.arrows) {
            sInputArrows = deftemplate.createElement('SPAN');
            const arrContainer = deftemplate.createElement('A');
            arrContainer.className = 'arrcontainer';
            arrContainer.style.display = 'inline-block';
            arrContainer.setAttribute('attKey', property.data.attKey);
            const arrUp = deftemplate.createElement('I');
            arrUp.className = 'arrup';
            const arrDown = deftemplate.createElement('I');
            arrDown.className = 'arrdown';

            arrContainer.appendChild(arrUp);
            arrContainer.appendChild(arrDown);
            sInputArrows.appendChild(arrContainer);
          }
        } else if (property.data.datatype === 'label') {
          sInput.setAttribute('type', 'text');
          sInput.className = 'input-free';
          sInput.style.display = 'none';
        } else {
          sInput.setAttribute('type', 'text');
          sInput.className = '';
          if (property.data.inputsize !== null) {
            if (property.data.inputsize === 'F') {
              sInput.className = 'input-free';
            } else if (property.data.inputsize === 'S') {
              sInput.className = 'input-small';
            } else if (property.data.inputsize === 'M') {
              sInput.className = 'input-med';
            } else if (property.data.inputsize === 'L') {
              sInput.className = 'input-large';
            } else if (property.data.inputsize === 'T') {
              sInput.className = 'input-tiny';
            }
          } else {
            sInput.className = 'input-free';
          }
        }

        if (property.data.auto !== '') {
          sInput.setAttribute('readonly', 'true');
          sInput.className += ' input-disabled';
        }
      }

      // Adds identifier
      sInput.className += ` ${property.data.attKey}`;
      if (property.data.datatype !== 'table') {
        sInput.className += ` ${inputgroup}`;
      }

      sInput.setAttribute('attId', property._id);

      if (!property.data.editable) {
        sInput.className += ' inputGM';
      }

      if (property.data.ishidden) {
        sInput.style.display = 'none';
        // if (sLabel !== null && property.data.haslabel) {
        //   sLabel.style.display = 'none';
        // }
      }

      if (property.data.datatype !== 'label') {
        divtemp.appendChild(sInput);
      }

      if (property.data.automax !== '' && property.data.maxvisible && sInputMax !== null) {
        // sInputMax.className += " " + inputgroup;
        // divtemp.insertBefore(sInputMax, sInput.nextSibling);
        await divtemp.appendChild(sInputMax);
      }
      if (sInputArrows !== null) {
        await divtemp.appendChild(sInputArrows);
      }

      count += 1;

      if (count === columns) {
        count = 0;
      }

      // }, this);
    }

    // GEt final HTML
    let parentRow;
    // console.log("rwidth: " + flags.rwidth + " rows: " + flags.rows);
    if (multiID === null) {
      console.log(`${tabpanel.name} width: ${flags.rwidth} rows: ${flags.rows} initial: ${initial}`);
    } else {
      console.log(`${tabpanel.name} rwidth: ${flags.rwidth} multiwidth: ${flags.multiwidth} initial: ${initial} maxrows ${flags.maxrows}`);
    }

    const checktest = deftemplate.getElementById(`${tabname}row${flags.rows}`);

    if ((flags.rwidth === 0 || initial) && (firstmrow || checktest === null)) {
      console.log('setting new row attribute');
      parentRow = deftemplate.createElement('DIV');
      parentRow.className = 'new-block';

      if (multiID === null) {
        parentRow.setAttribute('id', `${tabname}row${flags.rows}`);
        await parentNode.appendChild(parentRow);
      } else {
        const { multiwclass } = flags;
        console.log(`MultiPanel Container ${multiwclass}`);
        let parentRoot;
        const parentGranda = deftemplate.createElement('DIV');
        parentGranda.setAttribute('id', `${multiID}multi`);
        parentGranda.className = `${multiwclass}-col`;

        // If has header:
        if (multiName !== null && multiName !== '') {
          const newHeader = document.createElement('DIV');

          if (tabpanel.data.backg === 'T') {
            newHeader.className = 'panelheader-t';
          } else {
            newHeader.className = 'panelheader';
          }
          newHeader.className += ` ${multiheadergroup}`;
          newHeader.textContent = multiName;
          await parentGranda.appendChild(newHeader);
        }

        parentRow.setAttribute('id', `${multiID}multirow${flags.maxrows}`);

        if (flags.rwidth === 0) {
          parentRoot = document.createElement('DIV');
          parentRoot.className = 'new-block';

          parentRoot.setAttribute('id', `${tabname}row${flags.rows}`);
          parentNode.appendChild(parentRoot);
          parentRoot.appendChild(parentGranda);
        } else {
          parentRoot = deftemplate.getElementById(`${tabname}row${flags.rows}`);
          parentNode.appendChild(parentRoot);
          parentRoot.appendChild(parentGranda);
        }

        parentGranda.appendChild(parentRow);

        // parentGranda conditional visibility, to reorganize in method with previous ones
        //                if(_paneldata.condop!="NON"){
        //                    let attProp = ".value";
        //                    if(_paneldata.condat!=null){
        //                        if(_paneldata.condat.includes("max")){
        //                            attProp = "";
        //                        }
        //                    }
        //
        //
        //                    if(_paneldata.condop=="EQU"){
        //                        if(_paneldata.condvalue === "true"||_paneldata.condvalue === "false" || typeof _paneldata.condvalue ==="boolean"){
        //                            parentGranda.insertAdjacentHTML( 'beforebegin', "{{#if actor.data.attributes." + _paneldata.condat + attProp + "}}" );
        //                            parentGranda.insertAdjacentHTML( 'afterend', "{{/if}}" );
        //                        }
        //                        else{
        //                            parentGranda.insertAdjacentHTML( 'afterbegin', "{{#ifCond actor.data.attributes." + _paneldata.condat + attProp + " '" + _paneldata.condvalue + "'}}" );
        //                            parentGranda.insertAdjacentHTML( 'beforeend', "{{/ifCond}}" );
        //                        }
        //
        //                    }
        //
        //                    else if(_paneldata.condop=="HIH"){
        //                        parentGranda.insertAdjacentHTML( 'afterbegin', "{{#ifGreater actor.data.attributes." + _paneldata.condat + attProp + " '" + _paneldata.condvalue + "'}}" );
        //                        parentGranda.insertAdjacentHTML( 'beforeend', "{{/ifGreater}}" );
        //                    }
        //
        //                    else if(_paneldata.condop=="LOW"){
        //                        parentGranda.insertAdjacentHTML( 'afterbegin', "{{#ifLess actor.data.attributes." + _paneldata.condat + attProp + " '" + _paneldata.condvalue + "'}}" );
        //                        parentGranda.insertAdjacentHTML( 'beforeend', "{{/ifLess}}" );
        //                    }
        //                }
      }
    } else if (multiID === null) {
      console.log(`getting existing row id ${tabname}row${flags.rows}`);
      parentRow = deftemplate.getElementById(`${tabname}row${flags.rows}`);
    } else if (initial) {
      // parentRow = deftemplate.getElementById(multiID + "multi");
      parentRow = document.createElement('DIV');
      parentRow.className = 'new-multiblock';

      let parentRoot;
      const parentGranda = deftemplate.getElementById(`${multiID}multi`);

      console.log(`Creating multiRow Container: ${multiID}multirow${flags.maxrows}`);
      parentRow.setAttribute('id', `${multiID}multirow${flags.maxrows}`);

      if (flags.rwidth === 0) {
        parentRoot = deftemplate.createElement('DIV');
        parentRoot.className = 'new-block';
        console.log(`creating row: ${flags.rows}`);
        parentRoot.setAttribute('id', `${tabname}row${flags.rows}`);
      } else {
        parentRoot = deftemplate.getElementById(`${tabname}row${flags.rows}`);
      }

      await parentNode.appendChild(parentRoot);
      await parentRoot.appendChild(parentGranda);
      await parentGranda.appendChild(parentRow);
    } else {
      parentRow = deftemplate.getElementById(`${multiID}multirow${flags.maxrows}`);
    }
    console.log('almost there');
    await parentRow.appendChild(div6);
    console.log(div6);

    // ADD VISIBILITY RULES TO PANEL
    if (tabpanel.data.condop !== 'NON') {
      let attProp = '.value';
      if (tabpanel.data.condat !== null) {
        if (tabpanel.data.condat.includes('.max')) {
          attProp = '';
        }
      }

      if (tabpanel.data.condop === 'EQU') {
        console.log(div6);
        if ((tabpanel.data.condvalue === 'true' || tabpanel.data.condvalue === 'false' || tabpanel.data.condvalue === true || tabpanel.data.condvalue === false)) {
          div6.insertAdjacentHTML('beforebegin', `{{#if actor.data.attributes.${tabpanel.data.condat}${attProp}}}`);
          div6.insertAdjacentHTML('afterend', '{{/if}}');
        } else {
          div6.insertAdjacentHTML('afterbegin', `{{#ifCond actor.data.attributes.${tabpanel.data.condat}${attProp} '${tabpanel.data.condvalue}'}}`);
          div6.insertAdjacentHTML('beforeend', '{{/ifCond}}');
        }
      } else if (tabpanel.data.condop === 'HIH') {
        div6.insertAdjacentHTML('afterbegin', `{{#ifGreater actor.data.attributes.${tabpanel.data.condat}${attProp} '${tabpanel.data.condvalue}'}}`);
        div6.insertAdjacentHTML('beforeend', '{{/ifGreater}}');
      } else if (tabpanel.data.condop === 'LOW') {
        div6.insertAdjacentHTML('afterbegin', `{{#ifLess actor.data.attributes.${tabpanel.data.condat}${attProp} '${tabpanel.data.condvalue}'}}`);
        div6.insertAdjacentHTML('beforeend', '{{/ifLess}}');
      } else if (tabpanel.data.condop === 'NOT') {
        div6.insertAdjacentHTML('afterbegin', `{{#ifNot actor.data.attributes.${tabpanel.data.condat}${attProp} '${tabpanel.data.condvalue}'}}`);
        div6.insertAdjacentHTML('beforeend', '{{/ifNot}}');
      }
    }

    if (tabpanel.data.isimg) {
      div6.setAttribute('img', tabpanel.data.imgsrc);
      div6.className += ' isimg';

      if (tabpanel.data.contentalign === 'center') {
        div6.className += ' centertext';
      }
    }

    console.log(div6);

    console.log('almost there 2');
    const finalreturn = new XMLSerializer().serializeToString(deftemplate);
    return finalreturn;
    // this.actor.data.data._html = deftemplate.innerHTML;
  }

  async exportHTML(htmlObject, filename) {
    const data = new FormData();
    const blob = new Blob([htmlObject], { type: 'text/html' });

    data.append('target', `worlds/${game.data.world.name}/`);
    data.append('upload', blob, `${filename}.html`);
    data.append('source', 'data');
    console.log(data);

    fetch('upload', { method: 'POST', body: data });
  }

  /*  -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 */

  /**
   * Builds the character sheet template based on the options included
   */

  async buildSheet() {
    const { actor } = this;
    const { tabs } = actor.data.data;
    const { flags } = this.actor.data;

    const newhtml = await auxMeth.buildSheetHML();
    const stringHTML = new XMLSerializer().serializeToString(newhtml);
    // console.log(stringHTML);
    await this.actor.update({ 'data._html': stringHTML });

    setProperty(flags, 'rows', 0);
    setProperty(flags, 'rwidth', 0);
    setProperty(flags, 'multiwidth', 0);
    setProperty(flags, 'maxwidth', 0);
    setProperty(flags, 'maxrows', 0);
    setProperty(flags, 'multiwclass', '');

    console.log(actor);

    const keychecker = await this.checkTemplateKeys(tabs);
    await this.actor.update({ 'data.buildlog': keychecker.checkerMsg });
    console.log(keychecker);
    if (keychecker.hasissue) {
      ui.notifications.warn('Template actor has consistency problems, please check Config Tab');
    } else {
      await this.buildHTML(tabs);
      this.actor.update({ 'data.flags': flags }, { diff: false });
    }
  }

  async setcItemsKey() {
    const gamecItems = game.items.filter((y) => y.data.type === 'cItem');
    for (let i = 0; i < gamecItems.length; i += 1) {
      const mycitem = gamecItems[i];
      if (mycitem.data.data.ciKey === '') {
        await mycitem.update({ 'data.data.ciKey': mycitem.id });
      }
    }
  }

  async checkConsistency() {
    const gamecItems = game.items.filter((y) => y.data.type === 'cItem');
    let toupdate = false;
    for (let i = 0; i < gamecItems.length; i += 1) {
      const mycitem = gamecItems[i];
      const mycitemmods = mycitem.data.data.mods;
      for (let j = 0; j < mycitemmods.length; j += 1) {
        const mymod = mycitemmods[j];
        // setProperty(mymod, "citem", mycitem.data.id);
        // if (!hasProperty(mymod, "index")) {
        //     setProperty(mymod, "index", j);
        //     toupdate = true;
        // }

        if (mymod.items.length > 0) {
          for (let h = 0; h < mymod.items.length; h += 1) {
            if (mymod.items[h].ciKey === null) {
              const toaddotem = await auxMeth.getcItem(mymod.items[h].id, mymod.items[h].ciKey);
              if (toaddotem) {
                toupdate = true;
                mymod.items[h].ciKey = toaddotem.data.data.ciKey;
              }
            }
          }
        }
      }
      if (toupdate) {
        console.log('updating consistency');
        await mycitem.update({ data: mycitem.data.data });
      }
    }

    // let gameactors = game.actors;
    // for (let i = 0; i < gameactors.entities.length; i += 1) {

    //     const myactor = gameactors.entities[i];
    //     const myactorcitems = myactor.data.data.citems;
    //     //console.log("checking actor " + myactor.name);
    //     //console.log(myactorcitems);
    //     if (!myactor.data.data.istemplate) {
    //         if (myactorcitems !== null) {
    //             for (let j = myactorcitems.length - 1; j >= 0; j -= 1) {
    //                 let mycitem = myactorcitems[j];
    //                 //console.log(mycitem);
    //                 if (mycitem !== null) {
    //                     //let templatecItem = game.items.get(mycitem.id);
    //                     let templatecItem = await auxMeth.getcItem(mycitem.id, mycitem.iKey);
    //                     //console.log(templatecItem);

    //                     if (templatecItem !== null) {
    //                         let isconsistent = true;
    //                         let mymods = mycitem.mods;
    //                         if (mymods !== null) {
    //                             for (let r = 0; r < mymods.length; r += 1) {
    //                                 if (mycitem.id !== mymods[r].citem)
    //                                     mymods[r].citem = mycitem.id;
    //                                 if (!hasProperty(mymods[r], "index"))
    //                                     setProperty(mymods[r], "index", 0);

    //                                 if (templatecItem.data.data.mods[mymods[r].index] === null) {
    //                                     //console.log(templatecItem.name);
    //                                     //isconsistent = false;
    //                                 }

    //                                 else {
    //                                     if (mymods[r].expr !== templatecItem.data.data.mods[mymods[r].index].value)
    //                                         isconsistent = false;
    //                                 }

    //                             }
    //                         }

    //                         //MOD change consistency checker
    //                         if (!isconsistent) {
    //                             console.log(templatecItem.name + " is fucked in " + myactor.name);
    //                             let newData = await myactor.deletecItem(templatecItem.id, true);
    //                             await this.actor.update({ "data": newData.data });
    //                             let subitems = await myactor.addcItem(templatecItem);
    //                             if (subitems)
    //                                 this.updateSubItems(false, subitems);
    //                             //await myactor.update(myactor.data);
    //                         }

    //                     }

    //                     else {
    //                         delete myactorcitems[j];
    //                     }

    //                 }

    //                 else {
    //                     //myactorcitems.split(myactorcitems[j],1);
    //                     delete myactorcitems[j];
    //                 }

    //             }
    //         }

    //         try {
    //             await myactor.update({ "data": myactor.data.data }, { stopit: true });
    //         }
    //         catch (err) {
    //             ui.notifications.warn("Character " + myactor.name + " has consistency problems");
    //         }
    //     }

    // }
  }

  async checkTemplateKeys(tabs) {
    let hasissue = false;
    let compilationMsg = '';
    const myreturn = {};

    // SET CurRenT DATE
    let today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = today.getFullYear();

    today = `${mm}/${dd}/${yyyy}`;
    compilationMsg += `Last rebuilt: ${today}, `;

    const allProps = [];
    const allTabs = [];
    const allPanels = [];
    for (let y = 0; y < tabs.length; y += 1) {
      // let titem = game.items.get(tabs[y].id);
      const titem = await auxMeth.getTElement(tabs[y].id, 'sheettab', tabs[y].ikey);
      let tabitempanels = [];
      if (titem !== null) {
        tabitempanels = titem.data.data.panels;
        allTabs.push(titem.data.data.tabKey);
      } else {
        allTabs.push(`${tabs[y].name}_TAB_NONEXISTING`);
        hasissue = true;
      }

      if (tabitempanels === null) {
        tabitempanels = [];
      }

      for (let i = 0; i < tabitempanels.length; i += 1) {
        // let tabpanel = game.items.get(tabitempanels[i].id);
        const tabpanel = await auxMeth.getTElement(tabitempanels[i].id, 'panel', tabitempanels[i].ikey);
        console.log(`building panel with Key: ${tabitempanels[i].ikey}`);
        if (tabpanel === null) {
          console.log('PANEL NOT FOUND');
        }
        // console.log(tabpanel.name);
        let panelproperties = [];
        if (tabpanel !== null) {
          if (tabpanel.data.type === 'multipanel') {
            for (let b = 0; b < tabpanel.data.data.panels.length; b += 1) {
              // let subpanel = game.items.get(tabpanel.data.data.panels[b].id);
              console.log(tabpanel.data.data.panels[b]);
              // console.log(tabpanel.data.data.panels[b].ikey);
              const subpanel = await auxMeth.getTElement(tabpanel.data.data.panels[b].id, 'panel', tabpanel.data.data.panels[b].ikey);
              if (subpanel) {
                const subproperties = subpanel.data.data.properties;
                allPanels.push(subpanel.data.data.panelKey);
                panelproperties = [].concat(panelproperties, subproperties);
              } else {
                ui.notifications.warn(`Please remove panel ${tabpanel.data.data.panels[b].name} at multipanel ${tabpanel.name}`);
              }
            }
          } else {
            panelproperties = tabpanel.data.data.properties;
            allPanels.push(tabpanel.data.data.panelKey);
          }
        } else {
          allPanels.push(`${tabitempanels[i].name}_PANEL_NONEXISTING`);
          hasissue = true;
        }

        if (panelproperties === null) {
          panelproperties = [];
        }

        for (let j = 0; j < panelproperties.length; j += 1) {
          // let property = game.items.get(panelproperties[j].id);
          const property = await auxMeth.getTElement(panelproperties[j].id, 'property', panelproperties[j].ikey);
          if (property !== null) {
            if (property.data.data.datatype === 'table' && property.data.data.group.id === null) {
              compilationMsg += `${panelproperties[j].name} table property lacks table group`;
              hasissue = true;
            }

            allProps.push(property.data.data.attKey);
          } else {
            allProps.push(`${panelproperties[j].name}_PROP_NONEXISTING`);
            hasissue = true;
          }
        }
      }
    }

    // CHECK FOR DUPLICATES
    const duplicateProps = allProps.filter((e, i, a) => a.indexOf(e) !== i);
    for (let n = 0; n < duplicateProps.length; n += 1) {
      compilationMsg += `property key ${duplicateProps[n]} is duplicated,`;
    }

    const duplicatePanels = allPanels.filter((e, i, a) => a.indexOf(e) !== i);
    for (let m = 0; m < duplicatePanels.length; m += 1) {
      compilationMsg += `panel key ${duplicatePanels[m]} is duplicated, `;
    }

    const duplicateTabs = allTabs.filter((e, i, a) => a.indexOf(e) !== i);
    for (let s = 0; s < duplicateTabs.length; s += 1) {
      compilationMsg += `panel key ${duplicateTabs[s]} is duplicated, `;
    }

    // CHECK FOR INCORRECT KEYS
    Object.keys(allProps).forEach((checkPrKey) => {
      if (/\s/.checkPrKey) {
        compilationMsg += `property key ${checkPrKey} includes blank space, `;
        hasissue = true;
      }
    });

    Object.keys(allPanels).forEach((checkPaKey) => {
      if (/\s/.checkPaKey) {
        compilationMsg += `panel key ${checkPaKey} includes blank space, `;
        hasissue = true;
      }
    });

    Object.keys(allTabs).forEach((checkTaKey) => {
      if (/\s/.checkTaKey) {
        compilationMsg += `tab key ${checkTaKey} includes blank space, `;
        hasissue = true;
      }
    });

    // CHECK NONEXISTING TEMPLATE ELEMENTS
    let nonEmsg = '';
    const checkNonE = allProps.concat(allPanels, allTabs);
    const nonE = checkNonE.filter((y) => y.includes('_NONEXISTING'));
    if (nonE.length > 0) {
      nonEmsg = ', the following elements do not exist in world (type included after _):';
      hasissue = true;
    }

    for (let r = 0; r < nonE.length; r += 1) {
      const noneKey = nonE[r].replace('_NONEXISTING', '');
      nonEmsg += `${noneKey}, `;
    }

    compilationMsg += nonEmsg;

    // IF NOTHING WRONG
    if (!hasissue) {
      compilationMsg += ' SUCCESFULLY REBUILT';
    }

    myreturn.hasissue = hasissue;
    myreturn.checkerMsg = compilationMsg;

    return myreturn;
  }

  find_duplicate_in_array(myarray) {
    const result = [];
    for (let i = 0; i < myarray.length; i += 1) {
      const myelement = myarray[i];
      const timespresent = myarray.filter((v) => (v === myelement)).length;
      if (timespresent > 0 && !result.includes(myelement)) {
        result.push(myelement);
      }
    }
    return result;
  }

  async buildHTML(tabs) {
    console.log('building HTML');

    let newHTML;

    // if (game.settings.get("sandbox", "consistencycheck") !== "") {
    await this.checkConsistency();
    // }

    const { flags } = this.actor.data;
    for (let y = 0; y < tabs.length; y += 1) {
      // const titem = game.items.get(tabs[y].id).data;
      const titemfinder = await auxMeth.getTElement(tabs[y].id, 'sheettab', tabs[y].ikey);
      const titem = titemfinder.data;
      console.log(titem);
      console.log(tabs[y].ikey);
      flags.rwidth = 0;
      newHTML = await this.addNewTab(newHTML, titem, y + 1);
      // console.log(newHTML);
      const tabname = titem.data.tabKey;

      // let gtabitem = JSON.parse(titem.data.panels);
      const tabitempanels = titem.data.panels;
      // console.log(tabitempanels);

      flags.maxrows = 0;

      for (let i = 0; i < tabitempanels.length; i += 1) {
        // let tabpanel = game.items.get(tabitempanels[i].id);
        const tabpanel = await auxMeth.getTElement(tabitempanels[i].id, 'panel', tabitempanels[i].ikey);
        // console.log(tabpanel);

        if (tabpanel.type === 'panel') {
          newHTML = await this.addNewPanel(newHTML, tabpanel.data, titem.data.tabKey, tabname, true);
        }

        //                if(newpanelHTML!=null)
        //                    break;
        // console.log(newHTML);

        if (tabpanel.data.type === 'multipanel') {
          console.log('hay multi!');

          const multipanels = tabpanel.data.data.panels;
          const multiwidth = this.freezeMultiwidth(tabpanel.data);
          const newtotalwidth = flags.rwidth + multiwidth;

          flags.maxwidth = multiwidth;
          flags.multiwidth = 0;
          flags.multiwclass = this.getmultiWidthClass(tabpanel.data.data.width);

          // console.log(multipanels);
          let firstmrow = true;
          const ismulti = true;
          for (let j = 0; j < multipanels.length; j += 1) {
            // let singlepanel = game.items.get(multipanels[j].id);
            const singlepanel = await auxMeth.getTElement(multipanels[j].id, 'panel', multipanels[j].ikey);
            // console.log(multipanels[j]);
            // LAst argument is only to pass the conditionals. Poorly done, to fix in the future.
            newHTML = await this.addNewPanel(newHTML, singlepanel.data, titem.data.tabKey, tabname, firstmrow, tabpanel.data.data.panelKey, tabpanel.data.data.title, null, tabpanel.data.data.headergroup);
            newHTML = await this.addMultipanelVisibility(newHTML, tabpanel.data.data.panelKey, tabpanel.data.data.condat, tabpanel.data.data.condop, tabpanel.data.data.condvalue);
            if (firstmrow) {
              flags.rwidth += multiwidth;
            }
            firstmrow = false;
          }
        }
      }
      //            if(newpanelHTML!=null)
      //                break;
    }

    if (newHTML === null) {
      newHTML = this.actor.data.data._html;
    }

    console.log('panels built');
    await this.hideTabsinTemplate();
    // console.log(newHTML);

    const wrapper = document.createElement('div');
    wrapper.innerHTML = newHTML;
    this.actor.data.data._html = newHTML;
    const deftemplate = wrapper;
    // console.log(deftemplate);

    await this.registerHTML(deftemplate.querySelector('#sheet').outerHTML);
  }

  async addMultipanelVisibility(html, multiKey, att, op, val) {
    const deftemplate = new DOMParser().parseFromString(html, 'text/html');
    const parentGranda = deftemplate.getElementById(`${multiKey}multi`);
    if (op !== 'NON') {
      let attProp = '.value';
      if (att !== null) {
        if (att.includes('.max')) {
          attProp = '';
        }
      }

      if (op === 'EQU') {
        if (val === 'true' || val === 'false' || typeof val === 'boolean') {
          parentGranda.insertAdjacentHTML('beforebegin', `{{#if actor.data.attributes.${att}${attProp}}}`);
          parentGranda.insertAdjacentHTML('afterend', '{{/if}}');
        } else {
          parentGranda.insertAdjacentHTML('afterbegin', `{{#ifCond actor.data.attributes.${att}${attProp} '${val}'}}`);
          parentGranda.insertAdjacentHTML('beforeend', '{{/ifCond}}');
        }
      } else if (op === 'HIH') {
        parentGranda.insertAdjacentHTML('afterbegin', `{{#ifGreater actor.data.attributes.${att}${attProp} '${val}'}}`);
        parentGranda.insertAdjacentHTML('beforeend', '{{/ifGreater}}');
      } else if (op === 'LOW') {
        parentGranda.insertAdjacentHTML('afterbegin', `{{#ifLess actor.data.attributes.${att}${attProp} '${val}'}}`);
        parentGranda.insertAdjacentHTML('beforeend', '{{/ifLess}}');
      } else if (op === 'NOT') {
        parentGranda.insertAdjacentHTML('afterbegin', `{{#ifNot actor.data.attributes.${att}${attProp} '${val}'}}`);
        parentGranda.insertAdjacentHTML('beforeend', '{{/ifNot}}');
      }
    }

    const finalreturn = new XMLSerializer().serializeToString(deftemplate);
    return finalreturn;
  }

  hideTabsinTemplate() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = this.actor.data.data._html;
    const deftemplate = wrapper;

    // Tab selector
    const p = deftemplate.querySelector('#tab-0');
    const c = deftemplate.querySelector('#tab-last');
    p.insertAdjacentHTML('beforebegin', '{{#if actor.data.istemplate}}');
    p.insertAdjacentHTML('beforebegin', '{{else}}');
    c.insertAdjacentHTML('afterend', '{{/if}}');
  }

  freezeMultiwidth(tabpanel) {
    let newidth = 0;
    if (tabpanel.data.width === '1') {
      newidth = 1;
    } else if (tabpanel.data.width === '1/3') {
      newidth = 0.333;
    } else if (tabpanel.data.width === '2/3') {
      newidth = 0.666;
    } else if (tabpanel.data.width === '5/6') {
      newidth = 0.833;
    } else if (tabpanel.data.width === '3/4') {
      newidth = 0.75;
    } else if (tabpanel.data.width === '1/2') {
      newidth = 0.5;
    } else if (tabpanel.data.width === '1/4') {
      newidth = 0.25;
    } else if (tabpanel.data.width === '1/6') {
      newidth = 0.166;
    } else if (tabpanel.data.width === '1/8') {
      newidth = 0.125;
    } else if (tabpanel.data.width === '3/10') {
      newidth = 0.3;
    } else if (tabpanel.data.width === '1/16') {
      newidth = 0.0625;
    } else if (tabpanel.data.width === '5/8') {
      newidth = 0.625;
    } else if (tabpanel.data.width === '3/8') {
      newidth = 0.375;
    } else {
      newidth = 1;
    }
    return newidth;
  }

  getmultiWidthClass(width) {
    let wclass = '';
    if (width === '1') {
      wclass = 'multi-1-1';
    } else if (width === '1/3') {
      wclass = 'multi-1-3';
    } else if (width === '2/3') {
      wclass = 'multi-2-3';
    } else if (width === '5/6') {
      wclass = 'multi-5-6';
    } else if (width === '1/2') {
      wclass = 'multi-1-2';
    } else if (width === '1/4') {
      wclass = 'multi-1-4';
    } else if (width === '3/4') {
      wclass = 'multi-3-4';
    } else if (width === '1/6') {
      wclass = 'multi-1-6';
    } else if (width === '1/8') {
      wclass = 'multi-1-8';
    } else if (width === '3/10') {
      wclass = 'multi-3-10';
    } else if (width === '1/16') {
      wclass = 'multi-1-16';
    } else if (width === '5/8') {
      wclass = 'multi-5-8';
    } else if (width === '3/8') {
      wclass = 'multi-3-8';
    } else {
      wclass = 'multi-1-1';
    }
    return wclass;
  }

  async registerHTML(htmlObject) {
    console.log('registering HTML');

    let stringed = htmlObject.replace('=""', '');

    stringed = stringed.replace(/toparse="/g, '');
    stringed = stringed.replace(/~~"/g, '');

    // this.actor.data.data.gtemplate = this.actor.name;
    this.refreshSheet(this.actor.name);
    // this.actor.data.data._html = stringed;

    await this.actor.update({ 'data._html': stringed });
    // console.log(stringed);

    // THIS IS THE LIMITANT CHANGE:
    // await this.actor.update(this.actor.data);

    await this.actor.update();

    await auxMeth.getSheets();

    await this.setcItemsKey();

    // Comment this for debug
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  }

  /*  -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 */

  /**
   * Drop element event
   */
  async _onDrop(event) {
    // Initial checks
    event.preventDefault();
    event.stopPropagation();

    let dropitem;
    let dropdata;

    try {
      dropdata = JSON.parse(event.dataTransfer.getData('text/plain'));
      dropitem = game.items.get(dropdata.id);

      if (dropitem.data.type !== 'sheettab' && dropitem.data.type !== 'cItem') {
        // You can only drop sheettabs or cItems
        return Promise.reject();
      }
    } catch (err) {
      console.error('drop error');
      console.error(event.dataTransfer.getData('text/plain'));
      console.error(err);
      return Promise.reject();
    }

    if (dropdata.ownerID) {
      if (this.actor.id === dropdata.ownerID) {
        return Promise.reject();
      }

      this.showTransferDialog(dropdata.id, dropdata.ownerID);

      return Promise.resolve();
    }

    let subitemsTag;
    let isTab = true;
    let subiDataKey;
    let isUnique = true;

    if (dropitem.data.type === 'sheettab') {
      subitemsTag = 'tabs';
      subiDataKey = 'tabKey';
    } else if (dropitem.data.type === 'cItem') {
      subitemsTag = 'citems';
      isTab = false;
      subiDataKey = 'ciKey';

      if (!dropitem.data.data.isUnique) {
        isUnique = false;
      }
    }

    // Add tab id to panel
    let subitems = duplicate(this.actor.data.data[subitemsTag]);
    let increaseNum = false;

    for (let i = 0; i < subitems.length; i += 1) {
      if (subitems[i].id === dropitem.id) {
        if (isUnique) {
          // item is unique, can not double
          return Promise.reject();
        }

        subitems[i].number = parseInt(subitems[i].number, 10) + 1;
        subitems[i].uses = parseInt(subitems[i].uses, 10) + parseInt(dropitem.data.data.maxuses, 10);
        increaseNum = true;
        // await this.updateSubItems(isTab,subitems);
        // await this.actor.actorUpdater();
        // return;
      }
    }

    if (!increaseNum) {
      if (dropitem.data.type === 'cItem') {
        // console.log("adding cItem");
        subitems = await this.actor.addcItem(dropitem);
      } else {
        const itemKey = dropitem.data.data[subiDataKey];
        const newItem = {};

        setProperty(newItem, itemKey, {});
        newItem[itemKey].id = dropitem.id;
        newItem[itemKey].ikey = itemKey;
        newItem[itemKey].name = dropitem.data.name;

        subitems.push(newItem[itemKey]);
        // await this.scrollbarSet();
      }
    }

    await this.updateSubItems(isTab, subitems);

    return Promise.resolve();
  }

  async updateSubItems(isTab, subitems) {
    // await this.actor.update();

    if (isTab) {
      // await this.actor.update({"data.tabs": subitems}, {diff: false});
      this.actor.data.data.tabs = subitems;
      await this.actor.update({ 'data.tabs': subitems });
    } else {
      // this.actor.data.data.citems= subitems;
      // await this.actor.update(this.actor.data);
      // eslint-disable-next-line no-lonely-if
      if (this.actor.isToken) {
        const myToken = canvas.tokens.get(this.actor.token.id);
        await myToken.document.update({ 'actorData.data.citems': subitems });
      } else {
        await this.actor.update({ 'data.citems': subitems });
      }
    }
    // console.log("updating after drop");

    return subitems;
  }

  /*  -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 -= 1 */

  async refreshCItems(basehtml) {
    // console.log("refreshingCItems");
    // TEST
    const parser = new DOMParser();
    const htmlcode = await auxMeth.getTempHTML(this.actor.data.data.gtemplate);
    const _basehtml = await parser.parseFromString(htmlcode, 'text/html').querySelector('form');
    if (_basehtml === null) {
      ui.notifications.warn('Please rebuild character sheet before assigning');
      return;
    }

    // console.log(basehtml);
    // GET CITEMS
    let myactor = this.actor.data.data;

    if (this.actor.isToken) {
      const tokenId = this.id.split('-')[2];
      const mytoken = canvas.tokens.get(tokenId);
      myactor = mytoken.actor.data.data;
    }
    const { citems } = myactor;
    const { attributes } = myactor;

    // SET TABLES INFO
    const html = await basehtml.find('.table');
    const _html = await _basehtml.querySelectorAll('table');

    // Gets all game properties
    const propitems = game.items.filter((y) => y.data.type === 'property' && y.data.data.datatype === 'table');
    // console.log(propitems);

    const totalTables = [];
    const forceUpdate = false;

    for (let y = 0; y < html.length; y += 1) {
      const tableID = html[y].id;
      const tableVisible = true;
      const newElement = { tableID, tableVisible };
      totalTables.push(newElement);
    }

    for (let y = 0; y < _html.length; y += 1) {
      const tableID = _html[y].getAttribute('attid');
      const tableVisible = false;
      const existingTable = totalTables.find((et) => et.tableID === tableID);
      const newElement = { tableID, tableVisible };
      if (existingTable === null) {
        totalTables.push(newElement);
      }
    }

    // console.log(totalTables);

    for (let i = 0; i < totalTables.length; i += 1) {
      // console.log(html);
      const { tableID } = totalTables[i];
      const table = html[i];
      let inputgroup;

      // let table = html.find(y=>y.id==tableID);
      // console.log(tableID);

      if (table !== null) {
        table.innerHTML = '';
        inputgroup = await table.parentNode.getAttribute('inputgroup');
        if (inputgroup === null) {
          inputgroup = await table.getAttribute('inputgroup');
        }
      }

      if (inputgroup === null) {
        inputgroup = '';
      }

      const propTable = await propitems.find((y) => y.id === tableID);

      // const propTable = await propitems.find(y=>y.id === html[i].getAttribute("attid"));
      let group;
      let groupID;
      let tableKey;
      let isFree;

      if (propTable !== null) {
        groupID = propTable.data.data.group;
        // group = game.items.get(groupID.id);
        group = await auxMeth.getTElement(groupID.id, 'group', groupID.ikey);
        tableKey = propTable.data.data.attKey;
        isFree = propTable.data.data.isfreetable;
      }

      if (group !== null) {
        const groupprops = group.data.data.properties;
        let groupcitems;

        if (isFree) {
          if (attributes[tableKey] !== null) {
            groupcitems = attributes[tableKey].tableitems;

            if (this.sortOption !== null) {
              if (this.sortOption[tableKey] !== null) {
                groupcitems = groupcitems.sort(auxMeth.aTdynamicSort(this.sortOption[tableKey], propTable.data.data.datatype));
              }
            }
          }

          if (groupcitems === null) {
            groupcitems = [];
          }
        } else {
          groupcitems = await citems.filter((y) => y.groups.find((item) => item.id === groupID.id || item.ikey === groupID.ikey));
          groupcitems = groupcitems.sort(auxMeth.dynamicSort('name'));

          if (this.sortOption !== null) {
            if (this.sortOption[tableKey] !== null && this.sortOption[tableKey] !== 'name') {
              groupcitems = groupcitems.sort(auxMeth.aTdynamicSort(this.sortOption[tableKey], propTable.data.data.datatype));
            }
          }
        }

        for (let n = 0; n < groupcitems.length; n += 1) {
          const ciObject = groupcitems[n];
          let ciTemplate;
          if (!isFree) {
            // ciTemplate = game.items.get(ciObject.id);
            ciTemplate = await auxMeth.getcItem(ciObject.id, ciObject.ciKey);
            // if (ciTemplate === null) {
            //     //game.actors.importFromCompendium(<PACK>, <ID>, {}, {keepId: true})

            //     let locatedPack;
            //     let locatedId;
            //     for (let pack of game.packs) {
            //         const packContents = await pack.getDocuments();
            //         let citems = packContents.filter(y => Boolean(y.data.data));
            //         let is_here = citems.find(y => y.data.data.ciKey === ciObject.id);
            //         if (is_here) {
            //             locatedPack = pack;
            //             locatedId = is_here.id;
            //         }

            //     }

            //     if (locatedPack !== null) {
            //         let importedobject = await game.items.importFromCompendium(locatedPack, locatedId, { folder: locatedPack.name, keepId: true });
            //         console.log(importedobject);
            //         ciTemplate = importedobject;
            //         ciObject.id = ciTemplate.id;
            //     }

            //     else {
            //         citems.splice(citems.indexOf(ciObject), 1);
            //         forceUpdate = true;
            //         continue;
            //     }

            // }
          }

          // console.log(ciObject.name);
          const newRow = document.createElement('TR');
          newRow.className = `table-row ${inputgroup} ${propTable.data.data.attKey}_row`;
          let rowname = `table-row-${ciObject.id}`;

          if (!isFree) {
            rowname = ciObject.name;
          }

          newRow.setAttribute('name', ciObject.name);
          newRow.setAttribute('id', ciObject.id);
          newRow.setAttribute('ciKey', ciObject.ciKey);

          if (table !== null) {
            table.appendChild(newRow);
          }

          if (ciObject !== null && (ciTemplate !== null || isFree)) {
            // Link Element
            if ((propTable.data.data.onlynames === 'DEFAULT' || propTable.data.data.onlynames === 'ONLY_NAMES') && !isFree) {
              const firstcell = document.createElement('TD');
              firstcell.className = `input-free linkable tablenamecell ${propTable.data.data.attKey}_namecell`;
              firstcell.className += ` ${inputgroup}`;
              firstcell.textContent = ciObject.name;
              firstcell.setAttribute('item_id', ciObject.id);
              firstcell.setAttribute('item_ciKey', ciObject.ciKey);
              firstcell.addEventListener('click', this.linkCItem, false);
              newRow.appendChild(firstcell);
            }

            if ((propTable.data.data.onlynames !== 'ONLY_NAMES')) {
              if (propTable.data.data.hasactivation && !isFree) {
                const activecell = document.createElement('TD');
                activecell.className = 'input-min centertext';
                activecell.className += ` ${inputgroup}`;
                newRow.appendChild(activecell);

                if (ciObject.usetype === 'ACT' && !isFree) {
                  const activeinput = document.createElement('INPUT');
                  activeinput.className = 'centertext';
                  activeinput.className += ` ${inputgroup}`;
                  activeinput.type = 'checkbox';
                  activeinput.checked = ciObject.isactive;

                  activeinput.addEventListener('change', (event) => this.useCIIcon(ciObject.id, ciObject.ciKey, activeinput.checked, false, true));

                  activecell.appendChild(activeinput);
                } else if (ciObject.usetype === 'CON' && !isFree) {
                  let inputwrapper = document.createElement('a');
                  let torecharge = false;

                  if (ciObject.uses > 0 || ciObject.maxuses === 0) {
                    inputwrapper.addEventListener('click', (event) => this.useCIIcon(ciObject.id, ciObject.ciKey, false, true));
                  } else if (ciObject.rechargable) {
                    torecharge = true;
                  } else {
                    inputwrapper = document.createElement('DIV');
                  }

                  inputwrapper.className = 'consumable-button';
                  inputwrapper.title = 'Use item';
                  activecell.appendChild(inputwrapper);

                  const activeinput = document.createElement('i');
                  if (ciObject.icon === 'BOOK') {
                    activeinput.className = 'fas fa-book';
                  } else if (ciObject.icon === 'VIAL') {
                    activeinput.className = 'fas fa-vial';
                  } else {
                    activeinput.className = 'fas fa-star';
                  }

                  if (torecharge) {
                    activeinput.className = 'fas fa-recycle';
                    inputwrapper.addEventListener('click', (event) => this.rechargeCI(ciObject.id, ciObject.ciKey));
                  }

                  inputwrapper.appendChild(activeinput);
                }
              }

              if (propTable.data.data.hasunits && !isFree) {
                const numcell = document.createElement('TD');
                numcell.className = 'input-min centertext';
                numcell.className += ` ${inputgroup}`;
                newRow.appendChild(numcell);

                const numinput = document.createElement('INPUT');
                numinput.className = 'table-input table-num centertext';
                numinput.className += ` ${inputgroup}`;

                const ciNumber = ciObject.number;

                numinput.value = ciObject.number;
                numinput.addEventListener('change', (event) => this.changeCINum(ciObject.id, ciObject.ciKey, event.target.value));

                numcell.appendChild(numinput);
              }

              // REMOVE USES WORKSTREAM
              if (propTable.data.data.hasuses && propTable.data.data.hasactivation && !isFree) {
                const usescell = document.createElement('TD');
                usescell.className = 'tabblock-center';
                usescell.className += ` ${inputgroup}`;
                newRow.appendChild(usescell);

                const usevalue = document.createElement('INPUT');
                usevalue.className = 'table-num centertext';
                usevalue.className += ` ${inputgroup}`;

                usescell.appendChild(usevalue);

                if (!propTable.data.data.editable && !game.user.isGM) {
                  // usevalue.setAttribute("readonly", "true");
                  usevalue.className += ' inputGM';
                }

                if (ciObject.usetype === 'CON') {
                  const { maxuses } = ciObject;

                  // if (!isFree)
                  //     maxuses = await auxMeth.autoParser(ciTemplate.data.data.maxuses, attributes, ciObject.attributes, false);
                  // maxuses = parseInt(maxuses);

                  const ciuses = ciObject.uses;

                  // if (Number.isNaN(ciuses))
                  //     ciObject.uses = await auxMeth.autoParser(ciuses, attributes, ciObject.attributes, false);
                  usevalue.value = parseInt(ciObject.uses, 10);

                  if (maxuses === 0) {
                    usescell.className = ' table-empty';
                    usevalue.className = ' table-empty-small';
                    usevalue.value = '∞';
                    usevalue.setAttribute('readonly', 'true');
                  } else {
                    const separator = document.createElement('DIV');
                    separator.className = 'table-sepnum';
                    separator.textContent = '/';

                    const maxusevalue = document.createElement('DIV');

                    let numberuses = ciObject.number;
                    if (numberuses === 0) {
                      numberuses = 1;
                    }

                    maxusevalue.className = 'table-maxuse table-num centertext';
                    // maxusevalue.textContent = "/ " + parseInt(numberuses * maxuses);
                    maxusevalue.textContent = parseInt(ciObject.maxuses, 10);
                    usevalue.addEventListener('change', (event) => this.changeCIUses(ciObject.id, event.target.value));
                    usescell.appendChild(separator);
                    usescell.appendChild(maxusevalue);
                  }
                } else {
                  usescell.className = ' table-empty';
                  usevalue.className = ' table-empty-small';
                  usevalue.value = ' ';
                  usevalue.setAttribute('readonly', 'true');
                }
              }

              const isfirstFree = true;

              for (let k = 0; k < groupprops.length; k += 1) {
                const propRef = groupprops[k].id;
                // let propObj = game.items.get(groupprops[k].id);
                const propObj = await auxMeth.getTElement(groupprops[k].id, 'property', groupprops[k].ikey);
                const propdata = propObj.data.data;
                const propKey = propObj.data.data.attKey;
                const newCell = document.createElement('TD');
                const { isconstant } = groupprops[k];

                newCell.className = 'centertext ';
                newCell.className += propKey;
                newCell.className += ` ${inputgroup}`;

                if (((ciObject.attributes[propKey] !== null && propdata.datatype !== 'label') || (propdata.datatype === 'label')) && !propdata.ishidden) {
                  if (propdata.datatype === 'textarea') {
                    const textiContainer = document.createElement('a');

                    const textSymbol = document.createElement('i');
                    textSymbol.className = 'far fa-file-alt';
                    textiContainer.appendChild(textSymbol);

                    newCell.appendChild(textiContainer);
                    newRow.appendChild(newCell);
                    let isdisabled = false;
                    if (isconstant) {
                      isdisabled = true;
                    }
                    textiContainer.addEventListener('click', (event) => {
                      if (isFree) {
                        this.showFreeTextAreaDialog(ciObject.id, tableKey, propKey, isdisabled);
                      } else {
                        this.showTextAreaDialog(ciObject.id, propKey, isdisabled);
                      }
                    });
                    // }
                  } else if (propdata.datatype !== 'radio' && propdata.datatype !== 'table') {
                    let constantvalue;
                    if (propdata.datatype !== 'label') {
                      if (!isFree) {
                        if (ciTemplate.data.data.attributes[propKey] === null) {
                          ui.notifications.warn(`Inconsistent cItem. Please remove and readd cItem ${ciTemplate.data.name} to Actor`);
                          console.log(`${propKey} fails from ${ciTemplate.data.name}`);
                        }

                        constantvalue = ciTemplate.data.data.attributes[propKey].value;
                        const cvalueToString = constantvalue.toString();
                        const nonumsum = /[#@]{|%\[|if\[|\?\[/g;
                        const checknonumsum = cvalueToString.match(nonumsum);
                        if (checknonumsum) {
                          constantvalue = await auxMeth.autoParser(constantvalue, this.actor.data.data.attributes, ciObject.attributes, true, false, ciObject.number);
                        }
                      } else {
                        constantvalue = propdata.defvalue;
                      }
                    }

                    if (propdata.auto !== '') {
                      constantvalue = ciObject.attributes[propKey].value;
                    }

                    if (isconstant) {
                      let cContent = constantvalue;
                      // console.log(propdata);
                      if (propdata.datatype === 'label') {
                        if (propdata.labelformat === 'D') {
                          cContent = '';
                          // console.log("adding roll");
                          const dieContainer = document.createElement('DIV');
                          dieContainer.setAttribute('title', propdata.tag);
                          dieContainer.className = '';
                          if (propdata.labelsize === 'S') {
                            dieContainer.className += 'label-small';
                          } else if (propdata.labelsize === 'T') {
                            dieContainer.className += 'label-tiny';
                          } else if (propdata.labelsize === 'M') {
                            dieContainer.className += 'label-med';
                          } else if (propdata.labelsize === 'L') {
                            dieContainer.className += 'label-large';
                          }

                          const dieSymbol = document.createElement('i');
                          dieSymbol.className = 'fas fa-dice-d20';
                          dieContainer.appendChild(dieSymbol);

                          newCell.appendChild(dieContainer);
                        } else {
                          cContent = propdata.tag;
                          newCell.textContent = cContent;
                        }
                      } else if (propdata.datatype === 'checkbox') {
                        // console.log("checkbox");
                        let cellvalue = document.createElement('INPUT');
                        // cellvalue.className = "table-input centertext";

                        cellvalue = document.createElement('INPUT');
                        cellvalue.className = 'input-small';
                        if (propdata.labelsize === 'T') {
                          cellvalue.className = 'input-tiny';
                        }
                        cellvalue.setAttribute('type', 'checkbox');
                        let setvalue = false;
                        // console.log(ciObject.attributes[propKey].value);
                        if (ciObject.attributes[propKey].value === true || ciObject.attributes[propKey].value === 'true') {
                          setvalue = true;
                        }

                        if (ciObject.attributes[propKey].value === false || ciObject.attributes[propKey].value === 'false') {
                          ciObject.attributes[propKey].value = false;
                        }

                        // console.log(setvalue);

                        cellvalue.checked = setvalue;
                        cellvalue.setAttribute('disabled', 'disabled');
                        // console.log("lol");
                        newCell.appendChild(cellvalue);
                      } else {
                        newCell.textContent = cContent;
                      }

                      if (propdata.labelsize === 'F') {
                        newCell.className += ' label-free';
                      } else if (propdata.labelsize === 'S') {
                        newCell.className += ' label-small';
                      } else if (propdata.labelsize === 'T') {
                        newCell.className += ' label-tiny';
                      } else if (propdata.labelsize === 'M') {
                        newCell.className += ' label-med';
                      } else if (propdata.labelsize === 'L') {
                        newCell.className += ' label-large';
                      }

                      if (propdata.hasroll) {
                        newCell.className += ' rollable';
                        newCell.addEventListener('click', this._onRollCheck.bind(this, groupprops[k].id, propdata.attKey, ciObject.id, ciObject.ciKey, false, isFree, tableKey, null), false);
                      }
                    } else {
                      // console.log(propdata);
                      let cellvalue = document.createElement('INPUT');
                      // cellvalue.className = "table-input centertext";

                      if (propdata.datatype === 'checkbox') {
                        cellvalue = document.createElement('INPUT');
                        cellvalue.className = 'input-small';
                        if (propdata.labelsize === 'T') {
                          cellvalue.className = 'input-tiny';
                        }
                        cellvalue.className += ` ${inputgroup}`;
                        cellvalue.setAttribute('type', 'checkbox');
                        let setvalue = false;

                        if (ciObject.attributes[propKey].value === true || ciObject.attributes[propKey].value === 'true') {
                          setvalue = true;
                        }
                      } else if (propdata.datatype === 'list') {
                        cellvalue = document.createElement('SELECT');
                        cellvalue.className = 'table-input centertext';

                        cellvalue.className += ` ${inputgroup}`;

                        if (propdata.inputsize === 'F') {
                          cellvalue.className += '  table-free';
                        } else if (propdata.inputsize === 'S') {
                          cellvalue.className += ' input-small';
                        } else if (propdata.inputsize === 'T') {
                          cellvalue.className += ' input-tiny';
                        } else if (propdata.inputsize === 'M') {
                          cellvalue.className += ' input-med';
                        } else if (propdata.inputsize === 'L') {
                          cellvalue.className += ' input-large';
                        } else {
                          cellvalue.className += '  table-free';
                        }

                        // IM ON IT
                        const rawlist = propdata.listoptions;
                        const listobjects = rawlist.split(',');
                        // console.log(ciObject.attributes[propKey].value);
                        for (let y = 0; y < listobjects.length; y += 1) {
                          const nOption = document.createElement('OPTION');
                          nOption.setAttribute('value', listobjects[y]);
                          nOption.textContent = listobjects[y];
                          cellvalue.appendChild(nOption);
                        }
                      } else if (propdata.datatype === 'simpletext' || propdata.datatype === 'label') {
                        cellvalue = document.createElement('INPUT');
                        cellvalue.setAttribute('type', 'text');

                        cellvalue.className = 'table-input';
                        cellvalue.className += ` ${inputgroup}`;
                        if (propdata.inputsize !== null && (((k > 0) && !isFree) || isFree)) {
                          if (propdata.inputsize === 'F') {
                            cellvalue.className += '  table-free';
                          } else if (propdata.inputsize === 'S') {
                            cellvalue.className += ' input-small';
                          } else if (propdata.inputsize === 'T') {
                            cellvalue.className += ' input-tiny';
                          } else if (propdata.inputsize === 'M') {
                            cellvalue.className += ' input-med';
                          } else if (propdata.inputsize === 'L') {
                            cellvalue.className += ' input-large';
                          } else {
                            cellvalue.className += '  table-free';
                          }
                        }

                        if (propdata.datatype === 'label') {
                          cellvalue.setAttribute('readonly', 'true');
                        }
                      } else if (propdata.datatype === 'simplenumeric') {
                        cellvalue = document.createElement('INPUT');
                        cellvalue.setAttribute('type', 'text');
                        cellvalue.className = 'table-input centertext';
                        cellvalue.className += ` ${propTable.data.data.inputgroup}`;

                        if (propdata.inputsize === 'M') {
                          cellvalue.className += ' input-med';
                        } else if (propdata.inputsize === 'T') {
                          cellvalue.className += ' table-tiny';
                        } else {
                          cellvalue.className += ' table-small';
                        }
                      }

                      if (!propdata.editable && !game.user.isGM) {
                        cellvalue.setAttribute('readonly', true);
                      }

                      if (propdata.datatype !== 'checkbox') {
                        cellvalue.value = ciObject.attributes[propKey].value;

                        if (ciObject.attributes[propKey].value === '') {
                          cellvalue.value = constantvalue;
                        }

                        if (propdata.auto !== '') {
                          cellvalue.setAttribute('readonly', true);
                        }
                      } else {
                        let setvalue = false;
                        // console.log(ciObject.attributes[propKey].value);
                        if (ciObject.attributes[propKey].value === true || ciObject.attributes[propKey].value === 'true') {
                          setvalue = true;
                        }

                        cellvalue.checked = setvalue;
                      }

                      cellvalue.className += ` ${propdata.attKey}`;
                      // if (isFree && isfirstFree) {
                      //     if (propdata.inputsize === "F")
                      //         cellvalue.className += " firstcol";
                      //     isfirstFree = false;
                      // }

                      if (!isFree) {
                        newCell.addEventListener('change', (event) => this.saveNewCIAtt(ciObject.id, groupprops[k].id, propdata.attKey, event.target.value));
                      } else {
                        let ischeck = false;
                        if (propdata.datatype === 'checkbox') {
                          ischeck = true;
                        }
                        newCell.addEventListener('change', (event) => this.saveNewFreeItem(ciObject.id, tableKey, propKey, event.target.value, ischeck, event.target.checked));
                      }

                      newCell.appendChild(cellvalue);
                    }
                  }

                  newRow.appendChild(newCell);
                }
              }
            }

            // Add transfer column
            if (propTable.data.data.transferrable) {
              const transferCell = document.createElement('TD');
              transferCell.className = 'ci-transfercell';

              const wraptransferCell = document.createElement('A');
              wraptransferCell.className = 'ci-transfer';
              // wraptransferCell.className += " " + inputgroup;
              wraptransferCell.title = 'Grab Item';
              wraptransferCell.draggable = 'true';
              transferCell.appendChild(wraptransferCell);

              transferCell.addEventListener('dragstart', (event) => this.dragcItem(event, ciObject.id, ciObject.number, this.actor.id));
              newRow.appendChild(transferCell);
            }

            // Delete Element
            if (propTable.data.data.editable || game.user.isGM) {
              const deletecell = document.createElement('TD');
              deletecell.className = 'ci-delete';
              // deletecell.className += " " + inputgroup;
              const wrapdeleteCell = document.createElement('a');
              wrapdeleteCell.className = 'ci-delete';
              // wrapdeleteCell.className += " " + inputgroup;
              wrapdeleteCell.title = 'Delete Item';
              deletecell.appendChild(wrapdeleteCell);

              const wrapdeleteBton = document.createElement('i');
              wrapdeleteBton.className = 'fas fa-times-circle';
              if (!isFree) {
                wrapdeleteBton.addEventListener('click', this.deleteCItem.bind(this, ciObject.id, false), false);
              } else {
                wrapdeleteBton.addEventListener('click', this.deleteFreeItem.bind(this, ciObject.id, tableKey), false);
              }

              wrapdeleteCell.appendChild(wrapdeleteBton);

              newRow.appendChild(deletecell);
            }
          }
        }

        if (groupcitems.length === 0) {
          // Empty row;

          const newRow = document.createElement('TR');
          newRow.className = 'empty-row';
          newRow.className += ` ${inputgroup}`;

          const headercells = document.getElementsByTagName('table');

          for (let x = 0; x < headercells.length; x += 1) {
            if (headercells[x].classList.contains(propTable.data.data.attKey)) {
              const columns = headercells[x].getElementsByTagName('th');
              for (let w = 0; w < columns.length; w += 1) {
                const emptyCell = document.createElement('TD');
                newRow.appendChild(emptyCell);
              }
            }
          }
          if (table !== null) {
            table.appendChild(newRow);
          }
        }

        if (isFree && table !== undefined) {
          const newRow = document.createElement('TR');
          newRow.className = 'transparent-row';
          if (inputgroup) {
            newRow.className += ` ${inputgroup}`;
          }
          newRow.setAttribute('id', `${propTable.data.data.attKey}_plus`);

          const newPlusCell = document.createElement('TD');
          newPlusCell.className = 'pluscell';

          const plusContainer = document.createElement('A');
          plusContainer.className = 'mod-button addRow';
          // plusContainer.addEventListener('click',this.addFreeRow.bind(propTable.data.data.attKey),false);
          plusContainer.addEventListener('click', (event) => this.addFreeRow(propTable.data.data.attKey));

          const plusButton = document.createElement('I');
          plusButton.className = 'fas fa-plus-circle fa-1x';

          plusContainer.appendChild(plusButton);
          newPlusCell.appendChild(plusContainer);
          newRow.appendChild(newPlusCell);
          table.appendChild(newRow);
        }

        if (propTable.data.data.hastotals && table !== null) {
          const newRow = document.createElement('TR');
          newRow.className = 'totals-row';

          const headercells = document.getElementsByTagName('table');
          const counter = groupcitems.length;

          const lastRow = table.children[table.children.length - 1];
          const cellTotal = lastRow.children.length;
          let cellcounter = 0;
          let totalin = false;

          if (propTable.data.data.onlynames !== 'ONLY_NAMES' && lastRow.children[cellcounter] !== null) {
            if (propTable.data.data.onlynames !== 'NO_NAMES' && !isFree) {
              const emptyCell = document.createElement('TD');
              emptyCell.textContent = 'TOTAL';
              emptyCell.className = lastRow.children[cellcounter].className;
              emptyCell.className += ' boldtext';
              emptyCell.className = emptyCell.className.replace('linkable', '');
              newRow.appendChild(emptyCell);
              cellcounter += 1;
              totalin = true;
            }

            if (propTable.data.data.hasactivation) {
              const emptyCell = document.createElement('TD');
              emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
              newRow.appendChild(emptyCell);
              cellcounter += 1;
            }

            if (propTable.data.data.hasunits) {
              const emptyCell = document.createElement('TD');
              emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
              newRow.appendChild(emptyCell);
              cellcounter += 1;
            }

            if (propTable.data.data.hasuses) {
              const emptyCell = document.createElement('TD');
              emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
              newRow.appendChild(emptyCell);
              cellcounter += 1;
            }

            for (let k = 0; k < groupprops.length; k += 1) {
              const propRef = groupprops[k].id;
              // let propObj = game.items.get(groupprops[k].id);
              const propObj = await auxMeth.getTElement(groupprops[k].id, 'property', groupprops[k].ikey);
              const propdata = propObj.data.data;
              const propKey = propObj.data.data.attKey;

              if (!propdata.ishidden) {
                if (propdata.totalize) {
                  const totalCell = document.createElement('TD');

                  let newtotal;
                  if (myactor.attributes[tableKey] !== null) {
                    const totalvalue = myactor.attributes[tableKey].totals[propKey];
                    if (totalvalue) {
                      newtotal = totalvalue.total;
                    }
                  }

                  if (newtotal === null || Number.isNaN(newtotal)) {
                    newtotal = 0;
                  }

                  totalCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
                  totalCell.className += ' centertext';
                  totalCell.textContent = newtotal;
                  newRow.appendChild(totalCell);
                  cellcounter += 1;
                } else {
                  const emptyCell = document.createElement('TD');
                  emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
                  newRow.appendChild(emptyCell);
                  cellcounter += 1;

                  if (!totalin) {
                    emptyCell.textContent = 'TOTAL';
                    emptyCell.className += ' boldtext';
                    totalin = true;
                  }
                }
              }
            }

            // For transfer cell
            if (propTable.data.data.transferrable) {
              const emptyCell = document.createElement('TD');
              emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
              newRow.appendChild(emptyCell);
              cellcounter += 1;
            }

            // Extra for deleted cell
            if (propTable.data.data.editable || game.user.isGM) {
              const emptyCell = document.createElement('TD');
              emptyCell.className = lastRow.children[cellcounter] !== null ? lastRow.children[cellcounter].className : '';
              newRow.appendChild(emptyCell);
              cellcounter += 1;
            }

            if (table !== null) {
              table.appendChild(newRow);
            }
          }
        }
      }
    }

    if (forceUpdate) {
      this.actor.update({ 'data.citems': citems });
    }
    // console.log("refreshcItem finished");
  }

  async dragcItem(evt, iD, number, originiD) {
    evt.stopPropagation();

    const ciTemTemplate = game.items.get(iD);

    const dragData = { type: ciTemTemplate, id: iD, ownerID: originiD };
    evt.dataTransfer.setData('text/plain', JSON.stringify(dragData));
    this._dragType = dragData.type;
  }

  async showTransferDialog(id, ownerID) {
    const actorOwner = game.actors.get(ownerID);
    const ownercItems = duplicate(actorOwner.data.data.citems);
    const cItem = ownercItems.find((y) => y.id === id);
    const cItemOrig = game.items.get(id);

    const d = new Dialog({
      title: `Transfer from ${actorOwner.name}`,
      content: `<div class="transfer-itemmname">
                  <label class="label-citemtransfer">${cItem.name} max: ${cItem.number}</label>
                </div>
                <div class="transfer-itemnumber">
                  <input class="input-transfer" type="number" id="transfer-number" value="1">
                </div>
                <div class="transfer-takeall">
                  <label class="label-transfer">Take All</label>
                  <input class="check-transfer" type="checkbox" id="transfer-all">
                </div>`,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: 'Save',
          callback: async (html) => {
            let numElem = html[0].getElementsByClassName('input-transfer');
            numElem = numElem[0].value;
            let transferAll = html[0].getElementsByClassName('check-transfer');
            transferAll = transferAll[0].checked;
            let mynum = 1;

            const regE = /^\d+$/g;
            const isnum = numElem.match(regE);
            if (isnum) {
              mynum = parseInt(numElem, 10);
            }

            if (transferAll) {
              mynum = parseInt(cItem.number, 10);
            }

            if (mynum > cItem.number) {
              mynum = cItem.number;
            }

            cItem.number -= mynum;

            // REQUEST IF NOT GM
            if (!game.user.isGM) {
              await this.actor.requestTransferToGM(this.actor.id, ownerID, id, mynum);
            } else {
              await actorOwner.update({ 'data.citems': ownercItems });
            }

            let newcitems = duplicate(this.actor.data.data.citems);
            const citemowned = newcitems.find((y) => y.id === id);

            if (!citemowned) {
              newcitems = await this.actor.addcItem(cItemOrig, null, null, mynum);
            } else {
              citemowned.number += mynum;
            }

            await this.updateSubItems(false, newcitems);
          },
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: 'Cancel',
          callback: () => {
            console.log('canceling text edition');
          },
        },
      },
      default: 'one',
      close: () => {
        console.log('Text edition dialog was shown to player.');
      },
    });

    d.render(true);
  }

  showTextAreaDialog(citemID, citemAttribute, disabled) {
    const citem = this.actor.data.data.citems.find((y) => y.id === citemID);
    const ciProp = game.items.find((y) => y.data.data.attKey === citemAttribute);
    if (ciProp === null) {
      return;
    }
    let isdisabled = '';
    if (disabled) {
      isdisabled = 'disabled';
    }

    let content = `
            <textarea id="dialog-textarea-${citemID}-${citemAttribute}" class="textdialog texteditor-large ${ciProp.data.data.inputgroup}" ${isdisabled}>${citem.attributes[citemAttribute].value}</textarea>
            `;
    content += `
            <div class="new-row">
                <div class="lockcontent">
                    <a class="dialoglock-${citemID}-${citemAttribute} lock centertext" title="Edit"><i class="fas fa-lock fa-2x"></i></a>
                    <a class="dialoglock-${citemID}-${citemAttribute} lockopen centertext" title="Edit"><i class="fas fa-lock-open fa-2x"></i></a>
                </div>
            </div>
            `;
    const d = new Dialog({
      title: `${citem.name}-${citemAttribute}`,
      content,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: 'Save',
          callback: async (html) => {
            if (!disabled) {
              citem.attributes[citemAttribute].value = d.data.dialogValue;
              await this.actor.update({ 'data.citems': this.actor.data.data.citems }, { diff: false });
            }
          },
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: 'Cancel',
          callback: () => {
            console.log('canceling text edition');
          },
        },
      },
      default: 'one',
      close: () => {
        console.log('Text edition dialog was shown to player.');
      },
      citemText: true,
      dialogValue: citem.attributes[citemAttribute].value,
    });

    d.render(true);
  }

  showFreeTextAreaDialog(freeId, freeTableKey, freePropKey, disabled) {
    const freeitem = this.actor.data.data.attributes[freeTableKey].tableitems.find((y) => y.id === freeId);
    const ciProp = game.items.find((y) => y.data.data.attKey === freePropKey);
    if (ciProp === null) {
      return;
    }
    let isdisabled = '';
    if (disabled) {
      isdisabled = 'disabled';
    }

    let content = `
            <textarea id="dialog-textarea-${freeId}-${freePropKey}" class="textdialog texteditor-large" ${isdisabled}>${freeitem.attributes[freePropKey].value}</textarea>
            `;
    if (game.user.isGM || ciProp.data.data.editable) {
      content += `
            <div class="new-row">
                <div class="lockcontent">
                    <a class="dialoglock-${freeId}-${freePropKey} lock centertext" title="Edit"><i class="fas fa-lock fa-2x"></i></a>
                    <a class="dialoglock-${freeId}-${freePropKey} lockopen centertext" title="Edit"><i class="fas fa-lock-open fa-2x"></i></a>
                </div>
            </div>
            `;
    }

    // '<textarea id="dialog-textarea-' + freeId + "-" + freePropKey + '" class="texteditor-large ' + ciProp.data.data.inputgroup + '"' + isdisabled + '>' + freeitem.attributes[freePropKey].value + '</textarea>'

    const d = new Dialog({
      title: `Item Num ${freeId}`,
      content,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: 'Save',
          callback: async (html) => {
            if (!disabled) {
              const key = `data.attributes.${freeTableKey}.tableitems[${freeId}].attributes.${freePropKey}.value`;
              const freeattributes = duplicate(this.actor.data.data.attributes[freeTableKey].tableitems);
              const freeTarget = freeattributes.find((y) => y.id === freeId);
              freeTarget.attributes[freePropKey].value = d.data.dialogValue;
              await this.actor.update({ [`data.attributes.${freeTableKey}.tableitems`]: freeattributes });
            }
          },
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: 'Cancel',
          callback: () => {
            console.log('canceling text edition');
          },
        },
      },
      default: 'one',
      close: () => {
        console.log('Text edition dialog was shown to player.');
      },
      citemText: true,
      dialogValue: freeitem.attributes[freePropKey].value,
    });

    d.render(true);
  }

  async saveNewCIAtt(ciId, propId, propKey, value) {
    // console.log("changing citem");
    const cItemsID = duplicate(this.actor.data.data.citems);
    const citem = cItemsID.find((y) => y.id === ciId);
    const propObj = await auxMeth.getTElement(propId, 'property', propKey);
    // console.log(value);

    if (propObj.data.data.datatype !== 'checkbox') {
      citem.attributes[propKey].value = value;
    } else {
      let setvalue = true;
      if (citem.attributes[propObj.data.data.attKey].value) {
        setvalue = false;
      }
      citem.attributes[propObj.data.data.attKey].value = setvalue;

      if (propObj.data.data.checkgroup !== null) {
        if (propObj.data.data.checkgroup !== '') {
          const { checkgroup } = propObj.data.data;
          const unparsedchkgroupArray = checkgroup.split(';');
          const chkgroupArray = [];
          for (let j = 0; j < unparsedchkgroupArray.length; j += 1) {
            let parsedgrpCheck = await auxMeth.autoParser(unparsedchkgroupArray[j], this.actor.data.data.attributes, citem.attributes, true);
            if (parsedgrpCheck === ' ') {
              parsedgrpCheck = '';
            }
            chkgroupArray.push(parsedgrpCheck);
          }

          if (setvalue) {
            for (let x = 0; x < cItemsID.length; x += 1) {
              const anycitem = cItemsID[x];
              Object.entries(anycitem.attributes).forEach(async (attrKey, attrValues) => {
                if (anycitem.id !== ciId) {
                  const propKeyObj = game.items.find((y) => y.data.data.attKey === attrKey);
                  if (propKeyObj !== null && attrKey !== 'name') {
                    if (propKeyObj.data.data.datatype === 'checkbox' && propKeyObj.data.data.checkgroup !== '') {
                      const pointerchkgroupArray = propKeyObj.data.data.checkgroup.split(';');
                      for (let z = 0; z < pointerchkgroupArray.length; z += 1) {
                        const checkKey = pointerchkgroupArray[z];
                        const parsedKey = await auxMeth.autoParser(checkKey, this.actor.data.data.attributes, anycitem.attributes, true);
                        if (chkgroupArray.includes(parsedKey)) {
                          attrValues.value = false;
                        }
                      }
                    }
                  }
                }
              });
            }
          }
        }
      }
    }

    if (!this.actor.isToken) {
      this.actor.update({ 'data.citems': cItemsID });
    } else {
      const tokenId = this.id.split('-')[2];
      const mytoken = canvas.tokens.get(tokenId);
      await mytoken.document.update({ 'actorData.data.citems': cItemsID });
    }
  }

  async saveNewFreeItem(id, tableKey, fpropKey, value, ischeck = false, checked = null) {
    const myfreeItems = await duplicate(this.actor.data.data.attributes[tableKey].tableitems);
    const myItem = myfreeItems.find((y) => y.id === id);

    if (ischeck) {
      value = checked;
      const propObj = game.items.find((y) => y.data.data.attKey === fpropKey);
      if (propObj.data.data.checkgroup !== null) {
        if (propObj.data.data.checkgroup !== '') {
          const { checkgroup } = propObj.data.data;
          const chkgroupArray = checkgroup.split(';');
          if (value) {
            Object.entries(myItem.attributes).forEach(async (propKey, propValues) => {
              if (propKey !== propObj.data.data.attKey) {
                const propKeyObj = game.items.find((y) => y.data.data.attKey === propKey);
                if (propKeyObj !== null) {
                  if (propKeyObj !== '' && propKeyObj.data.data.datatype === 'checkbox') {
                    const pointerchkgroupArray = propKeyObj.data.data.checkgroup.split(';');
                    for (let z = 0; z < chkgroupArray.length; z += 1) {
                      const checkKey = chkgroupArray[z];
                      const parsedKey = await auxMeth.autoParser(checkKey, this.actor.data.data.attributes, myItem.attributes, true);
                      if (pointerchkgroupArray.includes(parsedKey)) {
                        propValues.value = false;
                      }
                    }
                  }
                }
              }
            });
          }
        }
      }
    }

    myItem.attributes[fpropKey].value = value;
    await this.actor.update({ [`data.attributes.${tableKey}.tableitems`]: myfreeItems });
  }

  async linkCItem(evt) {
    // console.log();
    const item = await auxMeth.getcItem(evt.currentTarget.getAttribute('item_id'), evt.currentTarget.getAttribute('item_ciKey'));
    item.sheet.render(true);
  }

  async useCIIcon(itemId, ciKey, value, iscon = false, isactivation = false) {
    // const citemObj = game.items.get(itemId).data.data;
    const citemObjfinder = await auxMeth.getcItem(itemId, ciKey);
    const citemObj = citemObjfinder.data.data;

    if (citemObj.roll !== '' && (!isactivation || (isactivation && value))) {
      const cItemData = {};
      cItemData.id = itemId;
      cItemData.value = value;
      cItemData.iscon = iscon;
      this._onRollCheck(null, null, itemId, ciKey, true, null, null, cItemData);
    } else {
      this.activateCI(itemId, value, iscon, null, isactivation);
    }
  }

  async activateCI(itemId, value, iscon = false, roll = null, isactivation = false) {
    const actorData = duplicate(this.actor.data.data);
    const { citems } = actorData;
    const citem = citems.find((y) => y.id === itemId);
    const { attributes } = this.actor.data.data;

    const citemObjfinder = await auxMeth.getcItem(itemId, citem.ciKey);
    const citemObj = citemObjfinder.data.data;
    let objectUses = duplicate(citem.uses);

    if (isactivation) {
      citem.isactive = value;
    } else {
      citem.isactive = true;
    }

    if (citem.isactive) {
      citem.isreset = false;
    }

    // console.log(citem.maxuses);
    if (iscon && citem.maxuses > 0) {
      objectUses -= 1;
      const thismaxuses = parseInt(citem.maxuses, 10);
      if (citem.uses > 0 && citemObj.usetype === 'CON') {
        const actualItems = Math.ceil(parseInt(objectUses, 10) / (thismaxuses / citem.number));

        if (!citemObj.rechargable) {
          citem.number = actualItems;
          if (objectUses === 0) {
            citem.number = 0;
          }
        }
      }

      citem.uses -= 1;

      if (!citemObj.rechargable) {
        citem.maxuses = parseInt(citemObj.maxuses, 10) * parseInt(citem.number, 10);
      }
    }

    this.actor.data.flags.haschanged = true;

    if (roll !== null) {
      citem.attributes._lastroll = roll;
    }

    await this.actor.update({ 'data.citems': citems });
  }

  async rechargeCI(itemId, ciKey) {
    const citems = duplicate(this.actor.data.data.citems);
    const citem = citems.find((y) => y.id === itemId);
    // const citemObj = game.items.get(itemId).data.data;
    const citemObjfinder = await auxMeth.getcItem(itemId, ciKey);
    const citemObj = citemObjfinder.data.data;

    let totalnumber = citem.number;
    if (totalnumber === 0) {
      totalnumber = 1;
    }

    citem.uses = citem.maxuses;
    await this.actor.update({ 'data.citems': citems });
  }

  async deleteCItem(itemID, cascading = false) {
    // get Item
    // console.log("deleting");

    const subitems = await this.actor.deletecItem(itemID, cascading);

    // console.log(subitems);
    if (this.actor.isToken) {
      const myToken = canvas.tokens.get(this.actor.token.id);

      await myToken.actor.update({ data: subitems.data });
      // await myToken.update({"data.citems": this.actor.data.data.citems});
    } else {
      await this.actor.update({ data: subitems.data });
      // await this.actor.update(this.actor.data);
    }

    // await this.actor.update(this.actor.data);
  }

  async addFreeRow(tableKey) {
    const myfreeItems = await duplicate(this.actor.data.data.attributes[tableKey].tableitems);
    let lastIndex = -1;
    if (myfreeItems.length) {
      lastIndex = myfreeItems[myfreeItems.length - 1].id;
    }

    const newItem = {};
    newItem.attributes = {};
    newItem.icon = 'star';
    newItem.id = lastIndex + 1;

    // Get element values
    // let tableTemplate = game.items.find(y => y.data.type === "property" && y.data.data.datatype === "table" && y.data.data.attKey === tableKey);
    const tableTemplate = await auxMeth.getTElement(null, 'property', tableKey);

    if (tableTemplate !== null) {
      const tableGroup = tableTemplate.data.data.group.id;
      if (tableGroup !== null) {
        const groupTemplate = await auxMeth.getTElement(tableTemplate.data.data.group.id, 'group', tableTemplate.data.data.group.ikey);
        const groupProps = groupTemplate.data.data.properties;
        if (groupProps.length > 0) {
          for (let i = 0; i < groupProps.length; i += 1) {
            // let propTemplate = game.items.get(groupProps[i].id);
            const propTemplate = await auxMeth.getTElement(groupProps[i].id, 'property', groupProps[i].ikey);
            newItem.attributes[propTemplate.data.data.attKey] = {};
            newItem.attributes[propTemplate.data.data.attKey].value = propTemplate.data.data.defvalue;
          }
        }
      }
    }

    myfreeItems.push(newItem);
    await this.actor.update({ [`data.attributes.${tableKey}.tableitems`]: myfreeItems });
  }

  async deleteFreeItem(id, tableKey) {
    const myfreeItems = await duplicate(this.actor.data.data.attributes[tableKey].tableitems);
    myfreeItems.splice(myfreeItems.indexOf(myfreeItems.find((y) => y.id === id)), 1);
    await this.actor.update({ [`data.attributes.${tableKey}.tableitems`]: myfreeItems });
  }

  handleGMinputs(basehtml) {
    // SET TABLES INFO
    const gminputs = basehtml.find('.inputGM');
    for (let i = 0; i < gminputs.length; i += 1) {
      const input = gminputs[i];

      if (!game.user.isGM) {
        input.setAttribute('readonly', true);

        if (input.type === 'select-one') {
          input.className += ' list-noneditable';
        }
      }
    }
  }

  async changeCINum(itemID, ciKey, value) {
    const citemIDs = duplicate(this.actor.data.data.citems);
    const citem = this.actor.data.data.citems.find((y) => y.id === itemID);
    const citemNew = citemIDs.find((y) => y.id === itemID);

    if (value === 0) {
      value = 1;
    }

    if (value < 0 || Number.isNaN(value)) {
      value = citem.number;
    }

    citemNew.number = value;

    // let cItemTemp = game.items.get(itemID);
    // let cItemTemp = await auxMeth.getcItem(itemID, ciKey);
    // let tempmaxuses = await auxMeth.autoParser(cItemTemp.data.data.maxuses, this.actor.data.data.attributes, cItemTemp.data.data.attributes, false);

    // citemNew.maxuses = parseInt(value * tempmaxuses);
    // citemNew.uses = citemNew.maxuses;

    // await this.scrollbarSet(false);
    // this.actor.update(this.actor.data);

    await this.actor.update({ 'data.citems': citemIDs });
  }

  async changeCIUses(itemID, value) {
    const citemIDs = duplicate(this.actor.data.data.citems);
    const citem = citemIDs.find((y) => y.id === itemID);
    const myindex = citemIDs.indexOf(citem);

    citem.uses = value;
    // console.log("changing");
    // if (parseInt(citem.uses) >= parseInt(citem.maxuses)) {
    //     citem.maxuses = parseInt(citem.uses);
    // }

    // await this.scrollbarSet(false);
    // await this.actor.update(this.actor.data);
    await this.actor.update({ 'data.citems': citemIDs });
  }

  async refreshBadge(basehtml) {
    const html = await basehtml.find('.badge-click');
    for (let i = 0; i < html.length; i += 1) {
      const badgeNode = html[i];
      const propKey = badgeNode.getAttribute('attKey');
      const att = this.actor.data.data.attributes[propKey];
      if (att !== null) {
        badgeNode.textContent = att.value;
      }
    }
  }

  // call before super._render
  async _saveScrollStates() {
    const scrollStates = [];

    const html = this._element;
    if (html === null) {
      return Promise.reject();
    }

    const lists = html.find('.scrollable');
    [...lists].forEach((list) => {
      scrollStates.push($(list).scrollTop());
    });

    return scrollStates;
  }

  // call after super._render
  async _setScrollStates() {
    const html = this._element;
    if (html === null) {
      return Promise.reject();
    }

    if (this.scrollStates) {
      const lists = html.find('.scrollable');

      for (let i = 0; i < lists.length; i += 1) {
        const newEl = $(lists[i]);
        const newScroll = parseInt(this.scrollStates[i], 10);
        newEl[0].scrollTop = newScroll;
      }
    }

    return Promise.resolve();
  }

  async _render(force = false, options = {}) {
    this.scrollStates = await this._saveScrollStates();

    await super._render(force, options);

    // await new Promise(async ()=>{ await this._setScrollStates() },0);

    // await this._setScrollStates();
  }

  async modifyLists(basehtml) {
    const attKeys = Object.keys(this.actor.data.data.attributes);

    attKeys.forEach((key, index) => {
      const aProp = this.actor.data.data.attributes[key];

      if (aProp.listedit !== null) {
        const mytag = `.${key}`;
        const myhtmllist = basehtml.find(mytag);

        if (aProp.listedit.add !== null) {
          for (let i = 0; i < aProp.listedit.add.length; i += 1) {
            const addoption = aProp.listedit.add[i];

            const option = document.createElement('option');
            option.value = option.text === addoption;

            if ($(myhtmllist[0]).has(`option[value='${addoption}']`)) {
              myhtmllist[0].add(option);
            }
          }
        }

        if (aProp.listedit.remove !== null) {
          for (let j = 0; j < aProp.listedit.remove.length; j += 1) {
            const removeoption = aProp.listedit.remove[j];

            for (let n = 0; n < myhtmllist[0].options.length; n += 1) {
              if (myhtmllist[0].options[n].value === removeoption) {
                myhtmllist[0].remove(n);
                break;
              }
            }
          }
        }
        myhtmllist[0].value = aProp.value;
      }
    });
  }

  async populateRadioInputs(basehtml) {
    // console.log("reinput");
    const html = await basehtml.find('.radio-input');
    for (let i = 0; i < html.length; i += 1) {
      const radioNode = html[i];

      const { attributes } = this.actor.data.data;
      let value = 0;
      const propId = radioNode.getAttribute('attId');
      let propRawName = radioNode.getAttribute('name');
      propRawName = propRawName.replace('data.attributes.', '');
      propRawName = propRawName.replace('.value', '');
      // let property = game.items.get(propId);
      const property = await auxMeth.getTElement(propId, 'property', propRawName);

      if (property !== null) {
        const { attKey } = property.data.data;
        const { radiotype } = property.data.data;

        if (attributes[attKey] !== null) {
          const maxRadios = attributes[attKey].max;
          ({ value } = attributes[attKey]);

          radioNode.innerHTML = '';
          // console.log(value);
          if (maxRadios > 0) {
            for (let j = 0; j <= parseInt(maxRadios, 10); j += 1) {
              const radiocontainer = document.createElement('a');
              const clickValue = j;
              radiocontainer.setAttribute('clickValue', clickValue);
              radiocontainer.className = 'radio-element';
              radiocontainer.style = 'font-size:14px;';
              if (radiotype === 'S') {
                radiocontainer.style = 'font-size:16px;';
              }

              const radiobutton = document.createElement('i');

              if (j === 0) {
                radiobutton.className = 'far fa-times-circle';
              } else if (value >= clickValue) {
                radiobutton.className = 'fas fa-circle';
                if (radiotype === 'S') {
                  radiobutton.className = 'fas fa-square';
                }
              } else {
                radiobutton.className = 'far fa-circle';
                if (radiotype === 'S') {
                  radiobutton.className = 'far fa-square';
                }
              }

              radiocontainer.appendChild(radiobutton);

              radiobutton.addEventListener('click', (event) => this.clickRadioInput(clickValue, propId, event.target));

              await radioNode.appendChild(radiocontainer);
            }
          }
        }
      }
    }
  }

  // Set external images
  async setImages(basehtml) {
    const html = await basehtml.find('.isimg');
    for (let i = 0; i < html.length; i += 1) {
      const imgNode = html[i];
      const imgPath = imgNode.getAttribute('img');

      const imgEl = document.createElement('img');
      imgEl.className = 'isimg';
      imgEl.src = imgPath;

      imgNode.appendChild(imgEl);
    }
  }

  async addHeaderButtons(basehtml) {
    // const headerElm = await basehtml.find(".window-header");

    // if (headerElm.length === 0)
    //     return;

    // let atagEl = document.createElement('a');
    // atagEl.className = "header-button hide-template";

    // let imgEl = document.createElement('i');
    // imgEl.className = "far fa-eye";
    // imgEl.textContent = "Hide";

    // atagEl.appendChild(imgEl);

    // headerElm[0].insertBefore(atagEl, headerElm[0].children[0].nextSibling);

    // imgEl.addEventListener("click", async (event) => {
    //     evt.preventDefault();
    //     console.log("muestrate");
    // });
  }

  // Set external images
  async setCheckboxImages(basehtml) {
    const html = await basehtml.find('.customcheck');
    for (let i = 0; i < html.length; i += 1) {
      const checkNode = html[i];
      const onPath = checkNode.getAttribute('onPath');
      const offPath = checkNode.getAttribute('offPath');
      const propKey = checkNode.getAttribute('attKey');

      if (this.actor.data.data.attributes[propKey] !== null) {
        const myvalue = this.actor.data.data.attributes[propKey].value;

        let selected = offPath;
        if (myvalue) {
          selected = onPath;
        }
        checkNode.style.backgroundImage = `url('${selected}')`;
      }
    }
  }

  async clickRadioInput(clickValue, propId, target) {
    // let property = game.items.get(propId);
    const property = await auxMeth.getTElement(propId);
    const { radiotype } = property.data.data;
    const { attKey } = property.data.data;
    const { attributes } = this.actor.data.data;
    // attributes[attKey].value =  clickValue;
    await this.actor.update({ [`data.attributes.${attKey}.value`]: clickValue });
    // await this.actor.actorUpdater();
    // await this.actor.update({"data.attributes":attributes}, {diff: false});
    if (clickValue > 0) {
      target.className = 'fas fa-circle';
      target.style = 'font-size:14px;';
      if (radiotype === 'S') {
        target.style = 'font-size:16px;';
        target.className = 'fas fa-square';
      }
    }

    // await this.scrollbarSet();
  }

  async displaceTabs2(next = null, newhtml = null) {
    // console.log("displacing");
    let tabs;
    let nonbio = false;
    const actorsheet = this;

    // console.log(newhtml);

    const fakelastTab = $(newhtml).find('#tab-last');
    fakelastTab.remove();

    const biotab = $(newhtml).find('#tab-0');
    if (!this.actor.data.data.biovisible) {
      nonbio = true;

      if (biotab.length > 0) {
        if (biotab[0].classList.contains('active')) {
          biotab[0].nextElementSibling.click();
        }
      }
      biotab.remove();
    } else {
      biotab[0].classList.add('player-tab');
    }

    if (game.user.isGM) {
      tabs = $(newhtml).find('.tab-button');
    } else {
      tabs = $(newhtml).find('.player-tab');
    }

    const activetab = $(newhtml).find('.tab-button.active');

    let foundfirst = false;
    const passedfirst = false;
    const maxtabs = this.actor.data.data.visitabs - 1;
    const totaltabs = tabs.length;
    // console.log(tabs);

    let minTab = totaltabs - (maxtabs + 1);
    if (minTab < 0) {
      minTab = 0;
    }
    if (activetab.index() > minTab) {
      tabs[minTab].classList.add('visible-tab');
    }

    const tabcounter = 0;
    const firsthidden = false;
    const firstpassed = false;
    let displaying = false;
    let displaycounter = 0;
    let fvble = actorsheet._tabs[0].active;
    if (actorsheet._tabs[0].firstvisible !== null) {
      fvble = actorsheet._tabs[0].firstvisible;
    }

    tabs.each(async (i, tab) => {
      if (tab.dataset.tab === fvble && !foundfirst) {
        const nexttab = tabs[i + 1];
        const prevtab = tabs[i - 1];
        const lasttab = tabs[i + maxtabs + 1];
        if (next === 'prev') {
          if (prevtab !== null) {
            fvble = prevtab.dataset.tab;
          }
        } else if (next === 'next') {
          if (nexttab !== null && lasttab !== null) {
            fvble = nexttab.dataset.tab;
          }
        }
        actorsheet._tabs[0].firstvisible = fvble;
        foundfirst = true;
      }
    });

    tabs.each(async (i, tab) => {
      if (displaying) {
        if (displaycounter <= maxtabs) {
          if (!tab.classList.contains('visible-tab')) {
            tab.classList.add('visible-tab');
          }
          tab.classList.remove('hidden-tab');
          displaycounter += 1;
        } else {
          displaying = false;
          if (!tab.classList.contains('hidden-tab')) {
            tab.classList.add('hidden-tab');
          }
          tab.classList.remove('visible-tab');
        }
      } else if (tab.dataset.tab === fvble) {
        if (!tab.classList.contains('visible-tab')) {
          tab.classList.add('visible-tab');
        }
        tab.classList.remove('hidden-tab');
        displaying = true;
        displaycounter += 1;
      } else {
        if (!tab.classList.contains('hidden-tab')) {
          tab.classList.add('hidden-tab');
        }
        tab.classList.remove('visible-tab');
      }
    });
  }

  async setSheetStyle() {
    // console.log(this.actor.data.data.gtemplate);

    const _mytemplate = await game.actors.find((y) => y.data.data.istemplate && y.data.data.gtemplate === this.actor.data.data.gtemplate);
    if (_mytemplate === null) {
      return;
    }
    const basehtml = this.element;

    if (this.actor.data.data.gtemplate === 'Default') {
      return;
    }

    const bground = await basehtml.find('.window-content');
    const sheader = await basehtml.find('.sheet-header');
    const wheader = await basehtml.find('.window-header');
    const stabs = await basehtml.find('.atabs');

    // Set Height
    if (_mytemplate.data.data.setheight !== '' && !_mytemplate.data.data.resizable) {
      basehtml[0].style.height = `${_mytemplate.data.data.setheight}px`;
      const tabhandler = await basehtml.find('.tab');
      for (let j = 0; j < tabhandler.length; j += 1) {
        const mytab = tabhandler[j];

        const totalheight = parseInt(_mytemplate.data.data.setheight, 10) - parseInt(wheader[0].clientHeight, 10) - parseInt(sheader[0].clientHeight, 10) - parseInt(stabs[0].clientHeight, 10) - 15;
        mytab.style.height = `${totalheight}px`;
      }
    }

    // Set Background
    if (_mytemplate.data.data.backg !== '') {
      bground[0].style.background = `url(${_mytemplate.data.data.backg}) repeat`;
    }

    if (!_mytemplate.data.data.resizable) {
      const sizehandler = await basehtml.find('.window-resizable-handle');
      sizehandler[0].style.visibility = 'hidden';
    } else {
      const tabhandler = await basehtml.find('.tab');
      for (let j = 0; j < tabhandler.length; j += 1) {
        const mytab = tabhandler[j];

        const totalheight = parseInt(basehtml[0].style.height, 10) - parseInt(wheader[0].clientHeight, 10) - parseInt(sheader[0].clientHeight, 10) - parseInt(stabs[0].clientHeight, 10) - 15;
        mytab.style.height = `${totalheight}px`;
      }
    }
  }

  async checkAttributes(formData) {
    Object.keys(formData).forEach((att) => {
      if (att.includes('data.attributes.')) {
        const thisatt = formData[att];
        if (Array.isArray(formData[att])) {
          formData[att] = thisatt[0];
        }
      }
    });

    return formData;
  }

  //* *override
  _onEditImage(event) {
    const attr = event.currentTarget.dataset.edit;
    const current = getProperty(this.actor.data, attr);
    const myactor = this.actor;
    new FilePicker({
      type: 'image',
      current,
      callback: async (path) => {
        event.currentTarget.src = path;
        // manual overwrite of src
        const imageform = this.form.getElementsByClassName('profile-img');
        imageform[0].setAttribute('src', path);
        // myactor.data.img = path;

        // myactor.update(myactor.data);

        const mytoken = await this.setTokenOptions(myactor.data, path);

        if (mytoken) {
          await myactor.update({ token: mytoken, img: path });
        }

        this._onSubmit(event);
      },
      top: this.position.top + 40,
      left: this.position.left + 10,
    }).browse(current);
  }

  async setTokenOptions(myactorData, path = null) {
    // console.log(myactorData.token);

    if (path === null) {
      path = myactorData.img;
    }

    const mytoken = await duplicate(myactorData.token);

    if (!myactorData.data.istemplate) {
      if (mytoken.dimLight === null) {
        mytoken.dimLight = 0;
      }

      if (mytoken.dimSight === null) {
        mytoken.dimSight = 0;
      }

      if (mytoken.brightLight === null) {
        mytoken.brightLight = 0;
      }

      mytoken.img = path;

      // mytoken.name = myactorData.name;

      if (game.settings.get('sandbox', 'tokenOptions')) {
        const { displayName } = myactorData.data;

        if (myactorData.token) {
          mytoken.displayName = displayName;

          mytoken.displayBars = displayName;

          if (myactorData.data.tokenbar1 !== null) {
            mytoken.bar1.attribute = myactorData.data.tokenbar1;
          }
        }
      }
    }

    return mytoken;
  }

  async _updateObject(event, formData) {
    event.preventDefault();

    // await this.scrollbarSet();

    if (event.target === null && !game.user.isGM && !formData['data.biography']) {
      return;
    }

    if (event.target) {
      if (event.target.name === '') {
        return;
      }
    }

    if (formData['data.gtemplate'] === '') {
      formData['data.gtemplate'] = this.actor.data.data.gtemplate;
    }

    formData = await this.checkAttributes(formData);
    if (event.target !== null) {
      let target = event.target.name;
      const escapeForm = false;

      if (target === 'data.gtemplate') {
        return;
      }

      // if(!escapeForm){

      let property;
      let modmax = false;
      if (target.includes('.max')) {
        modmax = true;
      }
      if (target !== null) {
        target = target.replace('.value', '');
        target = target.replace('.max', '');

        const attri = target.split('.')[2];
        // property = game.items.find(y => y.data.type === "property" && y.data.data.attKey === attri);
        property = await auxMeth.getTElement('NONE', 'property', attri);
      }

      if (property !== null) {
        if (property.data.data.datatype !== 'checkbox') {
          formData[event.target.name] = event.target.value;
        } else {
          formData[event.target.name] = event.target.checked;
        }

        const attrimodified = `${target}.modified`;
        const attrimodmax = `${target}.modmax`;
        if (!modmax) {
          formData[attrimodified] = true;
        } else {
          formData[attrimodmax] = true;
        }
      } else if (target === 'data.biovisible') {
        formData['data.biovisible'] = event.target.checked;
      } else if (target === 'data.resizable') {
        formData['data.resizable'] = event.target.checked;
      } else if (target === 'data.istemplate') {
        formData['data.istemplate'] = event.target.checked;
      } else {
        formData[event.target.name] = event.currentTarget.value;
      }
    }

    await super._updateObject(event, formData);
  }

  /* -------------------------------------------- */
}

export default gActorSheet;
